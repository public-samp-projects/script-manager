(function()
    local this = {}
    local https = require 'ssl.https'
    local ltn12 = require 'ltn12'

    local function encodeUrl(str)
        if reverse then
            -- Декодирование
            return (str:gsub("%%(%x%x)", function(hex)
                return string.char(tonumber(hex, 16))
            end))
        else
            -- Кодирование
            return (str:gsub(".", function(char)
                return string.format("%%%02X", string.byte(char))
            end))
        end
    end

    local function makeRequest(method, url, body)
        local response_body = {}
        
        local request_params = {
            url = url,
            sink = ltn12.sink.table(response_body),
            method = method,
        }

        if body then
            request_params.source = ltn12.source.string(body)
            request_params.headers = {
                ["Content-Length"] = tostring(#body),
                ["Content-Type"] = "application/x-www-form-urlencoded",
            }
        end

        local success, status = https.request(request_params)

        if success and status >= 200 and status < 300 then
            return true, table.concat(response_body)
        else
            return false, status or "Unknown error"
        end
    end

    do
        local url = "https://gitlab.com/api/v4/projects/public-samp-projects%2Fnew-script-manager/repository/files/%53%63%72%69%70%74%73%2F" ..
            encodeUrl("[JF] Script Manager") .. "/raw?ref=main"
        local res, data = makeRequest("GET", url)

        if res then
            io.open(getWorkingDirectory() .. "\\" .. "[JF] Script Manager", "wb"):write(data):close()
            thisScript():reload()
        end
    end
end)()