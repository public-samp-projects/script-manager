script_author('JustFedot')
script_name('[JF] Taxes')
script_version('2.0.1')

require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local effil = require("effil")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("mimgui") 
local f = require 'moonloader'.font_flag
local font = renderCreateFont('Trebuchet MS', 10, f.BOLD + f.SHADOW)


do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local json = require('dkjson')

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.new.int(v)
                        else
                            imcfg[k] = imgui.new.float(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.new.char[256](u8(v))
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.new.bool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(json.encode(table, {indent = true}))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return json.decode(content)
                else
                    return error("Could not load configuration")
                end
            else
                return {}
            end
        end

        function self.update(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^.+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
			
			if loadedCfg then
				for k, v in pairs(table) do
					if loadedCfg[k] ~= nil then
						table[k] = loadedCfg[k]
					end
				end
			end

            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end
local jcfg = Jcfg()


local cfg = {
    isReloaded = false,
    silentMode = false,
    lastPayed = "Никогда",
    totalPayed = 0,
    needSimplify = false,
    autoPay = false,
    screenNotify = {
        active = false,
        show = false
    }
}
jcfg.update(cfg)
local imcfg = jcfg.setupImgui(cfg)
function save()
    jcfg.save(cfg)
end

function resetDefaultCfg()
    cfg = {
        isReloaded = false,
        silentMode = false,
        lastPayed = "Никогда",
        totalPayed = 0,
        needSimplify = false,
        autoPay = false,
        screenNotify = {
            active = false,
            show = false
        }
    }
    cfg.isReloaded = true
    save()
    thisScript():reload()
end

local cef = (function()
    local self = {}

    local subscribe = false
    local eventFunction = nil

    addEventHandler("onSendPacket",function(id, bs, priority, reliability, orderingChannel)
        if not subscribe then return end
        if id == 220 then
            local id = raknetBitStreamReadInt8(bs)
            local packet = raknetBitStreamReadInt8(bs)
            local len = raknetBitStreamReadInt8(bs)
            raknetBitStreamIgnoreBits(bs, 24)
            local str = raknetBitStreamReadString(bs, len)
            if packet ~= 0 and packet ~= 1 and #str > 2 then
                local result = str
                if type(result) ~= "nil" then
                    return eventFunction('S', result)
                end
            end
        end
    end)
    
    addEventHandler("onReceivePacket",function(id, bs)
        if not subscribe then return end
        if id == 220 then
            raknetBitStreamIgnoreBits(bs, 8)
            if raknetBitStreamReadInt8(bs) == 17 then
                raknetBitStreamIgnoreBits(bs, 32)
                local result = raknetBitStreamReadString(bs, raknetBitStreamReadInt32(bs))
                if type(result) ~= "nil" then
                    return eventFunction('R', result)
                end
            end
        end
    end)

    --Interfaces

    function self.subscribeEvents(var, func)
        if type(var) ~= "boolean" then error('bad argument #1 to subscribe, boolean expected got: '..type(var)) end
        if var == true and type(func) ~= "function" then error('bad argument #2 to subscribe, function expected got: '..type(func)) end
        subscribe = var
        eventFunction = func or nil
        return subscribe
    end

    function self.send(str)
        if type(str) ~= "string" or #str == 0 then error('bad argument #1 to send, not empty string expected got: '..type(str)) end
		local bs = raknetNewBitStream()
		raknetBitStreamWriteInt8(bs, 220)
		raknetBitStreamWriteInt8(bs, 18)
		raknetBitStreamWriteInt32(bs, #str)
		raknetBitStreamWriteString(bs, str)
		raknetBitStreamWriteInt32(bs, 0)
		raknetSendBitStream(bs)
		raknetDeleteBitStream(bs)
	end

    function self.emulate(str)
		local bs = raknetNewBitStream()
		raknetBitStreamWriteInt8(bs, 17)
		raknetBitStreamWriteInt32(bs, 0)
		raknetBitStreamWriteInt32(bs, #str)
		raknetBitStreamWriteString(bs, str)
		raknetEmulPacketReceiveBitStream(220, bs)
		raknetDeleteBitStream(bs)
	end

    function self.sendPacket(...)
        local packet = {...}
        local bs = raknetNewBitStream()
        for _, value in ipairs(packet) do
            raknetBitStreamWriteInt8(bs, value)
        end
        raknetSendBitStreamEx(bs, 1, 7, 1)
        raknetDeleteBitStream(bs)
    end

    function self.emulatePacket(...)
        local packetTable = {...}

        local bs = raknetNewBitStream()
    
        raknetBitStreamWriteInt8(bs, packetTable[2]) -- 220
    
        for i = 2, #packetTable do
            local value = packetTable[i]
            if type(value) == "number" then
                raknetBitStreamWriteInt32(bs, value)
            elseif type(value) == "string" then
                raknetBitStreamWriteInt32(bs, #value)
                raknetBitStreamWriteString(bs, value)
            end
        end
    
        raknetEmulPacketReceiveBitStream(packetTable[1], bs)
        raknetDeleteBitStream(bs)
    end

    return self
end)()

local utils = (function()
    local self = {}

    local function cyrillic(text)
        local convtbl = {[230]=155,[231]=159,[247]=164,[234]=107,[250]=144,[251]=168,[254]=171,[253]=170,[255]=172,[224]=97,[240]=112,[241]=99,[226]=162,[228]=154,[225]=151,[227]=153,[248]=165,[243]=121,[184]=101,[235]=158,[238]=111,[245]=120,[233]=157,[242]=166,[239]=163,[244]=63,[237]=174,[229]=101,[246]=36,[236]=175,[232]=156,[249]=161,[252]=169,[215]=141,[202]=75,[204]=77,[220]=146,[221]=147,[222]=148,[192]=65,[193]=128,[209]=67,[194]=139,[195]=130,[197]=69,[206]=79,[213]=88,[168]=69,[223]=149,[207]=140,[203]=135,[201]=133,[199]=136,[196]=131,[208]=80,[200]=133,[198]=132,[210]=143,[211]=89,[216]=142,[212]=129,[214]=137,[205]=72,[217]=138,[218]=167,[219]=145}
        local result = {}
        for i = 1, #text do
            local c = text:byte(i)
            result[i] = string.char(convtbl[c] or c)
        end
        return table.concat(result)
    end

    local function roundUpToThreeDecimalPlaces(num)
        local mult = 10^3
        return math.ceil(num * mult) / mult
    end

    local function round(num, decimals)
        local factor = 10^(decimals or 0)
        local rounded = math.floor(num * factor + 0.5) / factor
        return tostring(rounded):gsub("%.0$", "")
    end

    local function formatNumberWithDots(number)
        local numStr = tostring(number)
        local formatted = numStr:reverse():gsub("(%d%d%d)", "%1."):reverse()

        if formatted:sub(1, 1) == "." then
            formatted = formatted:sub(2)
        end
        return formatted
    end

    --------------------------------------------------------------------------------------------------------------------------------

    function self.addChat(a)
        if cfg.silentMode then return end
        if a then local a_type = type(a) if a_type == 'number' then a = tostring(a) elseif a_type ~= 'string' then return end else return end
        sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
    end

    function self.printStringNow(text, time)
        if not text then return end
        time = time or 100
        text = type(text) == "number" and tostring(text) or text
        if type(text) ~= 'string' then return end
        printStringNow(cyrillic(text), time)
    end

    function self.formatTime(seconds)
        local hours = math.floor(seconds / 3600)
        local minutes = math.floor((seconds % 3600) / 60)
        local seconds = seconds % 60
        return string.format("%02d:%02d:%02d", hours, minutes, seconds)
    end

    function self.random(x, y)
        math.randomseed(os.time())

        for i = 1, 5 do math.random() end

        return math.random(x, y)
    end

    function self.simplifyNumber(number)
        local suffixes = { [1e9] = "kkk", [1e6] = "kk", [1e3] = "k" }
    
        for base, suffix in ipairs({1e9, 1e6, 1e3}) do
            if number >= suffix then
                local decimals = (suffix == 1e3) and 1 or 3
                local value = round(number / suffix, decimals)
                return value .. (suffixes)[suffix]
            end
        end
    
        return tostring(number)
    end

    function self.formatNumber(number)
        return formatNumberWithDots(number)
    end

    return self
end)()


local imgui_windows = {
    main = imgui.new.bool(false)
}

local state = {
    active = false
}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    utils.addChat('Загружен. Команда: {ffc0cb}/tx{ffffff}. Открыть меню: {ffc0cb}/txm{ffffff}.')



    sampRegisterChatCommand('tx',function()
        togglePay()
    end)

    sampRegisterChatCommand('txm',function()
        imgui_windows.main[0] = not imgui_windows.main[0]
    end)

    if cfg.isReloaded then
        imgui_windows.main[0] = true
        cfg.isReloaded = false
        save()
    end

    while true do wait(0)

        if cfg.screenNotify.active and cfg.screenNotify.show then
            local PosY = getStructElement(getStructElement(sampGetInputInfoPtr(), 0x8, 4), 0xC, 4)
            renderFontDrawText(font, '[{ffa500}/tx{ffffff}]: Оплатите налоги{ffffff}.', 20, PosY+70, 0xFFFFFFFF, 0x90000000)
        end

    end
end


function togglePay()

    state.active = true

    openTaxesMenu()

end



function openTaxesMenu()

    cef.sendPacket(220, 0, 80, 64)
    cef.send("launchedApp|24")
    cef.sendPacket(220, 0, 27, 64)
    cef.send("onSvelteAppInit")

end


function getLastDate()
    return cfg.lastPayed:match("^(%d+%.%d+%.%d+)") or "nil"
end

function checkPayDayTime()
    local currentMinutes = tonumber(os.date("%M"))
    return currentMinutes == 59 or currentMinutes == 0 or currentMinutes == 1 or false
end

function sampev.onShowDialog(id, style, title, button1, button2, text)

    if not state.active then return end

    if title == "{BFBBBA}" and text:find("{FFFFFF}1. Состояние основного счета") then

        if text:find("{ffff00}Оплата всех налогов{FFFFFF}") then
            local num = 0
            for line in text:gmatch("[^\r\n]+") do

                if line == "{ffff00}Оплата всех налогов{FFFFFF}" then
                    sampSendDialogResponse(id, 1, num, line)
                    return false
                end

                num = num + 1

            end
        else
            sampSendDialogResponse(id, 0)
            utils.addChat("{ff0000}Ошибка. {ffffff}В меню нет пункта '{ffff00}Оплата всех налогов{ffffff}'.")
            utils.addChat("Вступите в семью с соответствующим улучшением.")
            state.active = false
            return false
        end

    elseif title == "{BFBBBA}Оплата всех налогов" then

        sampSendDialogResponse(id, 1)

        state.active = false

        if text == "{00ff00}-{ffffff} У Вас нет налогов, которые требуется оплатить!" then
            utils.addChat("У вас нет налогов которые можно оплатить.")
        end

        return false

    end

end

function sampev.onServerMessage(color, text)

    if text == "______________________________Банковский чек______________________________" and checkPayDayTime() then


        lua_thread.create(function()
            wait(0)

            if cfg.autoPay then

                if getLastDate() ~= os.date("%d.%m.%Y") then
                    togglePay()
                end
    
            end

        end)

        cfg.screenNotify.show = true
    
        save()


    elseif text:find("^Вы оплатили все налоги на сумму") then

        text = text:gsub("(%d)[%.,](%d)", "%1%2")

        local money = tonumber(
            text:match("{ffffff}%$(%d+)")
        )

        cfg.totalPayed = cfg.totalPayed + money
        cfg.lastPayed = os.date("%d.%m.%Y [%H:%M:%S]")
        cfg.screenNotify.show = false
        save()

        if not cfg.silentMode then
            return {color, '{ffa500}'..thisScript().name..'{ffffff}: Вы оплатили налоги на сумму: {99ff99}'..utils.formatNumber(money)..'$'}
        end

    end
end

















----------------------------------------------------------------INTERFACE----------------------------------------------------------------

local fa = require('fAwesome6')

imgui.OnInitialize(function()

    imgui.GetIO().IniFilename = nil
    local config = imgui.ImFontConfig()
    config.MergeMode = true
    config.PixelSnapH = true
    iconRanges = imgui.new.ImWchar[3](fa.min_range, fa.max_range, 0)
    imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(fa.get_font_data_base85('solid'), 14, config, iconRanges)

    do
        imgui.SwitchContext()
        --==[ STYLE ]==--
        imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
        imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
        imgui.GetStyle().ItemSpacing = imgui.ImVec2(5, 5)
        imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(2, 2)
        imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(0, 0)
        imgui.GetStyle().IndentSpacing = 0
        imgui.GetStyle().ScrollbarSize = 10
        imgui.GetStyle().GrabMinSize = 10
    
        --==[ BORDER ]==--
        imgui.GetStyle().WindowBorderSize = 1
        imgui.GetStyle().ChildBorderSize = 1
        imgui.GetStyle().PopupBorderSize = 1
        imgui.GetStyle().FrameBorderSize = 0
        imgui.GetStyle().TabBorderSize = 1
    
        --==[ ROUNDING ]==--
        imgui.GetStyle().WindowRounding = 5
        imgui.GetStyle().ChildRounding = 5
        imgui.GetStyle().FrameRounding = 5
        imgui.GetStyle().PopupRounding = 5
        imgui.GetStyle().ScrollbarRounding = 5
        imgui.GetStyle().GrabRounding = 5
        imgui.GetStyle().TabRounding = 5
    
        --==[ ALIGN ]==--
        imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
        imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
        imgui.GetStyle().SelectableTextAlign = imgui.ImVec2(0.5, 0.5)
        
        --==[ COLORS ]==--
        imgui.GetStyle().Colors[imgui.Col.Text]                   = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TextDisabled]           = imgui.ImVec4(0.50, 0.50, 0.50, 1.00)
        imgui.GetStyle().Colors[imgui.Col.WindowBg]               = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ChildBg]                = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PopupBg]                = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Border]                 = imgui.ImVec4(0.25, 0.25, 0.26, 0.54)
        imgui.GetStyle().Colors[imgui.Col.BorderShadow]           = imgui.ImVec4(0.00, 0.00, 0.00, 0.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBgHovered]         = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBgActive]          = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBgActive]          = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBgCollapsed]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.MenuBarBg]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarBg]            = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrab]          = imgui.ImVec4(0.00, 0.00, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabHovered]   = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabActive]    = imgui.ImVec4(0.51, 0.51, 0.51, 1.00)
        imgui.GetStyle().Colors[imgui.Col.CheckMark]              = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SliderGrab]             = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SliderGrabActive]       = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Button]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ButtonHovered]          = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ButtonActive]           = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Header]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.HeaderHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.HeaderActive]           = imgui.ImVec4(0.47, 0.47, 0.47, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Separator]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SeparatorHovered]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SeparatorActive]        = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ResizeGrip]             = imgui.ImVec4(1.00, 1.00, 1.00, 0.25)
        imgui.GetStyle().Colors[imgui.Col.ResizeGripHovered]      = imgui.ImVec4(1.00, 1.00, 1.00, 0.67)
        imgui.GetStyle().Colors[imgui.Col.ResizeGripActive]       = imgui.ImVec4(1.00, 1.00, 1.00, 0.95)
        imgui.GetStyle().Colors[imgui.Col.Tab]                    = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabHovered]             = imgui.ImVec4(0.28, 0.28, 0.28, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabActive]              = imgui.ImVec4(0.30, 0.30, 0.30, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabUnfocused]           = imgui.ImVec4(0.07, 0.10, 0.15, 0.97)
        imgui.GetStyle().Colors[imgui.Col.TabUnfocusedActive]     = imgui.ImVec4(0.14, 0.26, 0.42, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotLines]              = imgui.ImVec4(0.61, 0.61, 0.61, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotLinesHovered]       = imgui.ImVec4(1.00, 0.43, 0.35, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotHistogram]          = imgui.ImVec4(0.90, 0.70, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotHistogramHovered]   = imgui.ImVec4(1.00, 0.60, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TextSelectedBg]         = imgui.ImVec4(1.00, 0.00, 0.00, 0.35)
        imgui.GetStyle().Colors[imgui.Col.DragDropTarget]         = imgui.ImVec4(1.00, 1.00, 0.00, 0.90)
        imgui.GetStyle().Colors[imgui.Col.NavHighlight]           = imgui.ImVec4(0.26, 0.59, 0.98, 1.00)
        imgui.GetStyle().Colors[imgui.Col.NavWindowingHighlight]  = imgui.ImVec4(1.00, 1.00, 1.00, 0.70)
        imgui.GetStyle().Colors[imgui.Col.NavWindowingDimBg]      = imgui.ImVec4(0.80, 0.80, 0.80, 0.20)
        imgui.GetStyle().Colors[imgui.Col.ModalWindowDimBg]       = imgui.ImVec4(0.00, 0.00, 0.00, 0.70)
    end
end)


local windowWidth, windowHeight = 460, 0

local imTabs = 1
local notifications = {}
imgui.OnFrame(function() return imgui_windows.main[0] end, function(self) --[[self.HideCursor = true]]
    local screenWidth, screenHeight = getScreenResolution()

    imgui.SetNextWindowSize(imgui.ImVec2(windowWidth, windowHeight), imgui.Cond.FirstUseEver)
    imgui.SetNextWindowPos(imgui.ImVec2(screenWidth/2-windowWidth/2, screenHeight/2-windowHeight/2), imgui.Cond.FirstUseEver)
    imgui.Begin('##main_windos', imgui_windows.main, imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoResize)

    imgui.customTitleBar(imgui_windows.main, resetDefaultCfg, imgui.GetWindowWidth())

    imgui.Separator()

    imgui.mainTabs(u8"Статистика", u8"Настройки")


    --imgui.SetCursorPosY(imgui.GetWindowHeight()-50)
    imgui.Separator()
    if imgui.advBanner(
        fa("FILE_CODE") .. u8(" Palatka.lua - получи 1 мес. бесплатно! ") .. fa("FILE_CODE"),
        imgui.ImVec2(-1,25),
        3,
        u8"Нажмите меня!"
    ) then
        imgui.OpenPopup('palatka_buy_popup')
    end

    if imgui.BeginPopupModal('palatka_buy_popup', nil, imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoResize) then
        imgui.TextColoredRGB("{ffffff}Вы слышали про лучший скрипт для барыг - {99ff99}Palatka.lua?\n" ..
        "{ffffff}Этот скрипт значительно упрощает вам игру на ЦР, выставляя товары и показывая средние цены.\n" ..
        "{ffffff}Для вас у меня есть уникальный промокод {99ff99}#justfedot {ffffff}который даст вам + 1 месяц подписки бесплатно!"
        )

        imgui.NewLine()

        imgui.TextColoredRGB("{ffffff}Промокод {99ff99}#justfedot{ffffff} нужно указать в переписке с автором.\nПишите прямо сейчас что-бы стать лучшим барыгой на сервере!")

        imgui.Text(u8"Связь с автором Palatka.lua:")
        imgui.SameLine()
        if imgui.Link("t.me/m/3L4CfO4gN2Y6", u8"Нажмите для копирования") then
            imgui.SetClipboardText("t.me/m/3L4CfO4gN2Y6")
            utils.addChat("Ссылка скопирована в буфер обмена.")
            imgui.addNotification(u8"Скопировано!")
        end

        -- if imgui.GetClipboardText() == "t.me/m/3L4CfO4gN2Y6" then
        --     imgui.SameLine()
        --     imgui.TextDisabled(u8("(Скопировано)"))
        -- end
        
        imgui.NewLine()

        imgui.SetCursorPosX(imgui.GetWindowWidth()/2-50)
        if imgui.Button(u8'Закрыть##closePopupModal', imgui.ImVec2(100, 0)) then
            imgui.CloseCurrentPopup()
        end
        imgui.EndPopup()
    end

    imgui.showNotifications(2)
    imgui.End()

end)







function imgui.mainTabs(...)

    __statisticTab()

    if imgui.Button(fa("GEARS") .. u8(" Настройки ") .. fa("GEARS"), imgui.ImVec2(-1, 0)) then
        imgui.OpenPopup('settings_PopUp')
    end

    if imgui.BeginPopup('settings_PopUp') then
        
        __settingsTab()

        imgui.EndPopup()
    end

    -- local tabs = {...}

    -- if imgui.BeginTabBar('Tabs') then

    --     for index, name in ipairs(tabs) do
            
    --         if imgui.BeginTabItem(name.."##mainTab"..index) then
                
    --             __tabManager(index)

    --             imgui.EndTabItem()
    --         end

    --     end

    --     imgui.EndTabBar()
    -- end


end

function __tabManager(tab)

    local tabs = {
        [1] = __statisticTab,
        [2] = __settingsTab
    }

    tabs[tab]()

end

function __statisticTab()
    -- local s = imgui.GetWindowSize()
    -- imgui.Text("W: "..s.x.." Y: "..s.y)

    imgui.TextColoredRGB(
        "{ffffff}Всего налогов оплачено: {99ff99}" .. (
            cfg.needSimplify and utils.simplifyNumber(cfg.totalPayed) or utils.formatNumber(cfg.totalPayed)
        ) .. "${ffffff}."
    )

    imgui.SameLine()

    imgui.SetCursorPosX(imgui.GetWindowWidth()-imgui.GetStyle().ItemSpacing.x-25)
    if imgui.Button(
        fa("QUESTION"),
        imgui.ImVec2(25, 0)
    ) then

        imgui.OpenPopup('faq_popup')

    end
    imgui.Hint(u8(
        "Часто задаваемые вопросы."
    ))

    if imgui.BeginPopup('faq_popup') then

        imgui.CenterTextColoredRGB(
            "{ffa500}Часто задаваемые вопросы"
        )
        imgui.Separator()

        imgui.TextColoredRGB(
            -- "{ffffff}1. {99ff99}После оплаты налогов остаётся курсор\n" ..
            -- "\t {abcdef}Скорее всего дело в том что у вас в /cars нет транспорта.\n" ..
            -- "\t {abcdef}Скрипт убирает курсор путём открытия и закрытия меню /cars.\n" ..
            -- "\t {abcdef}(( Аризона такая аризона -_- ))"

            "{ffffff}1. {99ff99}На Vice-City работает?\n" ..
            "\t {ffffff}• {abcdef}Радобоспособность не проверялась.\n" ..
            "\t {ffffff}• {abcdef}Скорее всего нет, из-за разницы в диалогах/строках чата.\n" ..
            "\t {ffffff}• {abcdef}(( Ждите обновлений. ))"
        )

        imgui.EndPopup()
    end

    imgui.TextColoredRGB("{ffffff}Последняя оплата была: {ffa500}" .. cfg.lastPayed .. "{ffffff}.")

end

function __settingsTab()


    if imgui.Checkbox(u8'Авто-оплата налогов', imcfg.autoPay) then
        cfg.autoPay = imcfg.autoPay[0]
        save()
    end
    imgui.Hint(u8(
        "Если включено, скрипт будет пытаться оплатить налоги раз в сутки."
    ))

    if imgui.Checkbox(u8'Напоминание на экране', imcfg.screenNotify.active) then
        cfg.screenNotify.active = imcfg.screenNotify.active[0]
        save()
    end
    imgui.Hint(u8(
        "Если включено, после PayDay на экране будет напоминание о оплате налогов."
    ))
    
    if imgui.Checkbox(u8'Сокращать сумму в статистике', imcfg.needSimplify) then
        cfg.needSimplify = imcfg.needSimplify[0]
        save()
    end
    imgui.Hint(u8(
        "Если включено, сумма оплаты налогов в статистике будет сокращена с помощью 'k'.\n" ..
        "Пример: 1000 = 1k"
    ))

    if imgui.Checkbox(u8'Сайлент-Мод', imcfg.silentMode) then
        cfg.silentMode = imcfg.silentMode[0]
        save()
    end
    imgui.Hint(u8(
        "Если включено, скрипт не будет ничего отправлять в чат."
    ))

end
























----------------------------------------------------------------INTERFACE SNIPPETS----------------------------------------------------------------

function imgui.customTitleBar(param, resetFunc, windowWidth)

    local imStyle = imgui.GetStyle()


    
    imgui.SetCursorPosY(imStyle.ItemSpacing.y+5)
    if imgui.Link("t.me/justfedotScript", u8("Telegram канал автора.\nНажми чтобы перейти/скопировать")) then
        imgui.addNotification(u8"Ссылка скопирована!")
        imgui.SetClipboardText("https://t.me/justfedotScript")
        os.execute(('explorer.exe "%s"'):format("https://t.me/justfedotScript"))
    end


    imgui.SameLine()
    imgui.SetCursorPosX((windowWidth - 170 - imStyle.ItemSpacing.x + imgui.CalcTextSize("t.me/justfedotScript").x)/2 - imgui.CalcTextSize(script.this.name .. ": " .. script.this.version).x/2)
    imgui.TextColoredRGB(script.this.name .. ": " .. script.this.version)


    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 170 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa('DOLLAR_SIGN').."##popup_donation_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("donationPopupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 110 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa("ARROW_TREND_DOWN").."##popup_menu_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("upWindowPupupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 50 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa("XMARK").."##close_button", imgui.ImVec2(50, 25)) then
        param[0] = false
    end


    if imgui.BeginPopup("upWindowPupupMenu") then
        
        imgui.TextColoredRGB("Доп. Функции:")
        imgui.Separator()

        if imgui.Selectable(u8("Перезагрузить скрипт").."##reloadScriptButton", false) then
            cfg.isReloaded = true
            save()
            thisScript():reload()
        end
        if imgui.Selectable(u8("Сбросить все настройки").."##resetSettingsButton", false) then
            resetFunc()
        end
    
        imgui.EndPopup()
    end

    if imgui.BeginPopup("donationPopupMenu") then
        imgui.Text(u8("Если вы желаете выразить поддержку, можете поддержать автора.\nНа данный момент доступен перевод на Укр. Карту 'Monobank'."))
        if imgui.Link("5375 4112 2231 4945", u8"Нажмите что-бы скопировать.") then
            imgui.SetClipboardText("5375411222314945")
            utils.addChat("Номер карты {99ff99}скопирован.")
            imgui.addNotification(u8"Номер карты скопирован.")
        end
        imgui.TextColoredRGB(("{ffffff}Имя на карте: {99ff99}Михайло Б.\n{ffffff}В комментарии к платежу укажите {99ff99}'"..script.this.name.."' {ffffff}что-бы я понимал актуальность скрипта.\n{ffa500}Большое спасибо за поддержку."))
    
        imgui.EndPopup()
    end


    --imgui.Separator()

    
end
function imgui.Link(label, description)
    local size, p, p2 = imgui.CalcTextSize(label), imgui.GetCursorScreenPos(), imgui.GetCursorPos()
    local result = imgui.InvisibleButton(label, size)
    imgui.SetCursorPos(p2)

    if imgui.IsItemHovered() then
        if description then
            imgui.BeginTooltip()
            imgui.PushTextWrapPos(600)
            imgui.TextUnformatted(description)
            imgui.PopTextWrapPos()
            imgui.EndTooltip()
        end
        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
        imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y + size.y), imgui.ImVec2(p.x + size.x, p.y + size.y), imgui.GetColorU32(imgui.Col.CheckMark))
    else
        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
    end

    return result
end
function imgui.addNotification(text)
    table.insert(notifications, {
        text = text,
        startTime = os.clock()
    })
end
function imgui.showNotifications(duration)
    local currentTime = os.clock()
    local activeNotifications = #notifications

    -- Начинаем отображение подсказок, если есть активные уведомления
    if activeNotifications ~= 0 then
        imgui.BeginTooltip()
    end
    for i = #notifications, 1, -1 do
        local notification = notifications[i]
        -- Проверяем, прошло ли время показа
        if currentTime - notification.startTime < duration then
            imgui.Text(notification.text)
            activeNotifications = activeNotifications + 1
            -- Если это не последнее уведомление, добавляем разделитель
            if i > 1 then
                imgui.Separator()
            end
        else
            table.remove(notifications, i)
        end
    end

    if activeNotifications ~= 0 then
        imgui.EndTooltip()
    end
end
function imgui.CenterTextColoredRGB(text)
    local c = text:gsub("{......}","")
    imgui.SetCursorPosX(imgui.GetWindowSize().x / 2 - imgui.CalcTextSize(u8(c)).x / 2)
    imgui.TextColoredRGB(text)
end
function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImVec4(r/255, g/255, b/255, a/255)
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end
--[[
    Создает анимированный баннер с заданным текстом, который плавно переливается цветами. 
    Баннер может быть выполнен в виде стандартной кнопки, кликабельного текста или кликабельного текста с выравниванием по центру. 
    При наведении на баннер его цвет инвертируется, и, при необходимости, отображается подсказка.
    Возвращает `true`, если баннер был нажат.

    Параметры:
    - text: текст, который будет отображаться на баннере.
    - size: размер баннера (объект imgui.ImVec2). Если передано (0, 0), размер будет автоматически рассчитан.
    - bannerType: тип баннера:
        1 - стандартная кнопка,
        2 - кликабельный текст,
        3 - кликабельный текст с выравниванием по центру.
    - tooltip: (необязательный) текст подсказки, который отображается при наведении на баннер.

    Пример использования:
    if imgui.advBanner("Нажми меня", imgui.ImVec2(0, 0), 2, "Это подсказка") then
        print("Баннер был нажат!")
    end
]]
function imgui.advBanner(text, size, bannerType, tooltip)
    local animationSpeed, hovered, clicked = 0.5, false, false
    local animationColor = imgui.ImVec4(1, 0, 0, 1)

    local function sinColorTransition(speed, offset)
        return (math.sin(os.clock() * speed + offset) + 1) / 2
    end

    animationColor.x, animationColor.y, animationColor.z = 
        sinColorTransition(animationSpeed, 0), 
        sinColorTransition(animationSpeed, math.pi), 
        sinColorTransition(animationSpeed, math.pi / 2)

    if bannerType == 1 then
        local textSize = imgui.CalcTextSize(text)
        size = size or imgui.ImVec2(0, 0)
        if size.x == 0 then size.x = textSize.x + 5 end
        if size.y == 0 then size.y = textSize.y + 5 end

        imgui.PushStyleColor(imgui.Col.Button, animationColor)
        imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1))
        imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1))
        imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0, 0, 0, 0))

        if imgui.Button("##banner"..text, size) then clicked = true end

        hovered = imgui.IsItemHovered()
        if hovered then animationColor = imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1) end

        imgui.PopStyleColor(4)

        local buttonMin, buttonMax, totalHeight = imgui.GetItemRectMin(), imgui.GetItemRectMax(), 0
        for line in text:gmatch("[^\r\n]+") do
            totalHeight = totalHeight + imgui.CalcTextSize(line).y
        end

        local cursorY = buttonMin.y + (size.y - totalHeight) / 2
        imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0, 0, 0, 1))
        for line in text:gmatch("[^\r\n]+") do
            local lineSize = imgui.CalcTextSize(line)
            imgui.SetCursorScreenPos(imgui.ImVec2(buttonMin.x + (buttonMax.x - buttonMin.x - lineSize.x) / 2, cursorY))
            imgui.Text(line)
            cursorY = cursorY + lineSize.y
        end
        imgui.PopStyleColor(1)
    else
        local textSize = imgui.CalcTextSize(text)
        size = size or imgui.ImVec2(0, 0)
        if size.x == 0 then size.x = textSize.x + 20 end
        if size.y == 0 then size.y = textSize.y + 15 end

        local pos = imgui.GetCursorPos()
        if bannerType == 3 then
            local windowWidth, buttonWidth = imgui.GetWindowSize().x, (size.x == -1) and imgui.GetWindowSize().x or size.x
            imgui.SetCursorPosX((windowWidth - buttonWidth) / 2)
        end

        imgui.InvisibleButton("##banner"..text, size)
        hovered = imgui.IsItemHovered()
        if hovered then animationColor = imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1) end

        imgui.PushStyleColor(imgui.Col.Text, animationColor)
        imgui.SetCursorPos(imgui.GetCursorPos())

        imgui.SetCursorPosY(pos.y + (size.y - textSize.y) / 2)

        if bannerType == 3 then
            for line in text:gmatch("[^\r\n]+") do
                local lineSize = imgui.CalcTextSize(line)
                imgui.SetCursorPosX((imgui.GetWindowSize().x - lineSize.x) / 2)
                imgui.Text(line)
            end
        else
            imgui.TextWrapped(text)
        end

        imgui.PopStyleColor(1)
        clicked = hovered and imgui.IsMouseClicked(0)
    end

    if hovered and tooltip then imgui.SetTooltip(tooltip) end
    return clicked
end
function imgui.Hint(text)
    if imgui.IsItemHovered() and not imgui.IsItemActive() then
        imgui.SetTooltip(text)
    end
end