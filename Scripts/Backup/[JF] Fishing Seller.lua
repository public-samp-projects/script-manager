script_author('JustFedot')
script_name('[JF] Fishing Seller')
script_version('1.0.0')

require("moonloader")
local sampev = require("samp.events")
local ffi = require('ffi')
local sampfuncs = require('sampfuncs')
local raknet = require('samp.raknet')
require('samp.synchronization')

local cef = (function()
    local self = {}

    local subscribe = false
    local eventFunction = nil

    addEventHandler("onSendPacket",function(id, bs, priority, reliability, orderingChannel)
        if not subscribe then return end
        if id == 220 then
            local id = raknetBitStreamReadInt8(bs)
            local packet = raknetBitStreamReadInt8(bs)
            local len = raknetBitStreamReadInt8(bs)
            raknetBitStreamIgnoreBits(bs, 24)
            local str = raknetBitStreamReadString(bs, len)
            if packet ~= 0 and packet ~= 1 and #str > 2 then
                local result = str
                if type(result) ~= "nil" then
                    return eventFunction('S', result)
                end
            end
        end
    end)
    
    addEventHandler("onReceivePacket",function(id, bs)
        if not subscribe then return end
        if id == 220 then
            raknetBitStreamIgnoreBits(bs, 8)
            if raknetBitStreamReadInt8(bs) == 17 then
                raknetBitStreamIgnoreBits(bs, 32)
                local result = raknetBitStreamReadString(bs, raknetBitStreamReadInt32(bs))
                if type(result) ~= "nil" then
                    return eventFunction('R', result)
                end
            end
        end
    end)

    --Interfaces

    function self.subscribeEvents(var, func)
        if type(var) ~= "boolean" then error('bad argument #1 to subscribe, boolean expected got: '..type(var)) end
        if var == true and type(func) ~= "function" then error('bad argument #2 to subscribe, function expected got: '..type(func)) end
        subscribe = var
        eventFunction = func or nil
        return subscribe
    end

    function self.send(str)
        if type(str) ~= "string" or #str == 0 then error('bad argument #1 to send, not empty string expected got: '..type(str)) end
		local bs = raknetNewBitStream()
		raknetBitStreamWriteInt8(bs, 220)
		raknetBitStreamWriteInt8(bs, 18)
		raknetBitStreamWriteInt32(bs, #str)
		raknetBitStreamWriteString(bs, str)
		raknetBitStreamWriteInt32(bs, 0)
		raknetSendBitStream(bs)
		raknetDeleteBitStream(bs)
	end

    function self.emulate(str)
		local bs = raknetNewBitStream()
		raknetBitStreamWriteInt8(bs, 17)
		raknetBitStreamWriteInt32(bs, 0)
		raknetBitStreamWriteInt32(bs, #str)
		raknetBitStreamWriteString(bs, str)
		raknetEmulPacketReceiveBitStream(220, bs)
		raknetDeleteBitStream(bs)
	end

    return self
end)()

do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local json = require('dkjson')

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(json.encode(table, {indent = true}))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return json.decode(content)
                else
                    return error("Could not load configuration")
                end
            else
                return false
            end
        end

        function self.update(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^.+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
			
			if loadedCfg then
				for k, v in pairs(table) do
					if loadedCfg[k] ~= nil then
						table[k] = loadedCfg[k]
					end
				end
			end

            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end
local jcfg = Jcfg()

local cfg = {
    bot = true,
}

jcfg.update(cfg)

function save()
    jcfg.save(cfg)
end

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local autoSellActive = false
local autoSellType = false
local autoSellThread = lua_thread.create_suspended(function()
    autoSellType = false
    local coords = {
        fish = {-2220.0000, 2293.0000, 5.1250},
        artifacts = {-2232.5000, 2285.3750, 5.1250},
    }
    local fish_pickup = registerPickup(coords.fish)
    local articafts_pickup = registerPickup(coords.artifacts)
    while autoSellActive do wait(0)
        if not autoSellType then
            takePickup(articafts_pickup)
        else
            takePickup(fish_pickup)
        end
        if not autoSellActive then break end
        wait(500)
    end
end)

function newZal()

    local drawArcIn3d = function(x, y, z, radius, startAngle, endAngle, polygons, width, color)
        local step = (endAngle - startAngle) / polygons
        local angle = startAngle
        local sX_old, sY_old
        while angle <= endAngle do
            local radians = math.rad(angle)
            local lX = radius * math.cos(radians) + x
            local lY = radius * math.sin(radians) + y
            local lZ = z
            local _, sX, sY, sZ, _, _ = convert3DCoordsToScreenEx(lX, lY, lZ)
            if sZ > 1 then
                if sX_old and sY_old then
                    renderDrawLine(sX, sY, sX_old, sY_old, width, color)
                end
                sX_old, sY_old = sX, sY
            end
            angle = angle + step
        end
    end

    local function findCircleIntersections(x0, y0, r0, x1, y1, r1)
        local dx, dy = x1 - x0, y1 - y0
        local d = math.sqrt(dx * dx + dy * dy)
        if d > r0 + r1 or d < math.abs(r0 - r1) or d == 0 then
            return nil
        else
            local a = (r0*r0 - r1*r1 + d*d) / (2*d)
            local h = math.sqrt(r0*r0 - a*a)
            local xm = x0 + a * dx / d
            local ym = y0 + a * dy / d
            local xs1 = xm + h * dy / d
            local ys1 = ym - h * dx / d
            local xs2 = xm - h * dy / d
            local ys2 = ym + h * dx / d
            return xs1, ys1, xs2, ys2
        end
    end

    local x1, y1, x2, y2 = findCircleIntersections(-2232.5000, 2285.3750, 13, -2220.0000, 2293.0000, 13)

    if x1 and y1 and x2 and y2 then
        local centerX1, centerY1 = -2232.5000, 2285.3750
        local centerX2, centerY2 = -2220.0000, 2293.0000
        local radius = 13
        local polygons = 36
        local width = 2
        local color = 0xFFFFFFFF

        local startAngle1 = math.deg(math.atan2(y1 - centerY1, x1 - centerX1))
        local endAngle1 = math.deg(math.atan2(y2 - centerY1, x2 - centerX1))
        
        local startAngle2 = startAngle1 + 180
        local endAngle2 = endAngle1 + 180
        
        if startAngle2 > 360 then
            startAngle2 = startAngle2 - 360
        end
        
        if endAngle2 > 360 then
            endAngle2 = endAngle2 - 360
        end
        
        drawArcIn3d(centerX1, centerY1, 5.1250, radius, startAngle1, endAngle1, polygons, width, color)
        drawArcIn3d(centerX2, centerY2, 5.1250, radius, startAngle2, endAngle2, polygons, width, color)
    end

end

local _charData = {
    char = nil,
    textId = nil,
}

local _displayCef = false

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    addChat('Загружен. Команда: {ffc0cb}/fisell{ffffff}.')
    addChat('Переключить продавца: {ffc0cb}/fitoggle{ffffff}.')
    sampRegisterChatCommand('fisell',function()
        local x, y, z = getCharCoordinates(1)
        local dist = getDistanceBetweenCoords3d(-2220.0000, 2293.0000, 5.1250, x, y, z)
        local dist2 = getDistanceBetweenCoords3d(-2232.5000, 2285.3750, 5.1250, x, y, z)
        if dist <= 13 and dist2 <= 13 then
            autoSellActive = true
            autoSellThread:run()
        else
            addChat('Вы находитесь слишком далеко от пикапов. Скрипт {ff0000}отключен{ffffff}.')
        end
    end)

    sampRegisterChatCommand('fitoggle',function()
        cfg.bot = not cfg.bot
        addChat('Показ бота '..(cfg.bot and '{99ff99}включен' or '{ff0000}отключен')..'{ffffff}.')
        if not cfg.bot then
            _deleteMerchantBot()
        end
        save()
    end)
    while true do wait(0)

        if cfg.bot then _botProcessor() end

    end
end

function _botProcessor()
    local mX, mY, mZ = getCharCoordinates(1)
    local dist = getDistanceBetweenCoords3d(mX, mY, mZ, -2229.2556152344, 2285.9262695313, 4)
    if dist <= 50 then
        if _charData.char and doesCharExist(_charData.char) then
            clearCharTasksImmediately(_charData.char)
            if dist <= 1.5 then
                if not _displayCef then
                    cef.emulate('window.executeEvent(\'cef.modals.showModal\', `["interactionSidebar",{"title": "Продавец","description":"","timer":7,"buttons":[{"title": "Продать барахло","keyTitle": "ALT","buttonColor": "#ffffff","backgroundColor": "rgba(171, 171, 171, 0.15)"}]}]`);')
                    _displayCef = true
                end
                if isKeyJustPressed(0xA4) and
                not sampIsChatInputActive() and
                not isSampfuncsConsoleActive() and
                not sampIsDialogActive() and
                not autoSellActive then
                    autoSellActive = true
                    autoSellThread:run()
                end
            else
                _displayCef = false
            end
        else
            spawnMerchantBot(-2229.2556152344, 2285.9262695313, 4, 212)
        end
    else
        _deleteMerchantBot()
    end
end

function spawnMerchantBot(x,y,z,skinid)
    requestModel(skinid)
    loadAllModelsNow()
    _charData.char = createChar(2, skinid, x,y,z)
    freezeCharPosition(_charData.char, true)
    setCharHeading(_charData.char, 180)
    _charData.textId = sampCreate3dText("{73B461}Продавец\n\n\n{ffffff}Скупщик рыбы и артефактов\n\n{cccccc}Нажмите 'ALT'",-1,x,y,z+1.5,5,false,-1,-1)
    markModelAsNoLongerNeeded(skinid)
end

function _deleteMerchantBot()
    if _charData.char then
        if doesCharExist(_charData.char) then deleteChar(_charData.char) end
        _charData.char = nil
        if sampIs3dTextDefined(_charData.textId) then sampDestroy3dText(_charData.textId) end
        _charData.textId = nil
    end
end

function registerPickup(cords)
    for id = 0, 4095 do
        local pickup = sampGetPickupHandleBySampId(id)
        if doesPickupExist(pickup) then
            local x, y, z = getPickupCoordinates(pickup)
            local bX = x == cords[1]
            local bY = y == cords[2]
            local bZ = z == cords[3]
            if bX and bY and bZ then
                return id
            end
        end
    end
end

local dbg = {false, false}

function takePickup(id)

    local pX, pY, pZ = getCharCoordinates(PLAYER_PED)

    if getDistanceBetweenCoords3d(pX, pY, pZ, getPickupCoordinates(sampGetPickupHandleBySampId(id))) <= 13 then
        sampSendPickedUpPickup(id)
        wait(50)
        local data = samp_create_sync_data('player')
        data.keysData = data.keysData + 1024
        data.send()
    else
        addChat('Вы находитесь слишком далеко от пикапов. Скрипт {ff0000}отключен{ffffff}.')
        autoSellActive = false
        dbg = {false, false}
    end

end



function sampev.onShowDialog(id, style, title, button1, button2, text)

    if not autoSellActive then return end

    if title == '{BFBBBA}{ae433d}Продажа рыбы' or title == '{BFBBBA}{ae433d}Продажа редких вещей' then
        if not text:find('[1-9]+ шт.') then
            if title == '{BFBBBA}{ae433d}Продажа рыбы' then
                dbg[1] = true
            else
                dbg[2] = true
                autoSellType = true
            end
            if dbg[1] and dbg[2] then
                autoSellActive = false
                dbg = {false, false}
            end
        end

        if text:find('{cccccc}Предмет') then

            local n = -1
            for line in text:gmatch('[^\r\n]+') do
                if line:find('Рыба') or line:find('шт%.') then
                    local count = tonumber(line:match('(%d+) шт.'))
                    if count > 0 then
                        sampSendDialogResponse(id, 1, n, nil)
                        return
                    end
                end
                n=n+1
            end

        elseif text:find('{ffffff}Введите количество продаваемой рыбы%.') or text:find('{ffffff}Введите количество продаваемых редких вещей') then
            local count = text:match('У вас в наличии: {ae433d}(%d+) шт.')
            if count then
                sampSendDialogResponse(id, 1, nil, tostring(count))
                return
            end

        elseif text:find('{ffffff}Вы действительно хотите продать рыбу') or text:find('{ffffff}Вы действительно хотите продать редкие вещи') then
            text = text:gsub("(%d),(%d)", "%1%2")
            local money = tonumber(text:match('{ffffff}Общая стоимость продажи: {ae433d}%$(%d+)') or text:match('{ffffff}С продажи вы получите: {ae433d}(%d+) рыбных монет'))
            sampSendDialogResponse(id, 1)
            return
        end
    end

    if title == '{BFBBBA}{ae433d}Продавец' then

        if autoSellActive and text:find('{ffffff} Продажа редких вещей за рыбные монеты') then
            sampSendDialogResponse(id, 1, 0)
            return
        end

    end

end

function samp_create_sync_data(sync_type, copy_from_player)

    copy_from_player = copy_from_player or true
    local sync_traits = {
        player = {'PlayerSyncData', raknet.PACKET.PLAYER_SYNC, sampStorePlayerOnfootData},
        vehicle = {'VehicleSyncData', raknet.PACKET.VEHICLE_SYNC, sampStorePlayerIncarData},
        passenger = {'PassengerSyncData', raknet.PACKET.PASSENGER_SYNC, sampStorePlayerPassengerData},
        aim = {'AimSyncData', raknet.PACKET.AIM_SYNC, sampStorePlayerAimData},
        trailer = {'TrailerSyncData', raknet.PACKET.TRAILER_SYNC, sampStorePlayerTrailerData},
        unoccupied = {'UnoccupiedSyncData', raknet.PACKET.UNOCCUPIED_SYNC, nil},
        bullet = {'BulletSyncData', raknet.PACKET.BULLET_SYNC, nil},
        spectator = {'SpectatorSyncData', raknet.PACKET.SPECTATOR_SYNC, nil}
    }
    local sync_info = sync_traits[sync_type]
    local data_type = 'struct ' .. sync_info[1]
    local data = ffi.new(data_type, {})
    local raw_data_ptr = tonumber(ffi.cast('uintptr_t', ffi.new(data_type .. '*', data)))
    -- copy player's sync data to the allocated memory
    if copy_from_player then
        local copy_func = sync_info[3]
        if copy_func then
            local _, player_id
            if copy_from_player == true then
                _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
            else
                player_id = tonumber(copy_from_player)
            end
            copy_func(player_id, raw_data_ptr)
        end
    end
    -- function to send packet
    local func_send = function()
        local bs = raknetNewBitStream()
        raknetBitStreamWriteInt8(bs, sync_info[2])
        raknetBitStreamWriteBuffer(bs, raw_data_ptr, ffi.sizeof(data))
        raknetSendBitStreamEx(bs, sampfuncs.HIGH_PRIORITY, sampfuncs.UNRELIABLE_SEQUENCED, 1)
        raknetDeleteBitStream(bs)
    end
    -- metatable to access sync data and 'send' function
    local mt = {
        __index = function(t, index)
            return data[index]
        end,
        __newindex = function(t, index, value)
            data[index] = value
        end
    }
    return setmetatable({send = func_send}, mt)
end

function onScriptTerminate(script,bool)
    if script == thisScript() then
        _deleteMerchantBot()
    end
end
