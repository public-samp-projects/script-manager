script_author('JustFedot')
script_name('[JF] Open Cases')
script_version('2.0.2')

require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local effil = require("effil")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("imgui")

function apply_custom_style()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
 
     style.WindowPadding = ImVec2(15, 15)
     style.WindowRounding = 15.0
     style.FramePadding = ImVec2(5, 5)
     style.ItemSpacing = ImVec2(12, 8)
     style.ItemInnerSpacing = ImVec2(8, 6)
     style.IndentSpacing = 25.0
     style.ScrollbarSize = 15.0
     style.ScrollbarRounding = 15.0
     style.GrabMinSize = 15.0
     style.GrabRounding = 7.0
     style.ChildWindowRounding = 8.0
     style.FrameRounding = 6.0
   
 
       colors[clr.Text] = ImVec4(0.95, 0.96, 0.98, 1.00)
       colors[clr.TextDisabled] = ImVec4(0.36, 0.42, 0.47, 1.00)
       colors[clr.WindowBg] = ImVec4(0.11, 0.15, 0.17, 1.00)
       colors[clr.ChildWindowBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
       colors[clr.Border] = ImVec4(0.43, 0.43, 0.50, 0.50)
       colors[clr.BorderShadow] = ImVec4(0.00, 0.00, 0.00, 0.00)
       colors[clr.FrameBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.FrameBgHovered] = ImVec4(0.12, 0.20, 0.28, 1.00)
       colors[clr.FrameBgActive] = ImVec4(0.09, 0.12, 0.14, 1.00)
       colors[clr.TitleBg] = ImVec4(0.09, 0.12, 0.14, 0.65)
       colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
       colors[clr.TitleBgActive] = ImVec4(0.08, 0.10, 0.12, 1.00)
       colors[clr.MenuBarBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.39)
       colors[clr.ScrollbarGrab] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
       colors[clr.ScrollbarGrabActive] = ImVec4(0.09, 0.21, 0.31, 1.00)
       colors[clr.ComboBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.CheckMark] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrab] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrabActive] = ImVec4(0.37, 0.61, 1.00, 1.00)
       colors[clr.Button] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ButtonHovered] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.ButtonActive] = ImVec4(0.06, 0.53, 0.98, 1.00)
       colors[clr.Header] = ImVec4(0.20, 0.25, 0.29, 0.55)
       colors[clr.HeaderHovered] = ImVec4(0.26, 0.59, 0.98, 0.80)
       colors[clr.HeaderActive] = ImVec4(0.26, 0.59, 0.98, 1.00)
       colors[clr.ResizeGrip] = ImVec4(0.26, 0.59, 0.98, 0.25)
       colors[clr.ResizeGripHovered] = ImVec4(0.26, 0.59, 0.98, 0.67)
       colors[clr.ResizeGripActive] = ImVec4(0.06, 0.05, 0.07, 1.00)
       colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16)
       colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39)
       colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00)
       colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
       colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
       colors[clr.PlotHistogram] = ImVec4(0.90, 0.70, 0.00, 1.00)
       colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
       colors[clr.TextSelectedBg] = ImVec4(0.25, 1.00, 0.00, 0.43)
       colors[clr.ModalWindowDarkening] = ImVec4(1.00, 0.98, 0.95, 0.73)
end
apply_custom_style()

do -- Xcfg Modified
    Xcfg = {
        _version    = 2.1,
        _author     = "Double Tap Inside",
        _modified   = "JustFedot",
        _email      = "double.tap.inside@gmail.com",
        _help = [[
            Module xcfg             = Xcfg()
            Создает и возвращает новый экземпляр модуля Xcfg.
    
            nil                     = xcfg.mkpath(Str filename)
            Создает необходимые директории для указанного пути файла.
    
            Table loaded / nil      = xcfg.load(Str filename, [Bool save = false])
            Загружает конфигурационный файл. Если 'save' установлено в true, автоматически сохраняет файл после загрузки.
    
            Bool result             = xcfg.save(Str filename, Table new)
            Сохраняет данные в конфигурационный файл.
    
            Bool result             = xcfg.insert(Str filename, (Value value or Int index), [Value value])
            Вставляет значение в конфигурационный файл. Если указан индекс, вставляет по индексу.
    
            Bool result             = xcfg.remove(Str filename, [Int index])
            Удаляет значение из конфигурационного файла. Если указан индекс, удаляет значение по индексу.
    
            Bool result             = xcfg.set(Str filename, (Int index or Str key), Value value)
            Устанавливает или обновляет значение в конфигурационном файле по ключу или индексу.
    
            Bool result             = xcfg.update(Table old, (Table new or StrFilename new), [Bool overwrite = true])
            Обновляет старую таблицу новыми значениями из другой таблицы или файла. 'overwrite' определяет, перезаписывать ли существующие значения.
    
            Bool result             = xcfg.write(Str filename, Str str)
            Пишет строку в файл, перезаписывая его содержимое.
    
            Bool result             = xcfg.append(Str filename, Str str)
            Добавляет строку в конец файла.
    
            Table                   = xcfg.setupImcfg(Table cfg)
            Создает и возвращает таблицу с элементами управления imgui на основе конфигурационной таблицы 'cfg'. Поддерживает различные типы данных, включая вложенные таблицы.
        ]]
    }
	function Xcfg.__init()
		local self = {}
		
		-- draw values
		local function draw_string(str)
			return string.format("%q", str)
		end
		
		local function is_var(key_or_index)
			if type(key_or_index) == "string" and key_or_index:match("^[_%a][_%a%d]*$") then
				return true
			
			else
				return false
			end
		end
		
		local function draw_table_key(key)
			if is_var(key) then
				return key
				
			else
				return "["..draw_key(key).."]"
			end
		end
		
		local function draw_table(tbl, tab)
			local tab = tab or ""
			local result = {}
			
			for key, value in pairs(tbl) do
				if type(value) == "string" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_string(value))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_string(value))
					end
					
				elseif type(value) == "number" or type(value) == "boolean" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, tostring(value))
						
					else
						table.insert(result, draw_table_key(key).." = "..tostring(value))
					end
				
				elseif type(value) == "table" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_table(value, tab.."\t"))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_table(value, tab.."\t"))
					end
					
				else
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_string(tostring(value)))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_string(tostring(value)))
					end
				end
			end
			
			if #result == 0 and tab == "" then
				return ""
				
			elseif #result == 0 then
				return "{}"
			
			elseif tab == "" then
				return table.concat(result, ",\n")..",\n"
			
			else
				return "{\n"..tab..table.concat(result, ",\n"..tab)..",\n"..tab:sub(2).."}"
			end       
		end
		
		local function draw_value(value, tab)
			if type(value) == "string" then
				return draw_string(value)
			
			elseif type(value) == "number" or type(value) == "boolean" or type(value) == "nil" then
				return tostring(value)
			
			elseif type(value) == "table" then
				return draw_table(value, tab)
				
			else
				return draw_string(tostring(value))
			end
		end
		
		local function draw_key(key)
			if "string" == type(key) then
				return draw_string(key)
			
			elseif "number" == type(key) then
				return tostring(key)
			end
		end
		
		
		local function draw_config(tbl)
			local result = {}
		
			for key, value in pairs(tbl) do
				
				if type(key) == "number" then
					table.insert(result, "table.insert(tbl, "..draw_value(value, "\t")..")")
				
				elseif type(key) == "string" then			
					if is_var(key) then
						table.insert(result, "tbl."..draw_table_key(key).." = "..draw_value(value, "\t"))
					
					else
						table.insert(result, "tbl"..draw_table_key(key).." = "..draw_value(value, "\t"))
					end
				end
			end
			
			if #result == 0 then
				return ""
				
			else
				return table.concat(result, "\n").."\n"
			end
		end

		function self.load(filename, overwrite)
			assert(type(filename)=="string", ("bad argument #1 to 'load' (string expected, got %s)"):format(type(filename)))
			
			if overwrite == nil then
				overwrite = false
			end
			
			local file = io.open(filename, "r")
			
			if file then
				local text = file:read("*all")
				file:close()
				local lua_code = loadstring("local tbl = {}\n"..text.."\nreturn tbl")
				
				if lua_code then
					local result = lua_code()
					
					if type(result) == "table" then
						if overwrite then
							self.save(filename, result)
						end
						
						return result
					end
				end
			end
		end
		
		function self.save(filename, new)
			assert(type(filename)=="string", ("bad argument #1 to 'table_save' (string expected, got %s)"):format(type(filename)))
			assert(type(new)=="table", ("bad argument #2 to 'table_save' (table expected, got %s)"):format(type(new)))
		
			self.mkpath(filename)
			local file = io.open(filename, "w+")
			
			if file then
				local text = draw_config(new)
				file:write(text)
				file:close()
				
				return true
			else
				return false
			end
		end
		
		function self.insert(filename, value_or_index, value)
			assert(type(filename)=="string", ("bad argument #1 to 'insert' (string expected, got %s)"):format(type(filename)))
			
			if value then
				assert(type(value_or_index)=="number", ("bad argument #2 to 'insert' (number expected, got %s)"):format(type(value_or_index)))
			end
			
			local result
			
			if value then
				result = "table.insert(tbl, "..value_or_index..", "..draw_value(value, "\t")..")"
				
			else
				result = "table.insert(tbl, "..draw_value(value_or_index, "\t")..")"
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.remove(filename, index)
			assert(type(filename)=="string", ("bad argument #1 to 'remove' (string expected, got %s)"):format(type(filename)))
			assert(type(index)=="number" or index == nil, ("bad argument #2 to 'remove' (number or nil expected, got %s)"):format(type(index)))
			local result
			
			if index then
				result = "table.remove(tbl, "..index..")"
				
			else
				result = "table.remove(tbl)"
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.set(filename, key, value)
			assert(type(filename)=="string", ("bad argument #1 to 'set' (string expected, got %s)"):format(type(filename)))
			assert(type(key)=="number" or type(key)=="string", ("bad argument #2 to 'set' (number or string expected, got %s)"):format(type(key)))
			
			local result
					
			if is_var() then
				result = "tbl."..tostring(var).." = "..draw_value(value, "\t")
			
			else
				result = "tbl["..draw_key(key).."] = "..draw_value(value, "\t")
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.mkpath(filename)
			assert(type(filename)=="string", ("bad argument #1 to 'mkpath' (string expected, got %s)"):format(type(filename)))
		
			local sep, pStr = package.config:sub(1, 1), ""
			local path = filename:match("(.+"..sep..").+$") or filename
			
			for dir in path:gmatch("[^" .. sep .. "]+") do
				pStr = pStr .. dir .. sep
				createDirectory(pStr)
			end
		end

		function self.update(old, new, overwrite)
			assert(type(old)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(old)))
			assert(type(new)=="string" or type(new)=="table", ("bad argument #2 to 'update' (string or table expected, got %s)"):format(type(new)))
			
			if overwrite == nil then
				overwrite = true
			end
		
			if type(new) == "table" then
				if overwrite then
					for key, value in pairs(new) do
						old[key] = value
					end
					
				else
					for key, value in pairs(new) do
						if old[key] == nil then
							old[key] = value
						end
					end
				end
				
				return true
				
			elseif type(new) == "string" then
				local loaded = self.load(new)
				
				if loaded then
					if overwrite then
						for key, value in pairs(loaded) do
							old[key] = value
						end
						
					else
						for key, value in pairs(loaded) do
							if old[key] == nil then
								old[key] = value
							end
						end
					end
					
					return true
				end
			end
			
			return false
		end
		
		function self.append(filename, str)
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(str)
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.write(filename, str)
			self.mkpath(filename)
			local file = io.open(filename, "w+")
			
			if file then
				file:write(str)
				file:close()
				return true
				
			else
				return false
			end
		end

        function self.setupImcfg(cfg)
            assert(type(cfg) == "table", ("bad argument #1 to 'setupImcfg' (table expected, got %s)"):format(type(cfg)))
            local function setupImcfgRecursive(cfg)
                local imcfg = {}
                for k, v in pairs(cfg) do
                    if type(v) == "table" then
                        imcfg[k] = setupImcfgRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        assert(false, ("Unsupported type for imcfg: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImcfgRecursive(cfg)
        end
		
		return self
	end
	setmetatable(Xcfg, {
	__call = function(self)
		return self.__init()
	end
})
end
local xcfg = Xcfg()

local filename = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.cfg'

local imgui_windows = {
    main = imgui.ImBool(false),
}

local cfg = {
    cases = {},
    noCloseInventory = false,
    waitTime = 60,
    isKeysNeeded = false,
    randomTime = {
        min = 5,
        max = 30,
    },
}
xcfg.update(cfg,filename)

function saveConfig()
    xcfg.save(filename,cfg)
end

local imcfg = xcfg.setupImcfg(cfg)

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local state = {
    active = false,
    inspect = false,
    state = "None",
    chat = "",
    keysBuy = false,
    keysTextdrawId = nil,
    keysBuySummary = 0,
}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    addChat('Загружен. Команда: {ffc0cb}/cs /css{ffffff}.')
    sampRegisterChatCommand('css',function() imgui_windows.main.v = not imgui_windows.main.v end)
    sampRegisterChatCommand('cs',function()
        startScript()
    end)
    sampRegisterChatCommand('csbuy',function(arg)
        if state.keysBuy then
            state.keysBuy = false
            state.keysTextdrawId = nil
            addChat('Закончили упражнение. Потрачено денег: {ffa500}'..formatMoney(state.keysBuySummary)..'$')
            state.keysBuySummary = 0
            return
        end
        if not state.keysBuy and arg and arg:find('^%d+$') then
            state.keysBuy = tonumber(arg)
            addChat('Покупаем ключи {ffa500}'..arg..' {ffffff}раз.')
            addChat('Откройте меню 24/7 и нажмите на ключи.')
            lua_thread.create(function()
                while state.keysBuy do wait(0)
                    if state.keysTextdrawId and sampTextdrawIsExists(state.keysTextdrawId) then
                        wait(300)
                        sampSendClickTextdraw(state.keysTextdrawId)
                    end
                end
            end)
        else
            return addChat('Неверный синтаксис. Пример: {ffa500}/csbuy 10')
        end
    end)
    while true do wait(0)
        imgui.Process = imgui_windows.main.v

        if state.inspect then
            local res = drawInventoryButtons(2125, 2209)

            if res then
                table.insert(res, "")
                table.insert(cfg.cases, res)
                addChat('{99ff99}[Успех] {ffffff}Предмет {ffc0cb}"'..res[1]..'"{ffffff} добавлен.')
                saveConfig()
                imcfg = xcfg.setupImcfg(cfg)
            end
            if not checkInventoryWindow() then
                addChat('{FF0000}[Ошибка] {ffffff}Не найдено {ffc0cb}"Окно инвентаря"{ffffff}.')
                addChat('{FFFF00}[Внимание] {ffffff}Для работы функции {ffc0cb}"Окно инвентаря"{ffffff} должно быть открыто.')
                state.inspect = false
            end
        end

    end
end

function startScript()
    if state.active then
        state.active = false
        addChat('{99ff99}[Успех] {ffffff}Отключено {ffc0cb}"Автооткрытие"{ffffff} кейсов.')
    else
        if not cfg.cases[1] then
            return addChat('{FF0000}[Ошибка] {ffffff}У вас нет {ffc0cb}"Предметов"{ffffff} для открытия.')
        end
        state.active = true
        state.state = "None"
        state.chat = ''
        activate()
        addChat('{99ff99}[Успех] {ffffff}Включено {ffc0cb}"Автооткрытие"{ffffff} кейсов.')
        if cfg.isKeysNeeded then
            sampShowDialog(6553535, thisScript().name, [[{FFFF00}[Внимание]
{ffffff}У вас {ff0000}закончились {ffc0cb}"Ключи от Тайника"{ffffff}.
{ffffff}Рекомендуется купить {ffc0cb}"Ключи от Тайника"{ffffff} в любом 24/7.
Скрипт продолжит свою работу, но тайники открыты не будут. {abcdef}(Сундуки - будут)
{ffffff}После того как вы купили ключи, нажмите соответствующую кнопку в {ffc0cb}"/css" {abcdef}(Вкладка "Основная")]], 'Принято', nil, 0)
        end
    end
end

local w,h = getScreenResolution()
local window_width,window_height = 740,360
local imTabs = 1
function imgui.OnDrawFrame()

    if imgui_windows.main.v then


        imgui.SetNextWindowSize(imgui.ImVec2(window_width,window_height), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2-window_width/2, h/2-window_height/2), imgui.Cond.FirstUseEver)
        imgui.Begin(u8(thisScript().name)..' Ver: '..thisScript().version.." ##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar)

        imTabs = imgui.Tabs(3, imTabs, u8'Основная', u8'Настройки', u8'Кейсы')


        imgui.BeginChild('##mainChild', imgui.ImVec2(-1, -1), true)

        if imTabs==1 then

            imgui.TextColoredRGB(state.active and
            "Сейчас скрипт: {99ff99}Включен.\nТекущая стадия: {ffa500}"..state.state..'.' or
            "Сейчас скрипт: {ff0000}Отключен.")

            if imgui.Checkbox(u8'Активация', imgui.ImBool(state.active)) then
                startScript()
            end

            -- if imgui.Button(u8'Дебаг Ключей', imgui.ImVec2(0,0)) then
            --     cfg.isKeysNeeded = true
            -- end

            if cfg.isKeysNeeded then
                if imgui.Button(u8'Я купил ключи', imgui.ImVec2(0,0)) then
                    cfg.isKeysNeeded = false
                    saveConfig()
                end
                if imgui.IsItemHovered() then
                    imgui.SetTooltip(u8('Если нажмёте на эту кнопку, это будет означать что вы купили ключи.\nНапоминание при запуске показываться не будет.'))
                end
            end

            imgui.Text(u8('Этот скрипт предназначен для автоматического открытия предметов.\nОсновные функции:\n- /csbuy - Закупить ключи для тайников\n- Активация скрипта командой /cs\n- Доступ к настройкам скрипта через команду /css\n- Возможность добавления и удаления предметов для открытия.\nP.S. Подходит также для открытия различных предметов.(Например -> Аксессуар "Обрез")'))

        elseif imTabs==2 then
    
            imgui.CenterTextColoredRGB('Добавление хуеты')

            if imgui.EncCheckbox('Инспектор Инвентаря', imgui.ImBool(state.inspect), 'Активирует режим инспектора инвентаря.\nВ этом режиме появляются кликабельные квадраты поверх инвентаря, на которые нужно нажать для добавления предмета в скрипт.') then
                if state.inspect then
                    state.inspect = false
                else
                    if checkInventoryWindow() then
                        state.inspect = true
                        addChat('{99ff99}[Успех] {ffffff}Включен {ffc0cb}"Инспектор"{ffffff} кейсов.')
                    else
                        addChat('{FF0000}[Ошибка] {ffffff}Не найдено {ffc0cb}"Окно инвентаря"{ffffff}.')
                        addChat('{FFFF00}[Внимание] {ffffff}Для работы функции {ffc0cb}"Окно инвентаря"{ffffff} должно быть открыто.')
                    end
                end
            end

            imgui.Separator()

            imgui.CenterTextColoredRGB('Настройки инвентаря')

            if imgui.EncCheckbox('Не закрывать окошко', imcfg.noCloseInventory, 'При активации, окно инвентаря не будет автоматически закрываться.\nПредназначено для АФК режима игры.') then
                cfg.noCloseInventory = imcfg.noCloseInventory.v
                saveConfig()
            end            

            imgui.Separator()

            imgui.CenterTextColoredRGB("Настройки времени")

            if state.active then
                imgui.TextDisabled(u8'Эти параметры нельзя изменять во время работы скрипта!')
            else
                if imgui.SliderInt(u8'Минимальное время ожидания##waitTimeSlider', imcfg.waitTime, 0, 120) then
                    cfg.waitTime = imcfg.waitTime.v
                    saveConfig()
                end
                if imgui.IsItemHovered() and not imgui.IsItemActive() then
                    imgui.SetTooltip(u8("Основное время ожидания перед повторным открытием кейсов (в минутах).\nУстановите значение, чтобы контролировать интервал между открытиями.\nP.S Если вам трудно установить точные значения на слайдерах, попробуйте нажать Ctrl+ЛКМ на слайдер для ручного ввода."))
                end

                -- if imgui.SliderInt(u8'Минимальное добавленное время##minRandomTimeSlider', imcfg.randomTime.min, 0, 120) then
                --     if imcfg.randomTime.min.v > imcfg.randomTime.max.v then
                --         imcfg.randomTime.min.v = math.max(imcfg.randomTime.max.v - 1, 0)
                --     end
                --     cfg.randomTime.min = imcfg.randomTime.min.v
                --     saveConfig()
                -- end
                -- if imgui.IsItemHovered() and not imgui.IsItemActive() then
                --     imgui.SetTooltip(u8("Минимальное дополнительное время (в минутах) для рандомизации задержки открытия.\nПомогает избежать подозрений со стороны администрации.\nЕсли вам не нужна рандомизация вообще, в обоих значениях выставите 0.\nP.S Если вам трудно установить точные значения на слайдерах, попробуйте нажать Ctrl+ЛКМ на слайдер для ручного ввода."))
                -- end
                
                if imgui.SliderInt(u8'Добавленное время##maxRandomTimeSlider', imcfg.randomTime.max, 0, 120) then
                    if imcfg.randomTime.max.v < imcfg.randomTime.min.v then
                        imcfg.randomTime.max.v = math.min(imcfg.randomTime.min.v + 1, 120)
                    end
                    cfg.randomTime.max = imcfg.randomTime.max.v
                    saveConfig()
                end
                if imgui.IsItemHovered() and not imgui.IsItemActive() then
                    imgui.SetTooltip(u8("Дополнительное время (в минутах) для рандомизации задержки открытия.\nПомогает избежать подозрений со стороны администрации.\nЕсли вам не нужна рандомизация вообще, в обоих значениях выставите 0.\nP.S Если вам трудно установить точные значения на слайдерах, попробуйте нажать Ctrl+ЛКМ на слайдер для ручного ввода."))
                end
            end

        elseif imTabs==3 then
            if state.active then
                imgui.TextDisabled(u8'Эти параметры нельзя изменять во время работы скрипта!')
            else
                for k, v in pairs(cfg.cases) do

                    imgui.Text(u8(v[1]))
                    if imgui.InputText('##chatTrigger'..k, imcfg.cases[k][7]) then
                        imcfg.cases[k][7].v=imcfg.cases[k][7].v:gsub('%s','')
                        v[7] = u8:decode(imcfg.cases[k][7].v)
                        saveConfig()
                    end
                    if imgui.IsItemHovered() and not imgui.IsItemActive() then
                        imgui.SetTooltip(u8('Триггер чата, по которому скрипт определяет открытие предмета.\nМожете взять из чат-лога. Примеры: "[Ошибка] Время использования не прошло" и "[Успешно] Вы открыли сундук Великого Федота".\nОставьте поле пустым для работы по задержкам, не учитывая чат.\nДля нескольких триггеров разделите их символом |, например: "Строка 1|Строка 2".\nЯ использую просто "[Ошибка]|[Информация]|[Успешно]", под мои сундуки и аксы подходит.(у меня не все тайники есть)\nP.S. После ввода вашей строки, пробелы удалятся. Это не баг, так и должно быть.'))
                    end 
                    imgui.Text(u8'Модель: '..v[2])
                    imgui.Text(u8('Ротация: '..v[3]..' | '..v[4]..' | '..v[5]))
                    imgui.Text(u8('Текст: '..v[6]))
                    if imgui.Button(u8'Удалить##deteleCaseButton'..k, imgui.ImVec2(0,0)) then
                        table.remove(cfg.cases, k)
                        saveConfig()
                    end
                    imgui.Separator()

                end
            end
        end


        imgui.EndChild()

        imgui.End()

    end

end

function imgui.CenterTextColoredRGB(text)
    local width = imgui.GetWindowWidth()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local textsize = w:gsub('{.-}', '')
            local text_width = imgui.CalcTextSize(u8(textsize))
            imgui.SetCursorPosX( width / 2 - text_width .x / 2 )
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else
                imgui.Text(u8(w))
            end
        end
    end
    render_text(text)
end
function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end

function imgui.Tabs(count, tabs, ...)
    local buttonLabels = {...}
    local style = imgui.GetStyle()
    local windowWidth = imgui.GetWindowWidth() - style.WindowPadding.x * 2
    local minButtonWidth = 0

    -- Определение самой длинной надписи
    for i = 1, count do
        local labelWidth = imgui.CalcTextSize(buttonLabels[i] or "Button "..i).x + style.FramePadding.x * 2
        if labelWidth > minButtonWidth then
            minButtonWidth = labelWidth
        end
    end

    -- Расчет количества кнопок в строке и общего количества строк
    local buttonsPerRow = math.floor(windowWidth / minButtonWidth)
    if buttonsPerRow < 1 then buttonsPerRow = 1 end  -- Убедиться, что хотя бы одна кнопка помещается в строку
    local rows = math.ceil(count / buttonsPerRow)

    for row = 1, rows do
        local buttonsInThisRow = math.min(buttonsPerRow, count - (row - 1) * buttonsPerRow)
        local buttonWidth = (windowWidth - (buttonsInThisRow - 1) * style.ItemSpacing.x) / buttonsInThisRow

        -- Отрисовка кнопок в строке
        for i = 1, buttonsInThisRow do
            local buttonIndex = (row - 1) * buttonsPerRow + i
            local buttonStyle = imgui.GetStyle()
            local needStylePop = false

            if tabs == buttonIndex then
                imgui.PushStyleColor(imgui.Col.Button, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonHovered, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonActive, buttonStyle.Colors[imgui.Col.CheckMark])
                needStylePop = true
            end

            local buttonLabel = buttonLabels[buttonIndex] or "Button "..buttonIndex
            local buttonPressed = imgui.Button(buttonLabel, imgui.ImVec2(buttonWidth, 0))

            if needStylePop then
                imgui.PopStyleColor(3)
            end

            if buttonPressed and tabs ~= buttonIndex then
                tabs = buttonIndex
            end

            if i < buttonsInThisRow then
                imgui.SameLine()
            end
        end

        -- Переход на новую строку, если это не последняя
        -- if row < rows then
        --     imgui.NewLine()
        -- end
    end

    return tabs
end

function checkInventoryWindow()
    for id = 2000, 2200 do
        if sampTextdrawIsExists(id) then
            local text = sampTextdrawGetString(id)
            if text:find('INVENTORY') or text:find('…H‹EHЏAP’', 1, false) then return true end
        end
    end
    return false
end

function imgui.EncCheckbox(name, cmd, hint)
    if imgui.Checkbox(u8(name), cmd) then
        return true
    end
    if imgui.IsItemHovered() then
        imgui.SetTooltip(u8(hint))
    end
end

function drawInventoryButtons(start, final)

    for id = start, final do
        if sampTextdrawIsExists(id) then
            local model, rotX, rotY, rotZ = sampTextdrawGetModelRotationZoomVehColor(id)

            local text = sampTextdrawGetString(id)


            if model ~= 0 then
                local x,y = sampTextdrawGetPos(id)
                x, y = convertGameScreenCoordsToWindowScreenCoords(x, y)
                local width, height = 30,30
                local isHovered = false
                local curX, curY = getCursorPos()
                if curX >= x and curX <= x + width and curY >= y and curY <= y + height then
                    isHovered = true
                end
            
                local borderColor = isHovered and 0xFFFF0000 or 0xFFFFFFFF
                
                if isHovered and wasKeyPressed(1) then
                    return {'Хуета номер: '..(#cfg.cases + 1), model, rotX, rotY, rotZ, text}
                end
            
                renderDrawBox(x, y, width, height, borderColor)
            end


        end
    end

end

local isOpening = false
function activate()
    lua_thread.create(function()

        local off = function()
            if not checkInventoryWindow() then
                state.active = false
                isOpening = false
                addChat('{FF0000}[Ошибка] {ffffff}Не найдено {ffc0cb}"Окно инвентаря"{ffffff}. Скрипт отключен.')
                addChat('{FFFF00}[Внимание] {ffffff}Для работы функции {ffc0cb}"Окно инвентаря"{ffffff} должно быть открыто.')
                if not cfg.noCloseInventory then
                    addChat('Пока скрипт не закончит открытие кейсов. Далее он закроет окно сам.')
                end
                return
            end 
        end

        while state.active do wait(0)

            if not checkInventoryWindow() then
                state.state = "Пытаюсь открыть окно инвентаря..."
                repeat sampSendChat('/invent') wait(1500) until checkInventoryWindow() or not state.active
                isOpening = false
            end

            isOpening = true
            local clickedTexdraws = {}
            state.state = "Открываю хуету..."
            for k, v in pairs(cfg.cases) do
                wait(500)
                if not state.active then isOpening = false return end
                off()
                for id = 2125, 2209 do
                    if not state.active then isOpening = false return end
                    if sampTextdrawIsExists(id) then
                        local model, rotX, rotY, rotZ = sampTextdrawGetModelRotationZoomVehColor(id)
                        local text = sampTextdrawGetString(id)
                        if model == v[2] and
                        rotX == v[3] and
                        rotY == v[4] and
                        rotZ == v[5] and
                        text == v[6] then
                            if not clickedTexdraws[id] then
                                clickedTexdraws[id] = true
                                sampSendClickTextdraw(id)
                                if v[7] ~= '' then
                                    state.chat = v[7]
                                else
                                    wait(1000)
                                end
                                while state.active and state.chat ~= '' do state.state = 'Открываю хуету... {abcdef}(Жду триггера чата: "'..state.chat..'")' wait(500) end
                                state.state = "Открываю хуету..."
                                break
                            end
                        end
                    end
                end
            end
            isOpening = false
            if not cfg.noCloseInventory then
                for i=1,2 do sampSendClickTextdraw(65535) end
            end

            local randomEnd = cfg.waitTime*60+randomizeValue(0, cfg.randomTime.max*60)
            local startTime = os.clock()
            while state.active do wait(0)
                local timeLeft = os.clock() - startTime
                if timeLeft >= randomEnd then
                    break
                end
                state.state = "Жду перед повторным открытием: "..formatTime(randomEnd - timeLeft)
            end
        end
        isOpening = false
    end)
end

function formatTime(seconds)
    local hours = math.floor(seconds / 3600)
    local minutes = math.floor((seconds % 3600) / 60)
    local seconds = seconds % 60
    return string.format("%02d:%02d:%02d", hours, minutes, seconds)
end

function randomizeValue(min, max)
    math.randomseed(os.time())

    for i=1, 10 do
        math.random()
    end

    return math.random(min, max)
end

function sampev.onShowTextDraw(id, data)
    if state.active and isOpening then
        if data.text == 'USE' or data.text == '…CЊO‡’€O‹AЏ’' then 
			lua_thread.create(function()
                wait(500)
                sampSendClickTextdraw(id + 1)
            end)
		end
    else
        isOpening = false
    end
end
function formatMoney(num)
    if not num then return "0" end
    return string.format("%d", num):reverse():gsub("(%d%d%d)", "%1,"):reverse():gsub("^,", "")
end


function sampev.onSendClickTextDraw(id)
    if state.inspect then return false end
    if state.keysBuy and not state.keysTextdrawId then state.keysTextdrawId = id addChat('Я запомнил.') end
end

function sampev.onShowDialog(id, style, title, button1, button2, text)
    if state.active then
        if title:find('{BFBBBA}{ff0000}Ошибка') and text:find('{ffffff}Для открытия тайника необходимо иметь ключ для открытия тайника') then
            sampSendDialogResponse(id, 1)
            if not cfg.isKeysNeeded then
                cfg.isKeysNeeded = true
                saveConfig()
            end
            return false
        elseif text:find('При использовании') and text:find('вы дополнительно получили предмет') and text:find('4 ДОНАТ') then
            sampSendDialogResponse(id, 1)
            return false
        end
    end

    if state.keysBuy then
        if title:find('{BFBBBA}Покупка предмета') then
            if text:find('{FFFFFF}Предмет: {FF332C}Ключ для тайника{ffffff}') then
                sampSendDialogResponse(id,1)
                return
            end
        elseif title:find('{BFBBBA}') then
            if text:find('{FFFFFF}Вы успешно купили {73B461}Ключ для тайника') then
                text = text:gsub("(%d),(%d)", "%1%2")
                state.keysBuy = state.keysBuy - 1
                state.keysBuySummary = state.keysBuySummary + tonumber(text:match('за {73B461}%$(%d+){FFFFFF}'))
                if state.keysBuy <= 0 then
                    state.keysBuy = false
                    state.keysTextdrawId = nil
                    addChat('Закончили упражнение. Потрачено денег: {ffa500}'..formatMoney(state.keysBuySummary)..'$')
                    state.keysBuySummary = 0
                end
                sampSendDialogResponse(id, 1)
                return
            end
        end
    end
end

function sampev.onServerMessage(color, text)
    if state.active then
        if state.chat ~= '' then
            local originalText = text:gsub('%s', '')
            for element in string.gmatch(state.chat, "[^|]+") do
                if originalText:find(element, 1, true) then
                    state.chat = ''
                    return
                end
            end
        end
    end
    if state.keysBuy and text == '[Ошибка] {FFFFFF}Подождите немного...' then return false end
end