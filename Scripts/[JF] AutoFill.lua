script_author('JustFedot')
script_name('[JF] AutoFill')
script_version('1.0.0')

require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local imgui = require("imgui")
local encoding = require('encoding')
encoding.default =('CP1251' )
u8 = encoding.UTF8

function jfTheme()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
 
     style.WindowPadding = ImVec2(15, 15)
     style.WindowRounding = 15.0
     style.FramePadding = ImVec2(5, 5)
     style.ItemSpacing = ImVec2(12, 8)
     style.ItemInnerSpacing = ImVec2(8, 6)
     style.IndentSpacing = 25.0
     style.ScrollbarSize = 15.0
     style.ScrollbarRounding = 15.0
     style.GrabMinSize = 15.0
     style.GrabRounding = 7.0
     style.ChildWindowRounding = 8.0
     style.FrameRounding = 6.0
   
 
       colors[clr.Text] = ImVec4(0.95, 0.96, 0.98, 1.00)
       colors[clr.TextDisabled] = ImVec4(0.36, 0.42, 0.47, 1.00)
       colors[clr.WindowBg] = ImVec4(0.11, 0.15, 0.17, 1.00)
       colors[clr.ChildWindowBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
       colors[clr.Border] = ImVec4(0.43, 0.43, 0.50, 0.50)
       colors[clr.BorderShadow] = ImVec4(0.00, 0.00, 0.00, 0.00)
       colors[clr.FrameBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.FrameBgHovered] = ImVec4(0.12, 0.20, 0.28, 1.00)
       colors[clr.FrameBgActive] = ImVec4(0.09, 0.12, 0.14, 1.00)
       colors[clr.TitleBg] = ImVec4(0.09, 0.12, 0.14, 0.65)
       colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
       colors[clr.TitleBgActive] = ImVec4(0.08, 0.10, 0.12, 1.00)
       colors[clr.MenuBarBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.39)
       colors[clr.ScrollbarGrab] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
       colors[clr.ScrollbarGrabActive] = ImVec4(0.09, 0.21, 0.31, 1.00)
       colors[clr.ComboBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.CheckMark] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrab] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrabActive] = ImVec4(0.37, 0.61, 1.00, 1.00)
       colors[clr.Button] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ButtonHovered] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.ButtonActive] = ImVec4(0.06, 0.53, 0.98, 1.00)
       colors[clr.Header] = ImVec4(0.20, 0.25, 0.29, 0.55)
       colors[clr.HeaderHovered] = ImVec4(0.26, 0.59, 0.98, 0.80)
       colors[clr.HeaderActive] = ImVec4(0.26, 0.59, 0.98, 1.00)
       colors[clr.ResizeGrip] = ImVec4(0.26, 0.59, 0.98, 0.25)
       colors[clr.ResizeGripHovered] = ImVec4(0.26, 0.59, 0.98, 0.67)
       colors[clr.ResizeGripActive] = ImVec4(0.06, 0.05, 0.07, 1.00)
       colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16)
       colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39)
       colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00)
       colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
       colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
       colors[clr.PlotHistogram] = ImVec4(0.90, 0.70, 0.00, 1.00)
       colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
       colors[clr.TextSelectedBg] = ImVec4(0.25, 1.00, 0.00, 0.43)
end
function violetTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    colors[clr.Text]                 = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
    colors[clr.WindowBg]             = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.ChildWindowBg]        = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.PopupBg]              = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.Border]               = ImVec4(0.71, 0.71, 0.71, 0.40)
    colors[clr.BorderShadow]         = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.FrameBg]              = ImVec4(0.34, 0.30, 0.34, 0.30)
    colors[clr.FrameBgHovered]       = ImVec4(0.22, 0.21, 0.21, 0.40)
    colors[clr.FrameBgActive]        = ImVec4(0.20, 0.20, 0.20, 0.44)
    colors[clr.TitleBg]              = ImVec4(0.52, 0.27, 0.77, 0.82)
    colors[clr.TitleBgActive]        = ImVec4(0.55, 0.28, 0.75, 0.87)
    colors[clr.TitleBgCollapsed]     = ImVec4(9.99, 9.99, 9.90, 0.20)
    colors[clr.MenuBarBg]            = ImVec4(0.27, 0.27, 0.29, 0.80)
    colors[clr.ScrollbarBg]          = ImVec4(0.08, 0.08, 0.08, 0.60)
    colors[clr.ScrollbarGrab]        = ImVec4(0.54, 0.20, 0.66, 0.30)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.21, 0.21, 0.21, 0.40)
    colors[clr.ScrollbarGrabActive]  = ImVec4(0.80, 0.50, 0.50, 0.40)
    colors[clr.ComboBg]              = ImVec4(0.20, 0.20, 0.20, 0.99)
    colors[clr.CheckMark]            = ImVec4(0.89, 0.89, 0.89, 0.50)
    colors[clr.SliderGrab]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.SliderGrabActive]     = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Button]               = ImVec4(0.48, 0.25, 0.60, 0.60)
    colors[clr.ButtonHovered]        = ImVec4(0.67, 0.40, 0.40, 1.00)
    colors[clr.ButtonActive]         = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Header]               = ImVec4(0.56, 0.27, 0.73, 0.44)
    colors[clr.HeaderHovered]        = ImVec4(0.78, 0.44, 0.89, 0.80)
    colors[clr.HeaderActive]         = ImVec4(0.81, 0.52, 0.87, 0.80)
    colors[clr.Separator]            = ImVec4(0.42, 0.42, 0.42, 1.00)
    colors[clr.SeparatorHovered]     = ImVec4(0.57, 0.24, 0.73, 1.00)
    colors[clr.SeparatorActive]      = ImVec4(0.69, 0.69, 0.89, 1.00)
    colors[clr.ResizeGrip]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.ResizeGripHovered]    = ImVec4(1.00, 1.00, 1.00, 0.60)
    colors[clr.ResizeGripActive]     = ImVec4(1.00, 1.00, 1.00, 0.89)
    colors[clr.CloseButton]          = ImVec4(0.33, 0.14, 0.46, 0.50)
    colors[clr.CloseButtonHovered]   = ImVec4(0.69, 0.69, 0.89, 0.60)
    colors[clr.CloseButtonActive]    = ImVec4(0.69, 0.69, 0.69, 1.00)
    colors[clr.PlotLines]            = ImVec4(1.00, 0.99, 0.99, 1.00)
    colors[clr.PlotLinesHovered]     = ImVec4(0.49, 0.00, 0.89, 1.00)
    colors[clr.PlotHistogram]        = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.TextSelectedBg]       = ImVec4(0.54, 0.00, 1.00, 0.34)
    colors[clr.ModalWindowDarkening] = ImVec4(0.20, 0.20, 0.20, 0.34)
end
function defaultTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    imgui.SwitchContext()

    style.WindowRounding = 9
    style.FrameRounding = 0
    style.ItemSpacing = imgui.ImVec2(8,4)
    style.ButtonTextAlign = imgui.ImVec2(0.5,0.5)
    style.IndentSpacing = 21
    style.WindowTitleAlign = imgui.ImVec2(0,0.5)
    style.ColumnsMinSpacing = 6
    style.WindowMinSize = imgui.ImVec2(32,32)
    style.ScrollbarSize = 16
    style.DisplaySafeAreaPadding = imgui.ImVec2(4,4)
    style.FramePadding = imgui.ImVec2(4,3)
    style.GrabRounding = 0
    style.ChildWindowRounding = 0
    style.TouchExtraPadding = imgui.ImVec2(0,0)
    style.ScrollbarRounding = 9
    style.WindowPadding = imgui.ImVec2(8,8)
    style.GrabMinSize = 10
    style.DisplayWindowPadding = imgui.ImVec2(22,22)
    style.ItemInnerSpacing = imgui.ImVec2(4,4)

    colors[imgui.Col.ScrollbarGrab] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.30000001192093)
    colors[imgui.Col.FrameBgActive] = imgui.ImVec4(0.89999997615814,0.64999997615814,0.64999997615814,0.44999998807907)
    colors[imgui.Col.ButtonHovered] = imgui.ImVec4(0.6700000166893,0.40000000596046,0.40000000596046,1)
    colors[imgui.Col.PlotHistogram] = imgui.ImVec4(0.89999997615814,0.69999998807907,0,1)
    colors[imgui.Col.ButtonActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,1)
    colors[imgui.Col.ResizeGripActive] = imgui.ImVec4(1,1,1,0.89999997615814)
    colors[imgui.Col.FrameBg] = imgui.ImVec4(0.80000001192093,0.80000001192093,0.80000001192093,0.30000001192093)
    colors[imgui.Col.TextDisabled] = imgui.ImVec4(0.60000002384186,0.60000002384186,0.60000002384186,1)
    colors[imgui.Col.ResizeGripHovered] = imgui.ImVec4(1,1,1,0.60000002384186)
    colors[imgui.Col.PlotHistogramHovered] = imgui.ImVec4(1,0.60000002384186,0,1)
    colors[imgui.Col.PlotLines] = imgui.ImVec4(1,1,1,1)
    colors[imgui.Col.SliderGrab] = imgui.ImVec4(1,1,1,0.30000001192093)
    colors[imgui.Col.CloseButton] = imgui.ImVec4(0.5,0.5,0.89999997615814,0.5)
    colors[imgui.Col.TextSelectedBg] = imgui.ImVec4(0,0,1,0.34999999403954)
    colors[imgui.Col.ModalWindowDarkening] = imgui.ImVec4(0.20000000298023,0.20000000298023,0.20000000298023,0.34999999403954)
    colors[imgui.Col.TitleBg] = imgui.ImVec4(0.27000001072884,0.27000001072884,0.54000002145767,0.8299999833107)
    colors[imgui.Col.SeparatorHovered] = imgui.ImVec4(0.60000002384186,0.60000002384186,0.69999998807907,1)
    colors[imgui.Col.ComboBg] = imgui.ImVec4(0.20000000298023,0.20000000298023,0.20000000298023,0.99000000953674)
    colors[imgui.Col.ResizeGrip] = imgui.ImVec4(1,1,1,0.30000001192093)
    colors[imgui.Col.SeparatorActive] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.89999997615814,1)
    colors[imgui.Col.Border] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.69999998807907,0.40000000596046)
    colors[imgui.Col.HeaderHovered] = imgui.ImVec4(0.44999998807907,0.44999998807907,0.89999997615814,0.80000001192093)
    colors[imgui.Col.Separator] = imgui.ImVec4(0.5,0.5,0.5,1)
    colors[imgui.Col.FrameBgHovered] = imgui.ImVec4(0.89999997615814,0.80000001192093,0.80000001192093,0.40000000596046)
    colors[imgui.Col.ScrollbarBg] = imgui.ImVec4(0.20000000298023,0.25,0.30000001192093,0.60000002384186)
    colors[imgui.Col.PlotLinesHovered] = imgui.ImVec4(0.89999997615814,0.69999998807907,0,1)
    colors[imgui.Col.TitleBgCollapsed] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.20000000298023)
    colors[imgui.Col.ChildWindowBg] = imgui.ImVec4(0,0,0,0)
    colors[imgui.Col.CheckMark] = imgui.ImVec4(0.89999997615814,0.89999997615814,0.89999997615814,0.5)
    colors[imgui.Col.ScrollbarGrabHovered] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.40000000596046)
    colors[imgui.Col.TitleBgActive] = imgui.ImVec4(0.31999999284744,0.31999999284744,0.62999999523163,0.87000000476837)
    colors[imgui.Col.HeaderActive] = imgui.ImVec4(0.52999997138977,0.52999997138977,0.87000000476837,0.80000001192093)
    colors[imgui.Col.CloseButtonActive] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.69999998807907,1)
    colors[imgui.Col.MenuBarBg] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.55000001192093,0.80000001192093)
    colors[imgui.Col.WindowBg] = imgui.ImVec4(0,0,0,0.69999998807907)
    colors[imgui.Col.SliderGrabActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,1)
    colors[imgui.Col.PopupBg] = imgui.ImVec4(0.050000000745058,0.050000000745058,0.10000000149012,0.89999997615814)
    colors[imgui.Col.ScrollbarGrabActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,0.40000000596046)
    colors[imgui.Col.Button] = imgui.ImVec4(0.6700000166893,0.40000000596046,0.40000000596046,0.60000002384186)
    colors[imgui.Col.Header] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.89999997615814,0.44999998807907)
    colors[imgui.Col.BorderShadow] = imgui.ImVec4(0,0,0,0)
    colors[imgui.Col.CloseButtonHovered] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.89999997615814,0.60000002384186)
    colors[imgui.Col.Text] = imgui.ImVec4(0.89999997615814,0.89999997615814,0.89999997615814,1)
end

do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(encodeJson(table))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return decodeJson(content)
                else
                    return error("Could not load configuration")
                end
            else
                return false
            end
        end

        function self.update(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^[%w%._/%\\]+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
			
			if loadedCfg then
				for k, v in pairs(table) do
					if loadedCfg[k] ~= nil then
						table[k] = loadedCfg[k]
					end
				end
			end

            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end
local jcfg = Jcfg()

local cef = (function()
    local self = {}

    local subscribe = false
    local eventFunction = nil

    addEventHandler("onSendPacket",function(id, bs, priority, reliability, orderingChannel)
        if subscribe and id == 220 then
            local id = raknetBitStreamReadInt8(bs)
            local packet = raknetBitStreamReadInt8(bs)
            local len = raknetBitStreamReadInt8(bs)
            raknetBitStreamIgnoreBits(bs, 24)
            local str = raknetBitStreamReadString(bs, len)
            if packet ~= 0 and packet ~= 1 and #str > 2 then
                local result = str
                if type(result) ~= "nil" then
                    eventFunction('S', result)
                end
            end
        end
    end)
    
    addEventHandler("onReceivePacket",function(id, bs)
        if subscribe and id == 220 then
            raknetBitStreamIgnoreBits(bs, 8)
            if raknetBitStreamReadInt8(bs) == 17 then
                raknetBitStreamIgnoreBits(bs, 32)
                local result = raknetBitStreamReadString(bs, raknetBitStreamReadInt32(bs))
                if type(result) ~= "nil" then
                    eventFunction('R', result)
                end
            end
        end
    end)

    --Interfaces

    function self.subscribeEvents(var, func)
        if type(var) ~= "boolean" then error('bad argument #1 to subscribe, boolean expected got: '..type(var)) end
        if var == true and type(func) ~= "function" then error('bad argument #2 to subscribe, function expected got: '..type(func)) end
        subscribe = var
        eventFunction = func or nil
        return subscribe
    end

    function self.send(str)
        if type(str) ~= "string" or #str == 0 then error('bad argument #1 to send, not empty string expected got: '..type(str)) end
		local bs = raknetNewBitStream()
		raknetBitStreamWriteInt8(bs, 220)
		raknetBitStreamWriteInt8(bs, 18)
		raknetBitStreamWriteInt32(bs, #str)
		raknetBitStreamWriteString(bs, str)
		raknetBitStreamWriteInt32(bs, 0)
		raknetSendBitStream(bs)
		raknetDeleteBitStream(bs)
	end

    return self
end)()

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local cfg = {
    imguiStyle = 0,
    active = false,
}
jcfg.update(cfg)

function imguiStyler(num)
    local tbl = {
        [0] = jfTheme,
        [1] = violetTheme,
        [2] = defaultTheme,
    }
    if num ~= 2 then defaultTheme() end
    tbl[num]()
end
imguiStyler(cfg.imguiStyle)

local imcfg = jcfg.setupImgui(cfg)

function save()
    jcfg.save(cfg)
end

local imgui_windows = {
    main = imgui.ImBool(false),
}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    addChat('Загружен. Команда: {ffc0cb}/jaf{ffffff}.')
    sampRegisterChatCommand('jaf',function()
        imgui_windows.main.v = not imgui_windows.main.v
    end)
    cef.subscribeEvents(cfg.active, cefHandler)
    while true do wait(0)
        imgui.Process = imgui_windows.main.v
    end
end

local max = 0
local current = 0
local fuelId = 0
function cefHandler(type, msg)
    if cfg.active then
        if type == 'R' then
            if msg:find('event.gasstation.initializeMaxLiters',1,true) then
                max = tonumber(msg:match('`%[(%d+)%]`'))
            elseif msg:find('event.gasstation.initializeCurrentLiters',1,true) then
                current = tonumber(msg:match('`%[(%d+)%]`'))
            elseif msg:find('event.gasstation.initializeFuelTypes',1,true) then
                local js = msg:match('`%[(.+)%]`')
                if js then
                    js = decodeJson(js)
                    for k,v in pairs(js) do
                        if v.available == 1 then
                            fuelId = v.id
                        end
                    end
                end
            end
        else
            if msg:find('onActiveViewChanged|GasStation') then
                cef.send(('purchaseFuel|%s|%s'):format(fuelId, max-current))
            end
        end
    end
end

local w,h = getScreenResolution()
local window_width,window_height = 250,150
local imTabs = 1
function imgui.OnDrawFrame()

    if imgui_windows.main.v then


        imgui.SetNextWindowSize(imgui.ImVec2(window_width,window_height), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2-window_width/2, h/2-window_height/2), imgui.Cond.FirstUseEver)
        imgui.Begin(u8(thisScript().name)..' Ver: '..thisScript().version.."##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.NoResize)

        if imgui.Checkbox(u8'Авто-заправка', imcfg.active) then
            cfg.active = imcfg.active.v
            cef.subscribeEvents(cfg.active, cefHandler)
            save()
        end
        if imgui.IsItemHovered() then
            imgui.SetTooltip(u8('Включить/Отключить скрипт.'))
        end
    
        imgui.Separator()
        imgui.Text(u8'Выберите тему:')
        if imgui.Combo('##selectStyleCombo', imcfg.imguiStyle, {u8"Стандартная [JF]", u8"Фиолетовая Violet", u8"Стандартная ImGui"}, 10) then
            if imcfg.imguiStyle.v ~= cfg.imguiStyle then
                cfg.imguiStyle = imcfg.imguiStyle.v
                imguiStyler(cfg.imguiStyle)
                save()
            end
        end
        if imgui.IsItemHovered() and not imgui.IsItemActive() then
            imgui.SetTooltip(u8('Тут вы можете поменять тему интерфейса.'))
        end

        imgui.End()

    end
    
end