script_author('JustFedot')
script_name('[JF] AD Helper')
script_version('1.0.0')

local sampev = require("samp.events")

function sampev.onShowDialog(id, style, title, button1, button2, text)
    if title:find('{BFBBBA}Выберите радиостанцию') then

        local n = -1
        local check = {}
        for line in text:gmatch('[^\r\n]+') do
            if line:find("назад$") then
                local hours, mins, secs = line:match("(%d+) час "), line:match("(%d+) мин "), line:match("(%d+) сек ")
                hours = tonumber(hours) or 0
                mins = tonumber(mins) or 0
                secs = tonumber(secs) or 0
            
                local totalSeconds = hours * 3600 + mins * 60 + secs
                table.insert(check, {n, totalSeconds})
            end
            n=n+1
        end
        if check[1] then
            table.sort(check, function(a,b) return a[2] < b[2] end)
            sampSendDialogResponse(id,1,check[1][1],nil)
            return false
        end
    end
end