script_author('JustFedot')
script_name('[JF] PayDay')
script_version('1.3.1')


require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("imgui")
local f = require 'moonloader'.font_flag
local font = renderCreateFont('Arial', 10, f.BOLD + f.SHADOW)

function VioletTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    colors[clr.Text]                 = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
    colors[clr.WindowBg]             = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.ChildWindowBg]        = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.PopupBg]              = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.Border]               = ImVec4(0.71, 0.71, 0.71, 0.40)
    colors[clr.BorderShadow]         = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.FrameBg]              = ImVec4(0.34, 0.30, 0.34, 0.30)
    colors[clr.FrameBgHovered]       = ImVec4(0.22, 0.21, 0.21, 0.40)
    colors[clr.FrameBgActive]        = ImVec4(0.20, 0.20, 0.20, 0.44)
    colors[clr.TitleBg]              = ImVec4(0.52, 0.27, 0.77, 0.82)
    colors[clr.TitleBgActive]        = ImVec4(0.55, 0.28, 0.75, 0.87)
    colors[clr.TitleBgCollapsed]     = ImVec4(9.99, 9.99, 9.90, 0.20)
    colors[clr.MenuBarBg]            = ImVec4(0.27, 0.27, 0.29, 0.80)
    colors[clr.ScrollbarBg]          = ImVec4(0.08, 0.08, 0.08, 0.60)
    colors[clr.ScrollbarGrab]        = ImVec4(0.54, 0.20, 0.66, 0.30)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.21, 0.21, 0.21, 0.40)
    colors[clr.ScrollbarGrabActive]  = ImVec4(0.80, 0.50, 0.50, 0.40)
    colors[clr.ComboBg]              = ImVec4(0.20, 0.20, 0.20, 0.99)
    colors[clr.CheckMark]            = ImVec4(0.89, 0.89, 0.89, 0.50)
    colors[clr.SliderGrab]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.SliderGrabActive]     = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Button]               = ImVec4(0.48, 0.25, 0.60, 0.60)
    colors[clr.ButtonHovered]        = ImVec4(0.67, 0.40, 0.40, 1.00)
    colors[clr.ButtonActive]         = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Header]               = ImVec4(0.56, 0.27, 0.73, 0.44)
    colors[clr.HeaderHovered]        = ImVec4(0.78, 0.44, 0.89, 0.80)
    colors[clr.HeaderActive]         = ImVec4(0.81, 0.52, 0.87, 0.80)
    colors[clr.Separator]            = ImVec4(0.42, 0.42, 0.42, 1.00)
    colors[clr.SeparatorHovered]     = ImVec4(0.57, 0.24, 0.73, 1.00)
    colors[clr.SeparatorActive]      = ImVec4(0.69, 0.69, 0.89, 1.00)
    colors[clr.ResizeGrip]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.ResizeGripHovered]    = ImVec4(1.00, 1.00, 1.00, 0.60)
    colors[clr.ResizeGripActive]     = ImVec4(1.00, 1.00, 1.00, 0.89)
    colors[clr.CloseButton]          = ImVec4(0.33, 0.14, 0.46, 0.50)
    colors[clr.CloseButtonHovered]   = ImVec4(0.69, 0.69, 0.89, 0.60)
    colors[clr.CloseButtonActive]    = ImVec4(0.69, 0.69, 0.69, 1.00)
    colors[clr.PlotLines]            = ImVec4(1.00, 0.99, 0.99, 1.00)
    colors[clr.PlotLinesHovered]     = ImVec4(0.49, 0.00, 0.89, 1.00)
    colors[clr.PlotHistogram]        = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.TextSelectedBg]       = ImVec4(0.54, 0.00, 1.00, 0.34)
    colors[clr.ModalWindowDarkening] = ImVec4(0.20, 0.20, 0.20, 0.34)
end
VioletTheme()

do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local json = require('dkjson')

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(json.encode(table, {indent = true}))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return json.decode(content)
                else
                    return error("Could not load configuration")
                end
            else
                return false
            end
        end

        function self.update(table, path)
            assert(type(table) == "table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^[%w%._/%\\]+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
            
            local function isList(tbl)
                if type(tbl) ~= "table" then return false end
                local count = 0
                for k, _ in pairs(tbl) do
                    count = count + 1
                    if type(k) ~= "number" or k ~= count then
                        return false
                    end
                end
                return true
            end
        
            local function updateTable(origTable, newTable)
                if newTable then
                    for k, v in pairs(newTable) do
                        if type(v) == "table" and type(origTable[k]) == "table" then
                            if isList(v) then
                                origTable[k] = v
                            else
                                updateTable(origTable[k], v)
                            end
                        elseif origTable[k] ~= nil then
                            origTable[k] = v
                        end
                    end
                end
            end
        
            updateTable(table, loadedCfg)
            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end

local jcfg = Jcfg()

local imgui_windows = {
    main = imgui.ImBool(false),
}

local cfg = {
    total = {
        money = 0,
        deposit = 0,
        az = 0,
    },
    current = {
        money = 0,
        deposit = 0,
        az = 0,
    },
	depositMinimalMoney = 270000000,
    depositMaximumAmountToTake = 10000000,
	log = {},
    bankPin = "0000",
    depositNoComission = false,
    business = {
        active = false,
        minimalBalance = 0,
    },
}

jcfg.update(cfg)

local imcfg = jcfg.setupImgui(cfg)

function save()
	jcfg.save(cfg)
end

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local on = false
local wallet = {
    main = 0,
    deposit = 0,
}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    addChat('Загружен. Команда: {ffc0cb}/pd{ffffff}.')
    sampRegisterChatCommand('pd',function()
        imgui_windows.main.v = not imgui_windows.main.v
    end)
    while true do wait(0)
        imgui.Process = imgui_windows.main.v
    end
end

local w,h = getScreenResolution()
local window_width,window_height = 350,440
local imTabs = 1
function imgui.OnDrawFrame()

    if imgui_windows.main.v then


        imgui.SetNextWindowSize(imgui.ImVec2(window_width,window_height), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2-window_width/2, h/2-window_height/2), imgui.Cond.FirstUseEver)
        imgui.Begin(u8(thisScript().name)..' Ver: '..thisScript().version.."##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar)

        imTabs = imgui.Tabs(2, imTabs, u8"Статистика", u8"Настройки")

        __tabInterface(imTabs)

        imgui.End()

    end

end
function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end
function imgui.Tabs(count, tabs, ...)
    local buttonLabels = {...}
    local style = imgui.GetStyle()
    local windowWidth = imgui.GetWindowWidth() - style.WindowPadding.x * 2
    local minButtonWidth = 0

    -- Определение самой длинной надписи
    for i = 1, count do
        local labelWidth = imgui.CalcTextSize(buttonLabels[i] or "Button "..i).x + style.FramePadding.x * 2
        if labelWidth > minButtonWidth then
            minButtonWidth = labelWidth
        end
    end

    -- Расчет количества кнопок в строке и общего количества строк
    local buttonsPerRow = math.floor(windowWidth / minButtonWidth)
    if buttonsPerRow < 1 then buttonsPerRow = 1 end  -- Убедиться, что хотя бы одна кнопка помещается в строку
    local rows = math.ceil(count / buttonsPerRow)

    for row = 1, rows do
        local buttonsInThisRow = math.min(buttonsPerRow, count - (row - 1) * buttonsPerRow)
        local buttonWidth = (windowWidth - (buttonsInThisRow - 1) * style.ItemSpacing.x) / buttonsInThisRow

        -- Отрисовка кнопок в строке
        for i = 1, buttonsInThisRow do
            local buttonIndex = (row - 1) * buttonsPerRow + i
            local buttonStyle = imgui.GetStyle()
            local needStylePop = false

            if tabs == buttonIndex then
                imgui.PushStyleColor(imgui.Col.Button, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonHovered, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonActive, buttonStyle.Colors[imgui.Col.CheckMark])
                needStylePop = true
            end

            local buttonLabel = buttonLabels[buttonIndex] or "Button "..buttonIndex
            local buttonPressed = imgui.Button(buttonLabel, imgui.ImVec2(buttonWidth, 0))

            if needStylePop then
                imgui.PopStyleColor(3)
            end

            if buttonPressed and tabs ~= buttonIndex then
                tabs = buttonIndex
            end

            if i < buttonsInThisRow then
                imgui.SameLine()
            end
        end

        -- Переход на новую строку, если это не последняя
        -- if row < rows then
        --     imgui.NewLine()
        -- end
    end

    return tabs
end
function imgui.MihailKrug(text, hint)
    local value = 0.3
    local bgColor = imgui.GetColorU32(imgui.ImVec4(0.2, 0.2, 0.2, 1.0))
    local fgColor = imgui.GetColorU32(imgui.ImVec4(1.0, 1.0, 1.0, 1.0))
    local speed = 5.0
    local width = 15.0
    text = u8(text or "Process...")
    hint = u8(hint or "Press me...")

    local textSize = imgui.CalcTextSize(text)
    local textWidth = textSize.x
    local textHeight = textSize.y

    if radius == nil then
        radius = math.max(textWidth, textHeight) * 0.5 + width + 10
    end

    imgui.SetCursorPosX((imgui.GetContentRegionAvail().x - (radius * 2)) / 2)
    local drawList = imgui.GetWindowDrawList()
    local cursorPos = imgui.GetCursorScreenPos()
    local centerX = cursorPos.x + radius
    local centerY = cursorPos.y + radius

    local segments = 64
    local angle = (1.0 - value) * math.pi

    local startAngle = math.pi * 0.5
    local animatedAngle = startAngle + (os.clock() * speed) % (math.pi * 2)
    local endAngle = animatedAngle + angle

    local radiusInner = radius - width
    imgui.Dummy(imgui.ImVec2(radius * 2, radius * 2))
    if imgui.IsItemHovered() then
        fgColor = imgui.GetColorU32(imgui.ImVec4(imgui.GetStyle().Colors[imgui.Col.ButtonHovered]))
        imgui.SetTooltip(hint)
    end
    if angle > 0 then
        local step = angle / segments
        for i = 0, segments - 1 do
            local x1 = centerX + math.cos(animatedAngle + i * step) * radius
            local y1 = centerY + math.sin(animatedAngle + i * step) * radius
            local x2 = centerX + math.cos(animatedAngle + (i + 1) * step) * radius
            local y2 = centerY + math.sin(animatedAngle + (i + 1) * step) * radius
            local x3 = centerX + math.cos(animatedAngle + i * step) * radiusInner
            local y3 = centerY + math.sin(animatedAngle + i * step) * radiusInner
            local x4 = centerX + math.cos(animatedAngle + (i + 1) * step) * radiusInner
            local y4 = centerY + math.sin(animatedAngle + (i + 1) * step) * radiusInner

            drawList:AddQuadFilled(imgui.ImVec2(x1, y1), imgui.ImVec2(x2, y2), imgui.ImVec2(x4, y4), imgui.ImVec2(x3, y3), fgColor)
        end
    end
    local textPos = imgui.ImVec2(centerX - textWidth * 0.5, centerY - textHeight * 0.5)
    drawList:AddText(textPos, fgColor, text)
    if imgui.IsItemHovered() then
        fgColor = imgui.GetColorU32(imgui.ImVec4(imgui.GetStyle().Colors[imgui.Col.ButtonHovered]))
        imgui.SetTooltip(hint)
    end
    if imgui.IsItemClicked(0) then
        return true
    end
end

function __tabInterface(tab)
    local tabs = {
        [1] = __statisticTab,
        [2] = __settingsTab,
    }
    imgui.BeginChild('##mainChild', imgui.ImVec2(-1, -1), true)
    tabs[tab]()
    imgui.EndChild()
end

local _searchStatisticBuffer = imgui.ImBuffer(256)
local _comboSelected = imgui.ImInt(0)

function __statisticTab()
    imgui.SetCursorPosX(imgui.GetWindowWidth() - 55)
    imgui.SetCursorPosY(10)
    imgui.Button('->O<-##allMoneyButton', imgui.ImVec2(45, 25))
	if imgui.IsItemHovered() then
		imgui.BeginTooltip()
            imgui.Text(u8'Общий доход:')
			imgui.TextColoredRGB('1ч: {99ff99}'..formatMoney(cfg.current.money*2+cfg.current.deposit*2)..'$')
			imgui.TextColoredRGB('24ч: {99ff99}'..formatMoney(cfg.current.money*2*24+cfg.current.deposit*2*24)..'$')
			imgui.TextColoredRGB('1мес: {99ff99}'..formatMoney(cfg.current.money*2*24*30+cfg.current.deposit*2*24*30)..'$')
            imgui.TextDisabled(u8('(AZ: 1ч: '..(cfg.current.az*2)..'. 24ч: '..formatMoney(cfg.current.az*2*24).. '. 1мес: '..formatMoney(cfg.current.az*2*24*30)..'.)'))
		imgui.EndTooltip()
	end

    imgui.SetCursorPosY(10)
    imgui.TextColoredRGB('Основной счёт: {99ff99}'..formatMoney(cfg.total.money)..'$')
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
            imgui.TextColoredRGB('Доход в час: {99ff99}'..formatMoney(cfg.current.money*2)..'$')
            imgui.TextDisabled(u8('24ч: '..formatMoney(cfg.current.money*2*24)..'$'))
            imgui.TextDisabled(u8('1мес: '..formatMoney(cfg.current.money*2*24*30)..'$'))
        imgui.EndTooltip()
    end

    imgui.TextColoredRGB('Депозит: {99ff99}'..formatMoney(cfg.total.deposit)..'$')
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
            imgui.TextColoredRGB('Доход в час: {99ff99}'..formatMoney(cfg.current.deposit*2)..'$')
            imgui.TextDisabled(u8('24ч: '..formatMoney(cfg.current.deposit*2*24)..'$'))
            imgui.TextDisabled(u8('1мес: '..formatMoney(cfg.current.deposit*2*24*30)..'$'))
        imgui.EndTooltip()
    end

    imgui.TextColoredRGB('AZ-Coins: {99ff99}'..formatMoney(cfg.total.az)..'.')
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
            imgui.TextColoredRGB('Доход в час: {99ff99}'..formatMoney(cfg.current.az*2)..'{ffffff}.')
            imgui.TextDisabled(u8('24ч: '..formatMoney(cfg.current.az*2*24)..'.'))
            imgui.TextDisabled(u8('1мес: '..formatMoney(cfg.current.az*2*24*30)..'.'))
        imgui.EndTooltip()
    end

    imgui.TextColoredRGB('Можно снять: {ABCDEF}'..formatMoney(((cfg.total.deposit> cfg.depositMinimalMoney) and cfg.total.deposit-cfg.depositMinimalMoney or 0) +cfg.total.money)..'$')
    if imgui.IsItemHovered() then
        imgui.BeginTooltip()
            imgui.TextColoredRGB('Со счёта: {99ff99}'..formatMoney(cfg.total.money)..'$')
            imgui.TextColoredRGB('С депозита: {99ff99}'..formatMoney((cfg.total.deposit-cfg.depositMinimalMoney > 0) and cfg.total.deposit-cfg.depositMinimalMoney or 0)..'$')
        imgui.EndTooltip()
    end

    if checkBankInterior() then
        imgui.Separator()

        if not on then
            imgui.TextColoredRGB('{ffff00}[Внимание]\nПохоже вы в здании банка.\nВам доступна функция:')
            imgui.SameLine()
            if imgui.Button(u8"Снять всё", imgui.ImVec2(100,0)) then
                local finalMoney = getPlayerMoney(PLAYER_HANDLE)+cfg.total.money+(cfg.total.deposit-cfg.depositMinimalMoney>0 and cfg.total.deposit-cfg.depositMinimalMoney or 0)
                if finalMoney >= 2140000000 then
                    addChat('{ff0000}[Ошибка]{ffffff} У вас на руках слишком много денег.')
                else
                    takeMoney()
                end
            end
            if imgui.IsItemHovered() then
                imgui.SetTooltip(u8('При нажатии этой кнопки скрипт будет снимать все деньги с ваших счетов.\nСохраняя минимальную сумму на депозите.\nПосле снятия у вас будет: '..
                formatMoney(getPlayerMoney(PLAYER_HANDLE)+cfg.total.money+(cfg.total.deposit-cfg.depositMinimalMoney>0 and cfg.total.deposit-cfg.depositMinimalMoney or 0))..'$(на руках)\nПомните, вы должны стоять у кассы банка.\nПосле ключения режима, встаньте на пикап кассы и нажмите Alt.(откройте меню банка)'))
            end
        else
            if imgui.MihailKrug("Снимаю деньги...","Нажмите чтобы остановить.") then
                takeMoney()
            end
        end

        imgui.Separator()
    end

    -- if imgui.Button('Test') then
    --     local date,money = "01.03.2024",1
    --     local needInsert = true
    --     for k,v in pairs(cfg.log) do
    --         if v[1] == date then
    --             v[2] = v[2] + money
    --             needInsert = false
    --             break
    --         end
    --     end
    --     if needInsert then table.insert(cfg.log,1,{date,money}) end
    --     save()
    -- end

    if imgui.CollapsingHeader(u8'Статистика по датам##dateStatisticCollapsingHeader') then
        
        if not next(cfg.log) then
            imgui.TextColoredRGB('{ff0000}Упс...{ffffff} Похоже тут ещё ничего нет.')
            imgui.Separator()
        else
            imgui.PushItemWidth(imgui.GetWindowWidth()-imgui.GetStyle().ItemSpacing.x*2)

            imgui.Combo('##drawCombo', _comboSelected, {u8'Показать последние 7 дней', u8'Показать последние 30 дней', u8('Показать все дни('..#cfg.log..' записи)')}, 10)
            if imgui.IsItemHovered() and not imgui.IsItemActive() then
                imgui.SetTooltip(u8("Тут вы можете выбрать за какой период вам показать логи.\nПоследние 7 дней, 30 или все записи что существуют."))
            end

            imgui.Separator()
            imgui.BeginChild('##dateStatisticChild', imgui.ImVec2(-1, -1), false)
            for k,v in pairs(cfg.log) do
                if _comboSelected.v == 0 then
                    if k > 7 then break end
                elseif _comboSelected.v == 1 then
                    if k > 30 then break end
                end
                imgui.TextColoredRGB('{ffa500}['..v[1]..']{ffffff} Заработано: {99ff99}'..formatMoney(v[2])..'$')
                imgui.SameLine()
                local azDisplayText = u8('AZ: '..(v[3]~=nil and formatMoney(v[3]) or 0)..'.')
                imgui.SetCursorPosX(imgui.GetWindowWidth() - imgui.CalcTextSize(azDisplayText).x-15)
                imgui.TextDisabled(azDisplayText)
                imgui.Separator()
            end
            imgui.EndChild()
        end

    end
end

local _showPassword = nil
function __settingsTab()
    if imgui.Checkbox(u8'Не считать комиссию##depositNoCommision', imcfg.depositNoComission) then
        cfg.depositNoComission = imcfg.depositNoComission.v
        save()
    end
    if imgui.IsItemHovered() then
        imgui.SetTooltip(u8('Если галочка активна, комиссия при снятии депозита учитыватся не будет.\nP.S. Нужно для владельцев Monobank Iron Card.\nОй, Железной карты Нац. Банка я имел ввиду.'))
    end

    if imgui.Checkbox(u8'Снимать деньги с бизнеса##takeBusinessMoney', imcfg.business.active) then
        cfg.business.active = imcfg.business.active.v
        save()
    end
    if imgui.IsItemHovered() then
        imgui.SetTooltip(u8('Если галочка активна, при снятии всех денег со счетов, будет сниматься финка бизнесов.\nP.S. Нужно для владельцев Monobank Iron Card.\nОй, Железной карты Нац. Банка я имел ввиду.'))
    end

    if cfg.business.active then
        if imgui.InputInt('##businessMinimalSumm', imcfg.business.minimalBalance, 0, 0) then
            cfg.business.minimalBalance = imcfg.business.minimalBalance.v
            save()
        end
        if imgui.IsItemHovered() and not imgui.IsItemActive() then
            imgui.SetTooltip(u8('В это поле нужно указать какое количество денег должно оставатся на балансе бизнеса.\nПрименяется ко всем бизнесам сразу.\nДля снятия всех денег, укажите 0.\nТекущая сумма: '..formatMoney(cfg.business.minimalBalance)..'$'))
        end
    end

    imgui.Separator()

    imgui.Text(u8'Мин. Сумма на депозите:')
    if imgui.InputInt('##minimalDepositInput', imcfg.depositMinimalMoney, 0, 0) then
        cfg.depositMinimalMoney = imcfg.depositMinimalMoney.v
        save()
    end
    if imgui.IsItemHovered() and not imgui.IsItemActive() then
        imgui.SetTooltip(u8('Укажите минимальную сумму которую нужно оставить.\nТекущая: '..formatMoney(cfg.depositMinimalMoney)..'$'))
    end

    imgui.Text(u8'Максимальная сумма разового снятия депозита:')
    if imgui.InputInt('##maximalDepositInput', imcfg.depositMaximumAmountToTake, 0, 0) then
        cfg.depositMaximumAmountToTake = imcfg.depositMaximumAmountToTake.v
        save()
    end
    if imgui.IsItemHovered() and not imgui.IsItemActive() then
        imgui.SetTooltip(u8('Укажите максимальную сумму для единоразового снятия.\nТекущая: '..formatMoney(cfg.depositMaximumAmountToTake)..'$'))
    end

    imgui.Text(u8'Ваш пин-код от карточки:')
    imgui.TextDisabled(u8"(И срок действия, ну и 3 цифры с оборота)")
    if imgui.InputText('##bankPinInput', imcfg.bankPin, _showPassword and imgui.InputTextFlags.Password or nil) then
        imcfg.bankPin.v=imcfg.bankPin.v:gsub('%D','')
        if #imcfg.bankPin.v<=0 then imcfg.bankPin.v = "0000" end
        cfg.bankPin = u8:decode(imcfg.bankPin.v)
        save()
    end
    _showPassword = not imgui.IsItemActive()

    imgui.Separator()

    if imgui.CollapsingHeader(u8'Очистка статистики##clearstatisticCollapsHeader') then
        imgui.SetCursorPosX(imgui.GetWindowWidth()/2-75)
        if imgui.Button(u8'Очистить статистику##cleanStatisticButton', imgui.ImVec2(150, 0)) then
            local path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\reservedLogs.json'
            jcfg.save(cfg.log, path)
            cfg.log = {}
            save()
            addChat('{ffff00}[Внимание] {ffffff}Статистика очищена. На всякий случай сохранена копия.')
            addChat(path)
        end
        if imgui.IsItemHovered() then
            imgui.SetTooltip(u8('Эта кнопка удаляет статистику по датам.\nНужна если начало лагать при просмотре статистики.\nP.S. Не переживайте, начнёт лагать разве что если вы соберёте статистику лет за 5 минимум.\nТак что кнопка счтитайте просто для галочки.'))
        end
    end
end

function formatMoney(num)
    if not num then return "0" end
    return string.format("%d", num):reverse():gsub("(%d%d%d)", "%1,"):reverse():gsub("^,", "")
end

function getCurrentDate()
    local currentDate = os.date("*t")
    return string.format("%02d.%02d.%d", currentDate.day, currentDate.month, currentDate.year)
end

function checkBankInterior()
    for i = 2048, 1, -1 do
        if sampIs3dTextDefined(i) then
            local text = sampGet3dTextInfoById(i)
            if text == "{73B461}Касса для проведения\nбанковских операций" then
                return true
            end
        end
    end
    return false
end

local off = function(msg)
    on = false
    addChat('{ffff00}[Внимание] {ffffff}Скрипт {ff0000}остановлен.')
    if msg then addChat('Причина: {99ff99}'..msg..'.') end
    return
end

local businessWindowsDebug = false

function sampev.onServerMessage(color, text)

    text = text:gsub("(%d),(%d)", "%1%2")
    text = text:gsub("%s", "")

    if text:find('^Баланснадонат%-счет[еу]*:%d+AZ{......}%(%+%d+AZ%)') then
        cfg.total.az = tonumber(text:match('^Баланснадонат%-счет[еу]*:(%d+)AZ{......}%(%+%d+AZ%)'))
        cfg.current.az = tonumber(text:match('^Баланснадонат%-счет[еу]*:%d+AZ{......}%(%+(%d+)AZ%)'))

        local date = getCurrentDate()
        for k,v in pairs(cfg.log) do
            if v[1] == date then
                if v[3] ~= nil then v[3] = v[3] + cfg.current.az else v[3] = cfg.current.az end
                save()
                return
            end
        end
        table.insert(cfg.log,1,{date,0,cfg.current.az})
        save()

    elseif text:find('^Текущаясумманадепозите:%$%d+{......}%(%+%$%d+%)') then
        cfg.total.deposit = tonumber(text:match('^Текущаясумманадепозите:%$(%d+){......}%(%+%$%d+%)'))
        local date,money = getCurrentDate(),tonumber(text:match('^Текущаясумманадепозите:%$%d+{......}%(%+%$(%d+)%)'))
		cfg.current.deposit = money
        for k,v in pairs(cfg.log) do
            if v[1] == date then
                v[2] = v[2] + money
                save()
                return
            end
        end
        table.insert(cfg.log,1,{date,money})
        save()

    elseif text:find("^Текущаясуммавбанке:%$%d+{......}%(%+%$%d+%)") then
        cfg.total.money = tonumber(text:match('^Текущаясуммавбанке:%$(%d+){......}%(%+%$%d+%)'))
        local date,money = getCurrentDate(),tonumber(text:match("^Текущаясуммавбанке:%$%d+{......}%(%+%$(%d+)%)"))
		cfg.current.money = money
        for k,v in pairs(cfg.log) do
            if v[1] == date then
                v[2] = v[2] + money
                save()
                return
            end
        end
        table.insert(cfg.log,1,{date,money})
        save()

	elseif text:find('^Выснялиденьгисдепозитногосчета%$%d+%.Комиссия:%$%d+') then
        if on then businessWindowsDebug = true end
		cfg.total.deposit = cfg.total.deposit - (tonumber(text:match('^Выснялиденьгисдепозитногосчета%$(%d+)%.Комиссия:%$%d+')) + tonumber(text:match('^Выснялиденьгисдепозитногосчета%$%d+%.Комиссия:%$(%d+)')))
		save()
    elseif text:find('^Выположилинасвойдепозитныйсчет%$%d+') then
        cfg.total.deposit = cfg.total.deposit + tonumber(text:match('^Выположилинасвойдепозитныйсчет%$(%d+)'))
        save()
	elseif text:find('^%[Информация%]{FFFFFF}Выснялисосвоегобанковскогосчета%$%d+') then
		local b = text:match('%$(%d+)')
		cfg.total.money = cfg.total.money-tonumber(b)
		save()
	elseif text:find('^%[Информация%]{FFFFFF}Выположилинасвойбанковскийсчет%$%d+') then
		local b = text:match('%$(%d+)')
		cfg.total.money = cfg.total.money+tonumber(b)
		save()
	elseif text:find('^{FFFFFF}Состояниесчета:{......}%$%d+,{FFFFFF}состояниедепозита:{......}%$%d+') then
		local a,b = text:match('^{FFFFFF}Состояниесчета:{......}%$(%d+),{FFFFFF}состояниедепозита:{......}%$(%d+)')
		cfg.total.deposit,cfg.total.money = tonumber(b),tonumber(a)
		save()
	elseif text:find('^%[Информация%]{FFFFFF}Выперевели%$%d+игроку.+насчет') then
		local b = text:match('%$(%d+)')
		cfg.total.money = cfg.total.money-tonumber(b)
		save()
	elseif text:find('^Вампоступилпереводнавашсчетвразмере%$%d+отжителя') then
		local b = text:match('%$(%d+)')
		cfg.total.money = cfg.total.money+tonumber(b)
		save()
    end

end

local businessMoneyDebug = {}

function sampev.onShowDialog(id, style, title, button1, button2, text)
    if on then
        text = text:gsub("(%d),(%d)", "%1%2")

        if title:find('{BFBBBA}{ffff00}Авторизация в банке') then
            if text:find("{ffff00}-{ffffff} Вы должны подтвердить свой PIN-код к карточке.",1,true) then
                sampSendDialogResponse(id, 1, nil, cfg.bankPin)
                return
            else
                sampSendDialogResponse(id,1,0,nil)
                return
            end
            off('Не найден пункт меню')
            return
        elseif title:find('{BFBBBA}Банк') then
            businessWindowsDebug = false
            local pattern
            if wallet.main > 0 then
                pattern = '{ffffff}Снять с основного счета'
            elseif wallet.deposit > 0 then
                pattern = '{ffffff}Снять деньги с депозита'
            elseif cfg.business.active then
                pattern = '{ffffff}Управление счетами бизнеса'
            else
                off('Недостаточно денег для снятия.')
                return
            end
            local n = 0
            for line in text:gmatch("[^\r\n]+") do
                if line:find(pattern) then
                    sampSendDialogResponse(id,1,n,nil)
                    return
                end
                n=n+1
            end
            off('Не найден пункт меню: '..pattern)
            return
        elseif title:find('{BFBBBA}Введите сумму') then
            if text:find('{ffff00}На вашем балансе сейчас: ') then
                if wallet.main > 0 then
                    sampSendDialogResponse(id,1,nil,tostring(wallet.main))
                    wallet.main = 0
                    return
                else
                    sampSendDialogResponse(id,0,nil,nil)
                end
            end
            if text:find('{FFFFFF}Введите сумму от {B7D22C}') then
                local summ
                if wallet.deposit <= cfg.depositMaximumAmountToTake then
                    summ = cfg.depositNoComission and wallet.deposit or (wallet.deposit * (1 - tonumber(text:match('Текущая комиссия: {B7D22C}(%d+)%%{ffffff}')) / 100))
                    wallet.deposit = 0
                else
                    summ = cfg.depositMaximumAmountToTake
                    local commission_amount = cfg.depositNoComission and 0 or (cfg.depositMaximumAmountToTake * tonumber(text:match('Текущая комиссия: {B7D22C}(%d+)%%{ffffff}')) / 100)
                    wallet.deposit = wallet.deposit - (cfg.depositMaximumAmountToTake + commission_amount)
                end
                if summ >= 10000 then
                    sampSendDialogResponse(id,1,nil,tostring(summ))
                    return
                else
                    on = false
                    addChat('{ffff00}[Внимание] {ffffff}Скрипт {ff0000}остановлен.')
                    addChat('Причина: {99ff99}Недостаточно денег для снятия.')
                    sampSendDialogResponse(id,0,nil,nil)
                    return
                end
            end
            off('Не найден пункт меню')
            return
        elseif cfg.business.active and title:find('{FAAC58}Выберите бизнес') then
            local n = 0
            for line in text:gmatch("[^\r\n]+") do
                if not businessMoneyDebug[line] and line:find('%(ID: %d+%)') then
                    businessMoneyDebug[line] = true
                    sampSendDialogResponse(id,1,n,nil)
                    return
                end
                n=n+1
            end
            off('Мы закончили')
            return
        elseif cfg.business.active and title:find('{BFBBBA}{FAAC58}Выберите действие') then
            if text:find('Снятие денежных средств со счёта') then
                sampSendDialogResponse(id,1,0,nil)
                return
            end
            off('Не найден пункт меню')
            return
        elseif cfg.business.active and title:find('{BFBBBA}Снятие со счёта бизнеса') then
            local money = tonumber(text:match('{ffffff}Баланс бизнеса: {FAAC58}%$(%d+)')) - cfg.business.minimalBalance
            if money > 0 then
                sampSendDialogResponse(id,1,0,tostring(money))
                return
            else
                businessWindowsDebug = true
                return
            end
            off('Не найден пункт меню')
            return
        end

    end
end


function takeMoney()
    if on then
        off()
        return
    else
        on = true
        businessMoneyDebug = {}
        businessWindowsDebug = false
        addChat('{ffff00}[Внимание] {ffffff}Скрипт {99ff99}запущен.')
        addChat('Встаньте на пикап кассы и откройте банковское меню.')
    end

    wallet.main = (cfg.total.money>0 and cfg.total.money or 0)
    wallet.deposit = (cfg.total.deposit-cfg.depositMinimalMoney>0 and cfg.total.deposit-cfg.depositMinimalMoney or 0)
    if wallet.main <= 0 and wallet.deposit < 10000 and not cfg.business.active then
        off('Недостаточно денег для снятия.')
    end

    lua_thread.create(function()

        -- wallet.main = (cfg.total.money>0 and cfg.total.money or 0)
        -- wallet.deposit = (cfg.total.deposit-cfg.depositMinimalMoney>0 and cfg.total.deposit-cfg.depositMinimalMoney or 0)

        local timeAnchor = 0
        while on do wait(0)

            if not checkBankInterior() then off('Вы покинули банк') return end

            if businessWindowsDebug then wait(300) setGameKeyState(21,255) end

            -- if os.clock()-timeAnchor >= 1.5 then
            --     timeAnchor = os.clock()
            --     setGameKeyState(21,255)
            -- end

            -- if wallet.main <= 0 and wallet.deposit < 10000 then
            --     off('Недостаточно денег для снятия.')
            -- end

        end
    end)
end