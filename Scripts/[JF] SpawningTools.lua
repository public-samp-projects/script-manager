script_author('JustFedot')
script_name('[JF] SpawningTools')
script_version('1.0.0')

require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local effil = require("effil")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("mimgui")
local ffi = require('ffi')
local sampfuncs = require('sampfuncs')
local raknet = require('samp.raknet')
require('samp.synchronization')


do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local json = require('dkjson')

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.new.int(v)
                        else
                            imcfg[k] = imgui.new.float(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.new.char[256](u8(v))
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.new.bool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(json.encode(table, {indent = true}))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return json.decode(content)
                else
                    return error("Could not load configuration")
                end
            else
                return {}
            end
        end

        function self.update(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^.+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
			
			if loadedCfg then
				for k, v in pairs(table) do
					if loadedCfg[k] ~= nil then
						table[k] = loadedCfg[k]
					end
				end
			end

            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end
local jcfg = Jcfg()


local cfg = {
    isReloaded = false,
    silentMode = false,

    autoSelectSpawn = {
        active = false,
        selected = {},
        all = {}
    },

    autoOutfit = {
        active = false,
        spawnPoint = {
            organization = "None",
            home = "None"
        },
        pickupPoint = {0, 0, 0},
        route = {},
        factionSkinId = 0
    },

    autoDoor = false

}
jcfg.update(cfg)
local imcfg = jcfg.setupImgui(cfg)
function save()
    jcfg.save(cfg)
end

function resetDefaultCfg()
    cfg = {
        isReloaded = false,
        silentMode = false,
    
        autoSelectSpawn = {
            active = false,
            selected = {},
            all = cfg.autoSelectSpawn.all
        },
    
        autoOutfit = {
            active = false,
            spawnPoint = {
                organization = "None",
                home = "None"
            },
            pickupPoint = {0, 0, 0},
            route = {},
            factionSkinId = 0
        },
    
        autoDoor = false
    
    }
    cfg.isReloaded = true
    save()
    thisScript():reload()
end


local utils = (function()
    local self = {}

    local function cyrillic(text)
        local convtbl = {[230]=155,[231]=159,[247]=164,[234]=107,[250]=144,[251]=168,[254]=171,[253]=170,[255]=172,[224]=97,[240]=112,[241]=99,[226]=162,[228]=154,[225]=151,[227]=153,[248]=165,[243]=121,[184]=101,[235]=158,[238]=111,[245]=120,[233]=157,[242]=166,[239]=163,[244]=63,[237]=174,[229]=101,[246]=36,[236]=175,[232]=156,[249]=161,[252]=169,[215]=141,[202]=75,[204]=77,[220]=146,[221]=147,[222]=148,[192]=65,[193]=128,[209]=67,[194]=139,[195]=130,[197]=69,[206]=79,[213]=88,[168]=69,[223]=149,[207]=140,[203]=135,[201]=133,[199]=136,[196]=131,[208]=80,[200]=133,[198]=132,[210]=143,[211]=89,[216]=142,[212]=129,[214]=137,[205]=72,[217]=138,[218]=167,[219]=145}
        local result = {}
        for i = 1, #text do
            local c = text:byte(i)
            result[i] = string.char(convtbl[c] or c)
        end
        return table.concat(result)
    end

    local function roundUpToThreeDecimalPlaces(num)
        local mult = 10^3
        return math.ceil(num * mult) / mult
    end

    local function round(num, decimals)
        local factor = 10^(decimals or 0)
        local rounded = math.floor(num * factor + 0.5) / factor
        return tostring(rounded):gsub("%.0$", "")
    end

    local function formatNumberWithDots(number)
        local numStr = tostring(number)
        local formatted = numStr:reverse():gsub("(%d%d%d)", "%1."):reverse()

        if formatted:sub(1, 1) == "." then
            formatted = formatted:sub(2)
        end
        return formatted
    end

    --------------------------------------------------------------------------------------------------------------------------------

    function self.addChat(a)
        if cfg.silentMode then return end
        if a then local a_type = type(a) if a_type == 'number' then a = tostring(a) elseif a_type ~= 'string' then return end else return end
        sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
    end

    function self.printStringNow(text, time)
        if not text then return end
        time = time or 100
        text = type(text) == "number" and tostring(text) or text
        if type(text) ~= 'string' then return end
        printStringNow(cyrillic(text), time)
    end

    function self.formatTime(seconds)
        local hours = math.floor(seconds / 3600)
        local minutes = math.floor((seconds % 3600) / 60)
        local seconds = seconds % 60
        return string.format("%02d:%02d:%02d", hours, minutes, seconds)
    end

    function self.random(x, y)
        math.randomseed(os.time())

        for i = 1, 5 do math.random() end

        return math.random(x, y)
    end

    function self.simplifyNumber(number)
        local suffixes = { [1e9] = "kkk", [1e6] = "kk", [1e3] = "k" }
    
        for base, suffix in ipairs({1e9, 1e6, 1e3}) do
            if number >= suffix then
                local decimals = (suffix == 1e3) and 1 or 3
                local value = round(number / suffix, decimals)
                return value .. (suffixes)[suffix]
            end
        end
    
        return tostring(number)
    end

    function self.formatNumber(number)
        return formatNumberWithDots(number)
    end

    return self
end)()





local imgui_windows = {
    main = imgui.new.bool(false)
}

local state = {
    autoOutfit = {
        pickup = false,
        active = false,
        state = 0,
        recording = false
    }
}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end


    utils.addChat('Загружен. Команда: {ffc0cb}/spt{ffffff}.')


    sampRegisterChatCommand('spt',function()
        imgui_windows.main[0] = not imgui_windows.main[0]
    end)

    -- sampRegisterChatCommand("spttest", function()
    --     autoOutfitToggle()
    -- end)

    if cfg.isReloaded then
        imgui_windows.main[0] = true
        cfg.isReloaded = false
        save()
    end

    while true do wait(0)

        if state.autoOutfit.active then
            for k, v in ipairs(getAllChars()) do
                if doesCharExist(v) and playerPed ~= v then
                    setCharCollision(v, false)
                end
            end
            for k, v in ipairs(getAllVehicles()) do
                if doesVehicleExist(v) then
                    setCarCollision(v, false)
                end
            end
            if cfg.autoDoor and AutoDoor() then
                wait(300)
            end
        end

        if state.autoOutfit.recording then

            utils.printStringNow("Идёт запись маршрута...", 100)

            routeRecorder()

        end

        -- local x, y, z = getCharCoordinates(1)
        -- printStringNow(getDistanceBetweenCoords3d(x, y, z, cfg.autoOutfit.pickupPoint[1], cfg.autoOutfit.pickupPoint[2], cfg.autoOutfit.pickupPoint[3]), 100)

    end
end




local autoOutfit_thread = lua_thread.create_suspended(function()


    local exeption = function(reason)

        local gamestate = sampGetGamestate() == 3

        if not state.autoOutfit.active or
        not gamestate or
        isCharInAnyCar(1)
        then
            autoOutfitToggle(not gamestate and "Соединение с сервером разорвано!" or reason or nil)
            return true
        else
            return false
        end
    end


    local routeIndex = 1


    local errorCount = 0

    while state.autoOutfit.active do wait(0)

        if sampIsLocalPlayerSpawned() and doesCharExist(1) then

            exeption()

            -- if state.autoOutfit.state ~= 4 and getCharModel(1) == cfg.autoOutfit.factionSkinId then
            --     state.autoOutfit.state = 4
            --     wait(500)
            --     sampProcessChatInput("/rec")
            -- end

            if state.autoOutfit.state == 1 then


                local isFactionSkin = getCharModel(1) == cfg.autoOutfit.factionSkinId
                
                if isFactionSkin then
                    wait(500)
                    state.autoOutfit.state = 3
                    sampProcessChatInput("/rec")
                    break
                end

                local x, y, z = getCharCoordinates(1)

                local routeCoords = cfg.autoOutfit.route[routeIndex]

                if routeCoords then

                    if routeCoords.type == 1 then

                        if walkTo(routeCoords.x, routeCoords.y, routeCoords.z, routeCoords.run, routeCoords.jump) then
                            routeIndex = routeIndex + 1
                        end

                    elseif routeCoords.type == 2 then

                        if not isFactionSkin then

                            if walkTo(routeCoords.x, routeCoords.y, routeCoords.z, routeCoords.run) then
                                local id = getPickupId(routeCoords.x, routeCoords.y, routeCoords.z)
                                if id then
                                    sampSendPickedUpPickup(id)
                                    wait(200)
                                end
                            end

                        -- else
                        --     --routeIndex = routeIndex + 1
                        --     wait(500)
                        --     state.autoOutfit.state = 3
                        --     sampProcessChatInput("/rec")
                        end

                    -- elseif routeCoords.type == 3 then

                    --     if getDistanceBetweenCoords2d(x, y, routeCoords.x, routeCoords.y) >= 5 then
                    --         routeIndex = routeIndex + 1
                    --     else
                    --         local id = getPickupId(routeCoords.x, routeCoords.y, routeCoords.z)
                    --         if id then
                    --             sampSendPickedUpPickup(id)
                    --             wait(200)
                    --         end
                    --     end

                    end

                else

                    autoOutfitToggle()

                end

            end

        end

    end

end)

function autoOutfitToggle(reason, needReload)



    state.autoOutfit.active = not state.autoOutfit.active

    if state.autoOutfit.active then

        utils.addChat("Запускаю Шиномонтаж...")

        state.autoOutfit.state = 1

        if autoOutfit_thread:status() ~= 'yielded' then
            autoOutfit_thread:run()
        end

    else

        if reason then utils.addChat("Произошло исключение: "..reason) end
        utils.addChat("Останавливаю Шиномонтаж...")

        if autoOutfit_thread:status() == 'yielded' then
            autoOutfit_thread:terminate()
        end

        if needReload then
            state.autoOutfit.active = true
            utils.addChat("Запускаю Шиномонтаж...")

            state.autoOutfit.state = 1
    
            if autoOutfit_thread:status() ~= 'yielded' then
                autoOutfit_thread:run()
            end
        end

    end

end




function sampev.onSetSpawnInfo(team, skin, unk, position, rotation, weapons, ammo)

    if not state.autoOutfit.active then return end

    local pos = cfg.autoOutfit.route[1]

    if pos then
        if getDistanceBetweenCoords2d(pos.x, pos.y, position.x, position.y) >= 10 and state.autoOutfit.state ~= 3 then
            autoOutfitToggle("Неверные координаты спавна!")
        end
    end

end





function sampev.onSendPickedUpPickup(id)

    --print(getPickupModel(id))

    if state.autoOutfit.active then return false end

    if not state.autoOutfit.recording then return end

    if not checkRememberedPickup() and getPickupModel(id) == 1275 then

        local x, y, z = getPickupCoordinates(sampGetPickupHandleBySampId(id))

        -- if cfg.autoOutfit.pickupPoint[1] ~= x and cfg.autoOutfit.pickupPoint[2] ~= y and cfg.autoOutfit.pickupPoint[3] ~= z then
        --     cfg.autoOutfit.pickupPoint = {x, y, z}
        --     utils.addChat("Пикап записан.")
        -- end

        table.insert(cfg.autoOutfit.route, {
            x = x,
            y = y,
            z = z,
            type = 2
        })

        utils.addChat("Пикап переодевания обнаружен. Отключаю запись марщрута...")

        state.autoOutfit.recording = false
        save()

    -- elseif getPickupModel(id) == 19132 then

    --     local x, y, z = getPickupCoordinates(sampGetPickupHandleBySampId(id))

    --     table.insert(cfg.autoOutfit.route, {
    --         x = x,
    --         y = y,
    --         z = z,
    --         type = 3
    --     })

    --     utils.addChat("Пикап записан.")

    end

    -- state.autoOutfit.pickup = false
end



function onReceiveRpc(id, bitStream)
    if cfg.autoOutfit.active then
        if (id == RPC_SCRINITGAME) then
            if (state.autoOutfit.active and (state.autoOutfit.state ~= 3 and state.autoOutfit.active) or false) then
                autoOutfitToggle("Реконнект...", true)
            elseif not state.autoOutfit.active then
                autoOutfitToggle()
            end
            --state.autoOutfit.active = true
        end
    end
end

function onReceivePacket(id, bitStream)	
	
    if state.autoOutfit.active then
        if (id == PACKET_DISCONNECTION_NOTIFICATION) or
        (id == PACKET_CONNECTION_LOST) then
            autoOutfitToggle("Потеряно соединение с сервером...")
        end
    end

end




function sampev.onShowDialog(id, style, title, button1, button2, text)


    -- if title == "{BFBBBA}Выбор места спавна" and button1 == "Выбрать" then

    --     __updateSpawnPoints(text)

    -- end


    if state.autoOutfit.active then
        if title == "{BFBBBA}" then

            if text:find("{42B02C}-{FFFFFF} Переодеться", nil, true) then
                sampSendDialogResponse(id, 1, 0)
                return false
            elseif text:find("Переодеться в {31853A}рабочую{FFFFFF} форму.", nil, true) then
                sampSendDialogResponse(id, 1, 0)
                return false
            elseif text:find("Переодеться в {......}") then
                sampSendDialogResponse(id, 0)
                return false
            end
    
        end
    end


    return autoSelectSpawnLogic(id, style, title, button1, button2, text)
    -- if not state.autoOutfit.active then
    --     return autoSelectSpawnLogic(id, style, title, button1, button2, text)
    -- else
    --     return autoOutfitLogic(id, style, title, button1, button2, text)
    -- end


end

function autoOutfitLogic(id, style, title, button1, button2, text)

    if title == "{BFBBBA}" then

        if text:find("{42B02C}-{FFFFFF} Переодеться", nil, true) then
            sampSendDialogResponse(id, 1, 0)
            return false
        elseif text:find("Переодеться в {31853A}рабочую{FFFFFF} форму.", nil, true) then
            sampSendDialogResponse(id, 1, 0)
            return false
        elseif text:find("Переодеться в {......}") then
            sampSendDialogResponse(id, 0)
            return false
        end

    end

end


function autoSelectSpawnLogic(id, style, title, button1, button2, text)

    if title == "{BFBBBA}Выбор места спавна" and button1 == "Выбрать" then

        cfg.autoSelectSpawn.all = {}

        text = text:gsub("{......}", "")

        local spawnIndex = {1000, 0}

        local num = 0
        for line in text:gmatch("[^\r\n]+") do

            if line:find("^%[%d+%]") then

                local name = line:match("^%[%d+%]%s(.-)$")

                table.insert(cfg.autoSelectSpawn.all, name)

                if state.autoOutfit.active then
                    if state.autoOutfit.state == 1 then
                        if name == cfg.autoOutfit.spawnPoint.organization then
                            spawnIndex = {1, num}
                        end
                    elseif state.autoOutfit.state == 3 then
                        if name == cfg.autoOutfit.spawnPoint.home then
                            spawnIndex = {1, num}
                        end
                    end
                else
                    local k = checkValue(cfg.autoSelectSpawn.selected, name)
                    if k and k < spawnIndex[1] then
                        spawnIndex = {k, num}
                    end
                end

            end

            num = num + 1

        end

        save()

        if not cfg.autoSelectSpawn.active and not state.autoOutfit.active then return end

        if spawnIndex[1] ~= 1000 then

            if state.autoOutfit.active and state.autoOutfit.state == 3 then
                autoOutfitToggle()
            end

            sampSendDialogResponse(id, 1, spawnIndex[2], nil)
            return false
        else
            if state.autoOutfit.active then
                autoOutfitToggle("Нет позиции для спавна!")
            end
            utils.addChat("Нет позиции для спавна!")
        end

    end

end

function __updateSpawnPoints(text)
    cfg.autoSelectSpawn.all = {}

    text = text:gsub("{......}", "")

    for line in text:gmatch("[^\r\n]+") do

        if line:find("^%[%d+%]") then

            local name = line:match("^%[%d+%]%s(.-)$")

            table.insert(cfg.autoSelectSpawn.all, name)

        end

    end

    save()
end




























function AutoDoor()
    for key, hObj in pairs(getAllObjects()) do
        if doesObjectExist(hObj) then
            local objModel = getObjectModel(hObj)
            local res, ox, oy, oz = getObjectCoordinates(hObj)
			local objHeading = getObjectHeading(hObj)
			local px, py, pz = getCharCoordinates(PLAYER_PED)
            local distance = getDistanceBetweenCoords3d(px, py, pz, ox, oy, oz)
            -- двери
            if objModel == 1495 or objModel == 3089 or objModel == 1557 or objModel == 1808 or objModel == 19857 or objModel == 19302 or objModel == 2634 or objModel == 19303 then
                if (objHeading > 179 and objHeading < 181) or (objHeading > 89 and objHeading < 91) or (objHeading > -1 and objHeading < 1) or (objHeading > 269 and objHeading < 271) then
					if distance <= 2 then
                        sampSendChat("/opengate")
                        return true
					end
                end
            -- шлагбаумы и заборы
            elseif objModel == 968 or objModel == 975 or objModel == 1374 or objModel == 19912 or objModel == 988 or objModel == 19313 or objModel == 11327 or objModel == 19313 or objModel == 980 then
				if distance < (isCharInAnyCar(PLAYER_PED) and 12 or 5) then
                    sampSendChat("/opengate")
                    return true
                end
            end
        end
    end
end
function checkValue(array, value)
    for k, v in ipairs(array) do
        if v == value then return k end
    end
    return false
end
function moveElement(array, index, direction)
    if index < 1 or index > #array then
        return
    end

    if direction == "up" and index > 1 then
        array[index], array[index - 1] = array[index - 1], array[index]
    elseif direction == "down" and index < #array then
        array[index], array[index + 1] = array[index + 1], array[index]
    end
end
function walkTo(x, y, z, run, jump)
    local cPosX, cPosY, cPosZ = getActiveCameraCoordinates()
    setCameraPositionUnfixed(0.0, (getHeadingFromVector2d(x - cPosX, y - cPosY) - 90.0) / 57.2957795)
    setGameKeyState(1, -255)

    if jump then
        setGameKeyState(14, 255)
    elseif run then setGameKeyState(16, 255) end

    local posX, posY, posZ = getCharCoordinates(playerPed)
    local dist = getDistanceBetweenCoords2d(posX, posY, x, y)

    if dist <= 1 then
        return true
    end
end
function samp_create_sync_data(sync_type, copy_from_player)

    copy_from_player = copy_from_player or true
    local sync_traits = {
        player = {'PlayerSyncData', raknet.PACKET.PLAYER_SYNC, sampStorePlayerOnfootData},
        vehicle = {'VehicleSyncData', raknet.PACKET.VEHICLE_SYNC, sampStorePlayerIncarData},
        passenger = {'PassengerSyncData', raknet.PACKET.PASSENGER_SYNC, sampStorePlayerPassengerData},
        aim = {'AimSyncData', raknet.PACKET.AIM_SYNC, sampStorePlayerAimData},
        trailer = {'TrailerSyncData', raknet.PACKET.TRAILER_SYNC, sampStorePlayerTrailerData},
        unoccupied = {'UnoccupiedSyncData', raknet.PACKET.UNOCCUPIED_SYNC, nil},
        bullet = {'BulletSyncData', raknet.PACKET.BULLET_SYNC, nil},
        spectator = {'SpectatorSyncData', raknet.PACKET.SPECTATOR_SYNC, nil}
    }
    local sync_info = sync_traits[sync_type]
    local data_type = 'struct ' .. sync_info[1]
    local data = ffi.new(data_type, {})
    local raw_data_ptr = tonumber(ffi.cast('uintptr_t', ffi.new(data_type .. '*', data)))
    -- copy player's sync data to the allocated memory
    if copy_from_player then
        local copy_func = sync_info[3]
        if copy_func then
            local _, player_id
            if copy_from_player == true then
                _, player_id = sampGetPlayerIdByCharHandle(PLAYER_PED)
            else
                player_id = tonumber(copy_from_player)
            end
            copy_func(player_id, raw_data_ptr)
        end
    end
    -- function to send packet
    local func_send = function()
        local bs = raknetNewBitStream()
        raknetBitStreamWriteInt8(bs, sync_info[2])
        raknetBitStreamWriteBuffer(bs, raw_data_ptr, ffi.sizeof(data))
        raknetSendBitStreamEx(bs, sampfuncs.HIGH_PRIORITY, sampfuncs.UNRELIABLE_SEQUENCED, 1)
        raknetDeleteBitStream(bs)
    end
    -- metatable to access sync data and 'send' function
    local mt = {
        __index = function(t, index)
            return data[index]
        end,
        __newindex = function(t, index, value)
            data[index] = value
        end
    }
    return setmetatable({send = func_send}, mt)
end
function getPickupId(pX, pY, pZ)
    for id = 0, 4095 do
        local pickup = sampGetPickupHandleBySampId(id)
        if doesPickupExist(pickup) then
            local x, y, z = getPickupCoordinates(pickup)
            local bX = x == pX
            local bY = y == pY
            local bZ = z == pZ
            if bX and bY and bZ then
                return id
            end
        end
    end
    return false
end
function routeRecorder()
    local x, y, z = getCharCoordinates(1)
    local v = cfg.autoOutfit.route[#cfg.autoOutfit.route]
    if not v or (x ~= v.x or y ~= v.y or z ~= v.z) then

        if v and v.type == 3 then
            if getDistanceBetweenCoords2d(x, y, v.x, v.y) >= 5 then
                table.insert(cfg.autoOutfit.route, {
                    x = x,
                    y = y,
                    z = z,
                    type = 1,
                    run = isButtonPressed(PLAYER_HANDLE, 16) and true or false,
                    jump = isButtonPressed(PLAYER_HANDLE, 14) and true or false
                })
            end
        else
            table.insert(cfg.autoOutfit.route, {
                x = x,
                y = y,
                z = z,
                type = 1,
                run = isButtonPressed(PLAYER_HANDLE, 16) and true or false,
                jump = isButtonPressed(PLAYER_HANDLE, 14) and true or false
            })
        end

        wait(100)
    end
end
function getPickupModel(id) -- https://www.blast.hk/threads/13380/page-15#post-604316
    local PICKUP_POOL = sampGetPickupPoolPtr()
    return ffi.cast("int *", (id * 20 + 61444) + PICKUP_POOL)[0]
end
function checkRememberedPickup()
    for k, v in pairs(cfg.autoOutfit.route) do

        if v.type == 2 then return true end

    end
    return false
end
function optimize_route(points, tolerance, extra_points)
    -- Проверяем, что массив точек не пуст
    if #points < 3 then
        return points
    end

    local optimized_points = {points[1]} -- Начинаем с первой точки

    for i = 2, #points - 1 do
        local prev = points[i - 1]
        local current = points[i]
        local next = points[i + 1]

        -- Вычисляем векторы между точками
        local vector1 = {x = current.x - prev.x, y = current.y - prev.y, z = current.z - prev.z}
        local vector2 = {x = next.x - current.x, y = next.y - current.y, z = next.z - current.z}

        -- Вычисляем длины векторов
        local length1 = math.sqrt(vector1.x^2 + vector1.y^2 + vector1.z^2)
        local length2 = math.sqrt(vector2.x^2 + vector2.y^2 + vector2.z^2)

        -- Нормализуем векторы
        vector1 = {x = vector1.x / length1, y = vector1.y / length1, z = vector1.z / length1}
        vector2 = {x = vector2.x / length2, y = vector2.y / length2, z = vector2.z / length2}

        -- Вычисляем косинус угла между векторами
        local dot_product = vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z

        -- Если точка является поворотом (угол отличается от 0 или 180 градусов)
        if math.abs(1 - dot_product) > tolerance then
            -- Добавляем текущую точку как есть
            table.insert(optimized_points, current)

            -- Вставляем дополнительные точки для плавного поворота
            if extra_points > 0 then
                local dx = (next.x - prev.x) / (extra_points + 1)
                local dy = (next.y - prev.y) / (extra_points + 1)
                local dz = (next.z - prev.z) / (extra_points + 1)

                for j = 1, extra_points do
                    local new_point = {
                        x = prev.x + dx * j,
                        y = prev.y + dy * j,
                        z = prev.z + dz * j
                    }
                    -- Сохраняем остальные ключи из текущей точки
                    for key, value in pairs(current) do
                        if new_point[key] == nil then
                            new_point[key] = value
                        end
                    end
                    table.insert(optimized_points, new_point)
                end
            end
        else
            -- Если нет поворота, точка считается избыточной и удаляется
            if #optimized_points == 0 or optimized_points[#optimized_points] ~= prev then
                table.insert(optimized_points, prev)
            end
        end
    end

    table.insert(optimized_points, points[#points]) -- Добавляем последнюю точку

    return optimized_points
end


















----------------------------------------------------------------INTERFACE----------------------------------------------------------------


local fa = require('fAwesome6')

imgui.OnInitialize(function()

    imgui.GetIO().IniFilename = nil
    local config = imgui.ImFontConfig()
    config.MergeMode = true
    config.PixelSnapH = true
    iconRanges = imgui.new.ImWchar[3](fa.min_range, fa.max_range, 0)
    imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(fa.get_font_data_base85('solid'), 14, config, iconRanges)

    do
        imgui.SwitchContext()
        --==[ STYLE ]==--
        imgui.GetStyle().WindowPadding = imgui.ImVec2(5, 5)
        imgui.GetStyle().FramePadding = imgui.ImVec2(5, 5)
        imgui.GetStyle().ItemSpacing = imgui.ImVec2(5, 5)
        imgui.GetStyle().ItemInnerSpacing = imgui.ImVec2(2, 2)
        imgui.GetStyle().TouchExtraPadding = imgui.ImVec2(0, 0)
        imgui.GetStyle().IndentSpacing = 0
        imgui.GetStyle().ScrollbarSize = 10
        imgui.GetStyle().GrabMinSize = 10
    
        --==[ BORDER ]==--
        imgui.GetStyle().WindowBorderSize = 1
        imgui.GetStyle().ChildBorderSize = 1
        imgui.GetStyle().PopupBorderSize = 1
        imgui.GetStyle().FrameBorderSize = 0
        imgui.GetStyle().TabBorderSize = 1
    
        --==[ ROUNDING ]==--
        imgui.GetStyle().WindowRounding = 5
        imgui.GetStyle().ChildRounding = 5
        imgui.GetStyle().FrameRounding = 5
        imgui.GetStyle().PopupRounding = 5
        imgui.GetStyle().ScrollbarRounding = 5
        imgui.GetStyle().GrabRounding = 5
        imgui.GetStyle().TabRounding = 5
    
        --==[ ALIGN ]==--
        imgui.GetStyle().WindowTitleAlign = imgui.ImVec2(0.5, 0.5)
        imgui.GetStyle().ButtonTextAlign = imgui.ImVec2(0.5, 0.5)
        imgui.GetStyle().SelectableTextAlign = imgui.ImVec2(0.5, 0.5)
        
        --==[ COLORS ]==--
        imgui.GetStyle().Colors[imgui.Col.Text]                   = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TextDisabled]           = imgui.ImVec4(0.50, 0.50, 0.50, 1.00)
        imgui.GetStyle().Colors[imgui.Col.WindowBg]               = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ChildBg]                = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PopupBg]                = imgui.ImVec4(0.07, 0.07, 0.07, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Border]                 = imgui.ImVec4(0.25, 0.25, 0.26, 0.54)
        imgui.GetStyle().Colors[imgui.Col.BorderShadow]           = imgui.ImVec4(0.00, 0.00, 0.00, 0.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBgHovered]         = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
        imgui.GetStyle().Colors[imgui.Col.FrameBgActive]          = imgui.ImVec4(0.25, 0.25, 0.26, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBg]                = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBgActive]          = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TitleBgCollapsed]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.MenuBarBg]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarBg]            = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrab]          = imgui.ImVec4(0.00, 0.00, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabHovered]   = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ScrollbarGrabActive]    = imgui.ImVec4(0.51, 0.51, 0.51, 1.00)
        imgui.GetStyle().Colors[imgui.Col.CheckMark]              = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SliderGrab]             = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SliderGrabActive]       = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Button]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ButtonHovered]          = imgui.ImVec4(0.21, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ButtonActive]           = imgui.ImVec4(0.41, 0.41, 0.41, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Header]                 = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.HeaderHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
        imgui.GetStyle().Colors[imgui.Col.HeaderActive]           = imgui.ImVec4(0.47, 0.47, 0.47, 1.00)
        imgui.GetStyle().Colors[imgui.Col.Separator]              = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SeparatorHovered]       = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.SeparatorActive]        = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.ResizeGrip]             = imgui.ImVec4(1.00, 1.00, 1.00, 0.25)
        imgui.GetStyle().Colors[imgui.Col.ResizeGripHovered]      = imgui.ImVec4(1.00, 1.00, 1.00, 0.67)
        imgui.GetStyle().Colors[imgui.Col.ResizeGripActive]       = imgui.ImVec4(1.00, 1.00, 1.00, 0.95)
        imgui.GetStyle().Colors[imgui.Col.Tab]                    = imgui.ImVec4(0.12, 0.12, 0.12, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabHovered]             = imgui.ImVec4(0.28, 0.28, 0.28, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabActive]              = imgui.ImVec4(0.30, 0.30, 0.30, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TabUnfocused]           = imgui.ImVec4(0.07, 0.10, 0.15, 0.97)
        imgui.GetStyle().Colors[imgui.Col.TabUnfocusedActive]     = imgui.ImVec4(0.14, 0.26, 0.42, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotLines]              = imgui.ImVec4(0.61, 0.61, 0.61, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotLinesHovered]       = imgui.ImVec4(1.00, 0.43, 0.35, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotHistogram]          = imgui.ImVec4(0.90, 0.70, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.PlotHistogramHovered]   = imgui.ImVec4(1.00, 0.60, 0.00, 1.00)
        imgui.GetStyle().Colors[imgui.Col.TextSelectedBg]         = imgui.ImVec4(1.00, 0.00, 0.00, 0.35)
        imgui.GetStyle().Colors[imgui.Col.DragDropTarget]         = imgui.ImVec4(1.00, 1.00, 0.00, 0.90)
        imgui.GetStyle().Colors[imgui.Col.NavHighlight]           = imgui.ImVec4(0.26, 0.59, 0.98, 1.00)
        imgui.GetStyle().Colors[imgui.Col.NavWindowingHighlight]  = imgui.ImVec4(1.00, 1.00, 1.00, 0.70)
        imgui.GetStyle().Colors[imgui.Col.NavWindowingDimBg]      = imgui.ImVec4(0.80, 0.80, 0.80, 0.20)
        imgui.GetStyle().Colors[imgui.Col.ModalWindowDimBg]       = imgui.ImVec4(0.00, 0.00, 0.00, 0.70)
    end
end)





local imTabs = 1
local notifications = {}
local windowWidth, windowHeight = 500, 600
imgui.OnFrame(function() return imgui_windows.main[0] end, function(self) --[[self.HideCursor = true]]
    local screenWidth, screenHeight = getScreenResolution()

    imgui.SetNextWindowSize(imgui.ImVec2(windowWidth, windowHeight), imgui.Cond.FirstUseEver)
    imgui.SetNextWindowPos(imgui.ImVec2(screenWidth/2-windowWidth/2, screenHeight/2-windowHeight/2), imgui.Cond.FirstUseEver)
    imgui.Begin('##main_windos', imgui_windows.main, imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoResize)

    imgui.customTitleBar(imgui_windows.main, resetDefaultCfg, imgui.GetWindowWidth())

    imgui.Separator()

    imgui.BeginChild('##globalChildWrapper', imgui.ImVec2(0, imgui.GetWindowHeight()-imgui.GetStyle().ItemSpacing.y*2-70), false)

        __mainTabBar()

    imgui.EndChild()

    if imgui.advBanner(
        fa("FILE_CODE") .. u8(" Palatka.lua - получи 1 мес. бесплатно! ") .. fa("FILE_CODE"),
        imgui.ImVec2(-1,25),
        3,
        u8"Нажмите меня!"
    ) then
        imgui.OpenPopup('palatka_buy_popup')
    end

    if imgui.BeginPopupModal('palatka_buy_popup', nil, imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoResize) then
        imgui.TextColoredRGB("{ffffff}Вы слышали про лучший скрипт для барыг - {99ff99}Palatka.lua?\n" ..
        "{ffffff}Этот скрипт значительно упрощает вам игру на ЦР, выставляя товары и показывая средние цены.\n" ..
        "{ffffff}Для вас у меня есть уникальный промокод {99ff99}#justfedot {ffffff}который даст вам + 1 месяц подписки бесплатно!"
        )

        imgui.NewLine()

        imgui.TextColoredRGB("{ffffff}Промокод {99ff99}#justfedot{ffffff} нужно указать в переписке с автором.\nПишите прямо сейчас что-бы стать лучшим барыгой на сервере!")

        imgui.Text(u8"Связь с автором Palatka.lua:")
        imgui.SameLine()
        if imgui.Link("t.me/m/3L4CfO4gN2Y6", u8"Нажмите для копирования") then
            imgui.SetClipboardText("t.me/m/3L4CfO4gN2Y6")
            utils.addChat("Ссылка скопирована в буфер обмена.")
            imgui.addNotification(u8"Скопировано!")
        end

        -- if imgui.GetClipboardText() == "t.me/m/3L4CfO4gN2Y6" then
        --     imgui.SameLine()
        --     imgui.TextDisabled(u8("(Скопировано)"))
        -- end
        
        imgui.NewLine()

        imgui.SetCursorPosX(imgui.GetWindowWidth()/2-50)
        if imgui.Button(u8'Закрыть##closePopupModal', imgui.ImVec2(100, 0)) then
            imgui.CloseCurrentPopup()
        end
        imgui.EndPopup()
    end

    imgui.showNotifications(2)

    imgui.End()

end)




function __mainTabBar()

    if imgui.BeginTabBar("Main Tabs") then

        if imgui.BeginTabItem(u8"Авто-выбор спавна") then

            __autoSelectSpawnTab()

            imgui.EndTabItem()
        end


        if imgui.BeginTabItem(u8"Шиномонтаж") then

            __autoOutfitTab()

            imgui.EndTabItem()
        end


        if imgui.BeginTabItem(u8"Общие настройки") then

            __otherSettingsTab()

            imgui.EndTabItem()
        end


        imgui.EndTabBar()
    end
    

end


function __otherSettingsTab()

    imgui.BeginChild('##settingsTab', imgui.ImVec2(0, 0), true)


        if imgui.Checkbox(u8'Сайлент-Мод', imcfg.silentMode) then
            cfg.silentMode = imcfg.silentMode[0]
            save()
        end
        imgui.Hint(u8(
            "Если включено, вы не увидите никаких сообщений в чат от скрипта.\n"..
            "Под фразой 'никаких', я имею ввиду вообще никаких, никогда."
        ), true)


    imgui.End()

end




function __autoOutfitTab()


    if imgui.Checkbox(u8'Активация', imcfg.autoOutfit.active) then
        if state.autoOutfit.active then autoOutfitToggle("Ручная остановка.") end
        cfg.autoOutfit.active = imcfg.autoOutfit.active[0]
        save()
    end
    if imgui.IsItemHovered() then
        imgui.SetMouseCursor(imgui.MouseCursor.Hand)
    end
    imgui.Hint(u8(
        "Если включено, будет переодеваться при входе на сервер."
    ))

    imgui.SameLine()

    if imgui.Checkbox(u8'Авто-Двери', imcfg.autoDoor) then
        cfg.autoDoor = imcfg.autoDoor[0]
        save()
    end
    imgui.Hint(u8(
        "AutoDoor вырезанный из одноименного скрипта.\nЭто вам не нужно, если на пути нет дверей, или у вас установлен оригинал."
    ), true)

    imgui.SameLine()

    imgui.SetCursorPosX(imgui.GetWindowWidth() - 110 - imgui.GetStyle().ItemSpacing.x)
    if imgui.Button(fa("CIRCLE_INFO") .. u8" Информация", imgui.ImVec2(110, 0)) then
        imgui.OpenPopup('autoOutfitHelpPopup')
    end
    if imgui.IsItemHovered() then
        imgui.SetMouseCursor(imgui.MouseCursor.Hand)
    end

    if imgui.BeginPopup('autoOutfitHelpPopup') then

        imgui.TextColoredRGB(
            "{99ff99}Что делает этот скрипт?\n" ..
            "{abcdef}\tЭтот скрипт автоматически заставляет вашего персонажа появляться (спавниться) в одной точке, переодеваться в нужную форму и затем перезаходить на другую точку спавна."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Важная информация!\n" ..
            "{abcdef}\tВсе жёлтые тексты в этом окне — это кнопки! Нажимайте на них, чтобы выполнять действия.\n" ..
            "{abcdef}\tДля работы скрипта у вас должен быть установлен 'Реконнект от Air' (доступна команда /rec). В противном случае корректное использование скрипта невозможно."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Как настроить скрипт? Пошаговая инструкция:\n" ..
            "{abcdef}\t1. Установите точки спавна для вашей Организации и Дома.\n" ..
            "{abcdef}\t   - Нажмите на соответствующий жёлтый текст. Откроется список доступных точек спавна.\n" ..
            "{abcdef}\t   - Если список пустой, значит, скрипт не запомнил точки. Отключите функцию 'Шиномонтаж' (так шутливо называется Авто-Форма), затем перезайдите на сервер. Это откроет окно выбора спавна, и скрипт запомнит точки. Повторите этот шаг, если нужно."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Как задать ID модели фракционного скина?\n" ..
            "{abcdef}\t2. Наденьте рабочую форму вашего персонажа.\n" ..
            "{abcdef}\t   - После этого нажмите на жёлтый текст в нужном месте в окне. Скрипт запомнит ID формы автоматически."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Как задать координаты пикапа для переодевания?\n" ..
            "{abcdef}\t3. Нажмите на соответствующий жёлтый текст. Скрипт включит режим перехвата.\n" ..
            "{abcdef}\t   - Когда режим перехвата включится, в чат придёт уведомление.\n" ..
            "{abcdef}\t   - Подойдите к пикапу переодевания и возьмите его. Координаты сохранятся автоматически, а режим отключится."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Как добавить точки маршрута?\n" ..
            "{abcdef}\t4. Добавьте нужное количество точек маршрута с помощью соответствующей кнопки.\n" ..
            "{abcdef}\t   - Внизу вы увидите список точек маршрута с их координатами. Нажимайте на жёлтый текст с координатами, чтобы установить текущие координаты вашего персонажа.\n" ..
            "{abcdef}\t   - Используйте точки маршрута, чтобы указать боту, как обойти препятствия. Если точка спавна и пикап не видны напрямую, добавьте точки так, чтобы бот обходил препятствия, как и любой другой бот."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Настройка опции 'Авто-Двери':\n" ..
            "{abcdef}\t5. Если на пути вашего персонажа есть двери, включите галочку 'Авто-Двери'. Эта функция повторяет работу скрипта AutoDoor.lua.\n" ..
            "{abcdef}\t   - Если у вас уже установлен AutoDoor.lua, галочку включать не нужно."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Финальный шаг: Активация скрипта\n" ..
            "{abcdef}\t6. После завершения всех настроек включите галочку 'Активация'.\n" ..
            "{abcdef}\t   - Пока эта галочка активна, скрипт будет автоматически работать каждый раз, когда вы заходите на сервер."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Важное предупреждение!\n" ..
            "{abcdef}\tЭтот скрипт несовместим с другим скриптом, называемым AutoSelectSpawn.lua.\n" ..
            "{abcdef}\t   - Если использовать их вместе, могут возникнуть баги, которые могут привести к блокировке вашего аккаунта!"
        )
    
        imgui.EndPopup()
    end


    imgui.BeginChild('##autoOutfitChild', imgui.ImVec2(0, 0), true)


        __autoOutfitUpPanel()


        imgui.Separator()
        imgui.CenterTextColoredRGB("Точки Маршрута")
        imgui.Separator()

        if imgui.Button(fa("SQUARE_PLUS").. u8(state.autoOutfit.recording and " Отключить запись маршрута" or " Включить запись маршрута") .."##addNewRoutePoint", imgui.ImVec2(200, 0)) then
            state.autoOutfit.recording = not state.autoOutfit.recording
            if state.autoOutfit.recording then
                cfg.autoOutfit.route = {}
            else
                utils.addChat("Маршрут сохранён.")
                --cfg.autoOutfit.route = optimize_route(cfg.autoOutfit.route, 0.01, 0)
                save()
            end
            -- table.insert(cfg.autoOutfit.route, {
            --     x = 0, y = 0, z = 0
            -- })
            -- save()
        end
        imgui.Hint(u8(
            "Включает/Отключает запись маршрута.\n" ..
            "Инструкция:\n" ..
            "\t1. Перезайдите на точку спавна организации.\n" ..
            "\t2. Не двигаясь с места, нажмите эту кнопку.\n" ..
            "\t3. Во время записи маршрута, просто бегите на пикап переодевания как обычно.\n" ..
            "\t\t(( маршрут будет записан как только вы возьмёте пикап переодевания ))"
        ), true)

        imgui.SameLine()

        imgui.SetCursorPosX(imgui.GetWindowWidth() - imgui.GetStyle().ItemSpacing.x - 200)
        if imgui.Button(fa("TRASH_CAN")..u8(" Очистить маршрут").."##cleanUpRoute", imgui.ImVec2(200, 0)) then
            cfg.autoOutfit.route = {}
            save()
            utils.addChat("Маршурт очищен.")
            imgui.addNotification(u8"Маршурт очищен.")
        end
        imgui.Hint(u8(
            "Очистить список маршрута."
        ), true)


        imgui.BeginChild('##routeChild', imgui.ImVec2(0, 0), true)

            for index, value in pairs(cfg.autoOutfit.route) do

                imgui.TextColoredRGB(
                    "{99ff99}" .. index .. ". {ffffff}" .. (value.type == 1 and "Коорд" or "{ffa500}Пикап") .. ": {99ff99}X: " .. math.floor(value.x) .. " Y: " .. math.floor(value.y) .. " Z: " .. math.floor(value.z)
                )

            end

        imgui.EndChild()


    imgui.EndChild()


end

function __autoOutfitUpPanel()

    imgui.TextColoredRGB(
        "{ffffff}Точка спавна Организации:"
    )
    imgui.SameLine()
    if imgui.SelectText(u8(cfg.autoOutfit.spawnPoint.organization).."##autoOutfitButtonorganization", u8("Нажмите для выбора точки.")) then
        imgui.OpenPopup('selectOrganizationSpawnPoint')
    end

    if imgui.BeginPopup("selectOrganizationSpawnPoint") then

        for index, value in ipairs(cfg.autoSelectSpawn.all) do

            if imgui.Selectable(u8(value).."##selectebleHuecleble"..index) then
                cfg.autoOutfit.spawnPoint.organization = value
                save()
            end
            if imgui.IsItemHovered() then
                imgui.SetMouseCursor(imgui.MouseCursor.Hand)
            end
            imgui.Hint(u8("Выбрать"))

        end

        imgui.EndPopup()
    end


    imgui.TextColoredRGB(
        "{ffffff}Точка спавна Дома:"
    )
    imgui.SameLine()
    if imgui.SelectText(u8(cfg.autoOutfit.spawnPoint.home).."##autoOutfitButtonhome", u8("Нажмите для выбора точки.")) then
        imgui.OpenPopup('selecthomeSpawnPoint')
    end

    if imgui.BeginPopup("selecthomeSpawnPoint") then

        for index, value in ipairs(cfg.autoSelectSpawn.all) do

            if imgui.Selectable(u8(value).."##selectebleHuecleble"..index) then
                cfg.autoOutfit.spawnPoint.home = value
                save()
            end
            if imgui.IsItemHovered() then
                imgui.SetMouseCursor(imgui.MouseCursor.Hand)
            end
            imgui.Hint(u8("Выбрать"))

        end

        imgui.EndPopup()
    end


    imgui.TextColoredRGB(
        "{ffffff}Организационный скин:"
    )
    imgui.SameLine()
    if imgui.SelectText(u8(cfg.autoOutfit.factionSkinId), u8("Нажмите что-бы установить текущий.")) then
        if doesCharExist(1) then
            local id = getCharModel(1)
            if id and id ~= 0 then
                cfg.autoOutfit.factionSkinId = id
                save()
                utils.addChat("Модель скина сохранена.")
                imgui.addNotification(u8("Модель скина сохранена."))
            else
                utils.addChat("Ошибка получения модели персонажа!")
                imgui.addNotification(u8("Ошибка получения модели персонажа!"))
            end
        else
            utils.addChat("Ваш персонаж не существует!")
            imgui.addNotification(u8("Ваш персонаж не существует!"))
        end
    end

end





















function __autoSelectSpawnTab()

    if imgui.Checkbox(u8'Активация', imcfg.autoSelectSpawn.active) then
        cfg.autoSelectSpawn.active = imcfg.autoSelectSpawn.active[0]
        save()
    end
    if imgui.IsItemHovered() then
        imgui.SetMouseCursor(imgui.MouseCursor.Hand)
    end
    imgui.Hint(u8(
        "Если активно, будет выбирать точку спавна исходя из приоритета."
    ), true)

    imgui.SameLine()

    imgui.SetCursorPosX(imgui.GetWindowWidth() - 110 - imgui.GetStyle().ItemSpacing.x)
    if imgui.Button(fa("CIRCLE_INFO") .. u8" Информация", imgui.ImVec2(110, 0)) then
        imgui.OpenPopup('autoSelectSpawnHelpPopup')
    end
    if imgui.IsItemHovered() then
        imgui.SetMouseCursor(imgui.MouseCursor.Hand)
    end

    if imgui.BeginPopup('autoSelectSpawnHelpPopup') then

        imgui.TextColoredRGB(
            "{99ff99}Галочка: Активация\n" ..
            "{abcdef}\tВключает и отключает функционал Автовыбора точки спавна."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Левая панель\n" ..
            "{abcdef}\tСодержит в себе текущие точки спавна, взятые из последнего диалога \"Выбор точки спавна\"."
        )
        imgui.Separator()
        imgui.TextColoredRGB(
            "{99ff99}Правая панель\n" ..
            "{abcdef}\tСодержит в себе выбранные точки спавна.\n" ..
            "{abcdef}\tПри заходе на сервер, точки выбираются по приоритету.(сверху вниз)\n" ..
            "{abcdef}\tНажатие на точку удаляет её из списка.\n" ..
            "{abcdef}\tТак же можно зажать, и перетащить для изменения приоритета выбора."
        )

        imgui.EndPopup()
    end


    imgui.BeginChild('##childWrapper', imgui.ImVec2(0, 0), false)


        imgui.BeginChild('##allSpawnPoints', imgui.ImVec2(imgui.GetWindowWidth()/2-imgui.GetStyle().ItemSpacing.x, 0), true)

            __autoSelectSpawnParser(1, fa("SQUARE_ARROW_RIGHT"))

        imgui.EndChild()

        imgui.SameLine()

        imgui.BeginChild('##selectedSpawnPoints', imgui.ImVec2(0, 0), true)

            __autoSelectSpawnParser(2, fa("SQUARE_ARROW_LEFT"))

        imgui.EndChild()


    imgui.EndChild()


end


function __autoSelectSpawnParser(id, label)

    if id == 1 then

        for index, name in ipairs(cfg.autoSelectSpawn.all) do

            if checkValue(cfg.autoSelectSpawn.selected, name) then

                imgui.TextDisabled(u8(name))
        
                imgui.SameLine()
        
                imgui.SetCursorPosX(
                    imgui.GetWindowWidth() - imgui.CalcTextSize(label).x - imgui.GetStyle().ItemSpacing.x
                )
                imgui.TextDisabled(label)

            else

                if imgui.Selectable(u8(
                    name .. "##selectedSukaBlya"..index
                )) then
                    table.insert(cfg.autoSelectSpawn.selected, name)
                    save()
                end

                if imgui.IsItemHovered() then
                    imgui.SetMouseCursor(imgui.MouseCursor.Hand)
                end
        
                imgui.SameLine()
        
                imgui.SetCursorPosX(
                    imgui.GetWindowWidth() - imgui.CalcTextSize(label).x - imgui.GetStyle().ItemSpacing.x
                )
                imgui.Text(label)

            end

        end

    elseif id == 2 then

        for index, name in ipairs(cfg.autoSelectSpawn.selected) do
        
            if imgui.Selectable(label .. ' ' .. u8(name .. "##selectedSukaBlya" .. index)) then
                table.remove(cfg.autoSelectSpawn.selected, index)
                save()
            end

            if imgui.IsItemHovered() then
                imgui.SetMouseCursor(imgui.MouseCursor.Hand)
            end

            imgui.Hint(u8(
                "Нажмите для удаления.\nЗажмите и перетаскивайте для изменения порядка."
            ))
        
            if imgui.BeginDragDropSource() then
                local payload = tostring(index)
                imgui.SetDragDropPayload("selectedSpawnPlayload", payload, #payload)
                imgui.Text(u8(name))
                imgui.SetMouseCursor(imgui.MouseCursor.ResizeNS)
                imgui.EndDragDropSource()
            end
        
            if imgui.BeginDragDropTarget() then

                local payload = imgui.AcceptDragDropPayload("selectedSpawnPlayload")
                if payload ~= nil then

                    local payloadData = ffi.string(payload.Data, payload.DataSize)
                    local payloadIndex = tonumber(payloadData)

                    if payloadIndex and payloadIndex ~= index and cfg.autoSelectSpawn.selected[payloadIndex] then

                        cfg.autoSelectSpawn.selected[index], cfg.autoSelectSpawn.selected[payloadIndex] = 
                        cfg.autoSelectSpawn.selected[payloadIndex], cfg.autoSelectSpawn.selected[index]
        
                        save()
                    end

                end
                imgui.EndDragDropTarget()
            end
        end

    end

end





























----------------------------------------------------------------INTERFACE SNIPPETS----------------------------------------------------------------

function imgui.customTitleBar(param, resetFunc, windowWidth)

    local imStyle = imgui.GetStyle()


    
    imgui.SetCursorPosY(imStyle.ItemSpacing.y+5)
    if imgui.Link("t.me/justfedotScript", u8("Telegram канал автора.\nНажми чтобы перейти/скопировать")) then
        imgui.addNotification(u8"Ссылка скопирована!")
        imgui.SetClipboardText("https://t.me/justfedotScript")
        os.execute(('explorer.exe "%s"'):format("https://t.me/justfedotScript"))
    end


    imgui.SameLine()
    imgui.SetCursorPosX((windowWidth - 170 - imStyle.ItemSpacing.x + imgui.CalcTextSize("t.me/justfedotScript").x)/2 - imgui.CalcTextSize(script.this.name .. ": " .. script.this.version).x/2)
    imgui.TextColoredRGB(script.this.name .. ": " .. script.this.version)


    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 170 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa('DOLLAR_SIGN').."##popup_donation_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("donationPopupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 110 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa("ARROW_TREND_DOWN").."##popup_menu_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("upWindowPupupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(windowWidth - 50 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa("XMARK").."##close_button", imgui.ImVec2(50, 25)) then
        param[0] = false
    end


    if imgui.BeginPopup("upWindowPupupMenu") then
        
        imgui.TextColoredRGB("Доп. Функции:")
        imgui.Separator()

        if imgui.Selectable(u8("Перезагрузить скрипт").."##reloadScriptButton", false) then
            cfg.isReloaded = true
            save()
            thisScript():reload()
        end
        if imgui.Selectable(u8("Сбросить все настройки").."##resetSettingsButton", false) then
            resetFunc()
        end
    
        imgui.EndPopup()
    end

    if imgui.BeginPopup("donationPopupMenu") then
        imgui.Text(u8("Если вы желаете выразить поддержку, можете поддержать автора.\nНа данный момент доступен перевод на Укр. Карту 'Monobank'."))
        if imgui.Link("5375 4112 2231 4945", u8"Нажмите что-бы скопировать.") then
            imgui.SetClipboardText("5375411222314945")
            utils.addChat("Номер карты {99ff99}скопирован.")
            imgui.addNotification(u8"Номер карты скопирован.")
        end
        imgui.TextColoredRGB(("{ffffff}Имя на карте: {99ff99}Михайло Б.\n{ffffff}В комментарии к платежу укажите {99ff99}'"..script.this.name.."' {ffffff}что-бы я понимал актуальность скрипта.\n{ffa500}Большое спасибо за поддержку."))
    
        imgui.EndPopup()
    end


    --imgui.Separator()

    
end
function imgui.Link(label, description)
    local size, p, p2 = imgui.CalcTextSize(label), imgui.GetCursorScreenPos(), imgui.GetCursorPos()
    local result = imgui.InvisibleButton(label, size)
    imgui.SetCursorPos(p2)

    if imgui.IsItemHovered() then
        if description then
            imgui.BeginTooltip()
            imgui.PushTextWrapPos(600)
            imgui.TextUnformatted(description)
            imgui.PopTextWrapPos()
            imgui.EndTooltip()
        end
        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
        imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y + size.y), imgui.ImVec2(p.x + size.x, p.y + size.y), imgui.GetColorU32(imgui.Col.CheckMark))
    else
        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
    end

    return result
end
function imgui.addNotification(text)
    table.insert(notifications, {
        text = text,
        startTime = os.clock()
    })
end
function imgui.showNotifications(duration)
    local currentTime = os.clock()
    local activeNotifications = #notifications

    -- Начинаем отображение подсказок, если есть активные уведомления
    if activeNotifications ~= 0 then
        imgui.BeginTooltip()
    end
    for i = #notifications, 1, -1 do
        local notification = notifications[i]
        -- Проверяем, прошло ли время показа
        if currentTime - notification.startTime < duration then
            imgui.Text(notification.text)
            activeNotifications = activeNotifications + 1
            -- Если это не последнее уведомление, добавляем разделитель
            if i > 1 then
                imgui.Separator()
            end
        else
            table.remove(notifications, i)
        end
    end

    if activeNotifications ~= 0 then
        imgui.EndTooltip()
    end
end
function imgui.CenterTextColoredRGB(text)
    local c = text:gsub("{......}","")
    imgui.SetCursorPosX(imgui.GetWindowSize().x / 2 - imgui.CalcTextSize(u8(c)).x / 2)
    imgui.TextColoredRGB(text)
end
function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImVec4(r/255, g/255, b/255, a/255)
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end
--[[
    Создает анимированный баннер с заданным текстом, который плавно переливается цветами. 
    Баннер может быть выполнен в виде стандартной кнопки, кликабельного текста или кликабельного текста с выравниванием по центру. 
    При наведении на баннер его цвет инвертируется, и, при необходимости, отображается подсказка.
    Возвращает `true`, если баннер был нажат.

    Параметры:
    - text: текст, который будет отображаться на баннере.
    - size: размер баннера (объект imgui.ImVec2). Если передано (0, 0), размер будет автоматически рассчитан.
    - bannerType: тип баннера:
        1 - стандартная кнопка,
        2 - кликабельный текст,
        3 - кликабельный текст с выравниванием по центру.
    - tooltip: (необязательный) текст подсказки, который отображается при наведении на баннер.

    Пример использования:
    if imgui.advBanner("Нажми меня", imgui.ImVec2(0, 0), 2, "Это подсказка") then
        print("Баннер был нажат!")
    end
]]
function imgui.advBanner(text, size, bannerType, tooltip)
    local animationSpeed, hovered, clicked = 0.5, false, false
    local animationColor = imgui.ImVec4(1, 0, 0, 1)

    local function sinColorTransition(speed, offset)
        return (math.sin(os.clock() * speed + offset) + 1) / 2
    end

    animationColor.x, animationColor.y, animationColor.z = 
        sinColorTransition(animationSpeed, 0), 
        sinColorTransition(animationSpeed, math.pi), 
        sinColorTransition(animationSpeed, math.pi / 2)

    if bannerType == 1 then
        local textSize = imgui.CalcTextSize(text)
        size = size or imgui.ImVec2(0, 0)
        if size.x == 0 then size.x = textSize.x + 5 end
        if size.y == 0 then size.y = textSize.y + 5 end

        imgui.PushStyleColor(imgui.Col.Button, animationColor)
        imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1))
        imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1))
        imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0, 0, 0, 0))

        if imgui.Button("##banner"..text, size) then clicked = true end

        hovered = imgui.IsItemHovered()
        if hovered then animationColor = imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1) end

        imgui.PopStyleColor(4)

        local buttonMin, buttonMax, totalHeight = imgui.GetItemRectMin(), imgui.GetItemRectMax(), 0
        for line in text:gmatch("[^\r\n]+") do
            totalHeight = totalHeight + imgui.CalcTextSize(line).y
        end

        local cursorY = buttonMin.y + (size.y - totalHeight) / 2
        imgui.PushStyleColor(imgui.Col.Text, imgui.ImVec4(0, 0, 0, 1))
        for line in text:gmatch("[^\r\n]+") do
            local lineSize = imgui.CalcTextSize(line)
            imgui.SetCursorScreenPos(imgui.ImVec2(buttonMin.x + (buttonMax.x - buttonMin.x - lineSize.x) / 2, cursorY))
            imgui.Text(line)
            cursorY = cursorY + lineSize.y
        end
        imgui.PopStyleColor(1)
    else
        local textSize = imgui.CalcTextSize(text)
        size = size or imgui.ImVec2(0, 0)
        if size.x == 0 then size.x = textSize.x + 20 end
        if size.y == 0 then size.y = textSize.y + 15 end

        local pos = imgui.GetCursorPos()
        if bannerType == 3 then
            local windowWidth, buttonWidth = imgui.GetWindowSize().x, (size.x == -1) and imgui.GetWindowSize().x or size.x
            imgui.SetCursorPosX((windowWidth - buttonWidth) / 2)
        end

        imgui.InvisibleButton("##banner"..text, size)
        hovered = imgui.IsItemHovered()
        if hovered then animationColor = imgui.ImVec4(1 - animationColor.x, 1 - animationColor.y, 1 - animationColor.z, 1) end

        imgui.PushStyleColor(imgui.Col.Text, animationColor)
        imgui.SetCursorPos(imgui.GetCursorPos())

        imgui.SetCursorPosY(pos.y + (size.y - textSize.y) / 2)

        if bannerType == 3 then
            for line in text:gmatch("[^\r\n]+") do
                local lineSize = imgui.CalcTextSize(line)
                imgui.SetCursorPosX((imgui.GetWindowSize().x - lineSize.x) / 2)
                imgui.Text(line)
            end
        else
            imgui.TextWrapped(text)
        end

        imgui.PopStyleColor(1)
        clicked = hovered and imgui.IsMouseClicked(0)
    end

    if hovered and tooltip then imgui.SetTooltip(tooltip) end
    return clicked
end
function imgui.Hint(text, active)

    if not active then
        active = not imgui.IsItemActive()
    end

    -- Если активен элемент или active == true, показываем подсказку
    if imgui.IsItemHovered() and active then
        imgui.SetTooltip(text)
    end
end
function imgui.SelectText(label, description)
    local btn = label
    label = label:gsub("##.+", "")
    local size, p, p2 = imgui.CalcTextSize(label), imgui.GetCursorScreenPos(), imgui.GetCursorPos()
    local result = imgui.InvisibleButton(btn, size)
    imgui.SetCursorPos(p2)

    if imgui.IsItemHovered() then
        imgui.SetMouseCursor(imgui.MouseCursor.Hand)
        if description then
            imgui.BeginTooltip()
            imgui.PushTextWrapPos(600)
            imgui.TextUnformatted(description)
            imgui.PopTextWrapPos()
            imgui.EndTooltip()
        end
        imgui.TextColored(imgui.ImVec4(1.0, 0.647, 0.0, 1.0), label)
        imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y + size.y), imgui.ImVec2(p.x + size.x, p.y + size.y), imgui.GetColorU32(imgui.Col.CheckMark))
    else
        imgui.TextColored(imgui.ImVec4(1.0, 0.647, 0.0, 1.0), label)
    end

    return result
end