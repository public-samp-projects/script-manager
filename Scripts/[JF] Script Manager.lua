script_author('JustFedot')
script_name('[JF] Script Manager')
script_version('1.5.0')

local scriptregistry_url = "https://gitlab.com/public-samp-projects/script-manager/-/raw/master/scriptregistry.lua"

require("moonloader")
require ("sampfuncs")
local requests = require('requests')
local lfs = require("lfs")
local sampev = require("samp.events")
local effil = require("effil")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("imgui")

function jfTheme()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2
 
     style.WindowPadding = ImVec2(15, 15)
     style.WindowRounding = 15.0
     style.FramePadding = ImVec2(5, 5)
     style.ItemSpacing = ImVec2(12, 8)
     style.ItemInnerSpacing = ImVec2(8, 6)
     style.IndentSpacing = 25.0
     style.ScrollbarSize = 15.0
     style.ScrollbarRounding = 15.0
     style.GrabMinSize = 15.0
     style.GrabRounding = 7.0
     style.ChildWindowRounding = 8.0
     style.FrameRounding = 6.0
   
 
       colors[clr.Text] = ImVec4(0.95, 0.96, 0.98, 1.00)
       colors[clr.TextDisabled] = ImVec4(0.36, 0.42, 0.47, 1.00)
       colors[clr.WindowBg] = ImVec4(0.11, 0.15, 0.17, 1.00)
       colors[clr.ChildWindowBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.PopupBg] = ImVec4(0.08, 0.08, 0.08, 0.94)
       colors[clr.Border] = ImVec4(0.43, 0.43, 0.50, 0.50)
       colors[clr.BorderShadow] = ImVec4(0.00, 0.00, 0.00, 0.00)
       colors[clr.FrameBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.FrameBgHovered] = ImVec4(0.12, 0.20, 0.28, 1.00)
       colors[clr.FrameBgActive] = ImVec4(0.09, 0.12, 0.14, 1.00)
       colors[clr.TitleBg] = ImVec4(0.09, 0.12, 0.14, 0.65)
       colors[clr.TitleBgCollapsed] = ImVec4(0.00, 0.00, 0.00, 0.51)
       colors[clr.TitleBgActive] = ImVec4(0.08, 0.10, 0.12, 1.00)
       colors[clr.MenuBarBg] = ImVec4(0.15, 0.18, 0.22, 1.00)
       colors[clr.ScrollbarBg] = ImVec4(0.02, 0.02, 0.02, 0.39)
       colors[clr.ScrollbarGrab] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
       colors[clr.ScrollbarGrabActive] = ImVec4(0.09, 0.21, 0.31, 1.00)
       colors[clr.ComboBg] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.CheckMark] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrab] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.SliderGrabActive] = ImVec4(0.37, 0.61, 1.00, 1.00)
       colors[clr.Button] = ImVec4(0.20, 0.25, 0.29, 1.00)
       colors[clr.ButtonHovered] = ImVec4(0.28, 0.56, 1.00, 1.00)
       colors[clr.ButtonActive] = ImVec4(0.06, 0.53, 0.98, 1.00)
       colors[clr.Header] = ImVec4(0.20, 0.25, 0.29, 0.55)
       colors[clr.HeaderHovered] = ImVec4(0.26, 0.59, 0.98, 0.80)
       colors[clr.HeaderActive] = ImVec4(0.26, 0.59, 0.98, 1.00)
       colors[clr.ResizeGrip] = ImVec4(0.26, 0.59, 0.98, 0.25)
       colors[clr.ResizeGripHovered] = ImVec4(0.26, 0.59, 0.98, 0.67)
       colors[clr.ResizeGripActive] = ImVec4(0.06, 0.05, 0.07, 1.00)
       colors[clr.CloseButton] = ImVec4(0.40, 0.39, 0.38, 0.16)
       colors[clr.CloseButtonHovered] = ImVec4(0.40, 0.39, 0.38, 0.39)
       colors[clr.CloseButtonActive] = ImVec4(0.40, 0.39, 0.38, 1.00)
       colors[clr.PlotLines] = ImVec4(0.61, 0.61, 0.61, 1.00)
       colors[clr.PlotLinesHovered] = ImVec4(1.00, 0.43, 0.35, 1.00)
       colors[clr.PlotHistogram] = ImVec4(0.90, 0.70, 0.00, 1.00)
       colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
       colors[clr.TextSelectedBg] = ImVec4(0.25, 1.00, 0.00, 0.43)
end
function violetTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    colors[clr.Text]                 = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
    colors[clr.WindowBg]             = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.ChildWindowBg]        = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.PopupBg]              = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.Border]               = ImVec4(0.71, 0.71, 0.71, 0.40)
    colors[clr.BorderShadow]         = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.FrameBg]              = ImVec4(0.34, 0.30, 0.34, 0.30)
    colors[clr.FrameBgHovered]       = ImVec4(0.22, 0.21, 0.21, 0.40)
    colors[clr.FrameBgActive]        = ImVec4(0.20, 0.20, 0.20, 0.44)
    colors[clr.TitleBg]              = ImVec4(0.52, 0.27, 0.77, 0.82)
    colors[clr.TitleBgActive]        = ImVec4(0.55, 0.28, 0.75, 0.87)
    colors[clr.TitleBgCollapsed]     = ImVec4(9.99, 9.99, 9.90, 0.20)
    colors[clr.MenuBarBg]            = ImVec4(0.27, 0.27, 0.29, 0.80)
    colors[clr.ScrollbarBg]          = ImVec4(0.08, 0.08, 0.08, 0.60)
    colors[clr.ScrollbarGrab]        = ImVec4(0.54, 0.20, 0.66, 0.30)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.21, 0.21, 0.21, 0.40)
    colors[clr.ScrollbarGrabActive]  = ImVec4(0.80, 0.50, 0.50, 0.40)
    colors[clr.ComboBg]              = ImVec4(0.20, 0.20, 0.20, 0.99)
    colors[clr.CheckMark]            = ImVec4(0.89, 0.89, 0.89, 0.50)
    colors[clr.SliderGrab]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.SliderGrabActive]     = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Button]               = ImVec4(0.48, 0.25, 0.60, 0.60)
    colors[clr.ButtonHovered]        = ImVec4(0.67, 0.40, 0.40, 1.00)
    colors[clr.ButtonActive]         = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Header]               = ImVec4(0.56, 0.27, 0.73, 0.44)
    colors[clr.HeaderHovered]        = ImVec4(0.78, 0.44, 0.89, 0.80)
    colors[clr.HeaderActive]         = ImVec4(0.81, 0.52, 0.87, 0.80)
    colors[clr.Separator]            = ImVec4(0.42, 0.42, 0.42, 1.00)
    colors[clr.SeparatorHovered]     = ImVec4(0.57, 0.24, 0.73, 1.00)
    colors[clr.SeparatorActive]      = ImVec4(0.69, 0.69, 0.89, 1.00)
    colors[clr.ResizeGrip]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.ResizeGripHovered]    = ImVec4(1.00, 1.00, 1.00, 0.60)
    colors[clr.ResizeGripActive]     = ImVec4(1.00, 1.00, 1.00, 0.89)
    colors[clr.CloseButton]          = ImVec4(0.33, 0.14, 0.46, 0.50)
    colors[clr.CloseButtonHovered]   = ImVec4(0.69, 0.69, 0.89, 0.60)
    colors[clr.CloseButtonActive]    = ImVec4(0.69, 0.69, 0.69, 1.00)
    colors[clr.PlotLines]            = ImVec4(1.00, 0.99, 0.99, 1.00)
    colors[clr.PlotLinesHovered]     = ImVec4(0.49, 0.00, 0.89, 1.00)
    colors[clr.PlotHistogram]        = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.TextSelectedBg]       = ImVec4(0.54, 0.00, 1.00, 0.34)
    colors[clr.ModalWindowDarkening] = ImVec4(0.20, 0.20, 0.20, 0.34)
end
function defaultTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    imgui.SwitchContext()

    style.WindowRounding = 9
    style.FrameRounding = 0
    style.ItemSpacing = imgui.ImVec2(8,4)
    style.ButtonTextAlign = imgui.ImVec2(0.5,0.5)
    style.IndentSpacing = 21
    style.WindowTitleAlign = imgui.ImVec2(0,0.5)
    style.ColumnsMinSpacing = 6
    style.WindowMinSize = imgui.ImVec2(32,32)
    style.ScrollbarSize = 16
    style.DisplaySafeAreaPadding = imgui.ImVec2(4,4)
    style.FramePadding = imgui.ImVec2(4,3)
    style.GrabRounding = 0
    style.ChildWindowRounding = 0
    style.TouchExtraPadding = imgui.ImVec2(0,0)
    style.ScrollbarRounding = 9
    style.WindowPadding = imgui.ImVec2(8,8)
    style.GrabMinSize = 10
    style.DisplayWindowPadding = imgui.ImVec2(22,22)
    style.ItemInnerSpacing = imgui.ImVec2(4,4)

    colors[imgui.Col.ScrollbarGrab] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.30000001192093)
    colors[imgui.Col.FrameBgActive] = imgui.ImVec4(0.89999997615814,0.64999997615814,0.64999997615814,0.44999998807907)
    colors[imgui.Col.ButtonHovered] = imgui.ImVec4(0.6700000166893,0.40000000596046,0.40000000596046,1)
    colors[imgui.Col.PlotHistogram] = imgui.ImVec4(0.89999997615814,0.69999998807907,0,1)
    colors[imgui.Col.ButtonActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,1)
    colors[imgui.Col.ResizeGripActive] = imgui.ImVec4(1,1,1,0.89999997615814)
    colors[imgui.Col.FrameBg] = imgui.ImVec4(0.80000001192093,0.80000001192093,0.80000001192093,0.30000001192093)
    colors[imgui.Col.TextDisabled] = imgui.ImVec4(0.60000002384186,0.60000002384186,0.60000002384186,1)
    colors[imgui.Col.ResizeGripHovered] = imgui.ImVec4(1,1,1,0.60000002384186)
    colors[imgui.Col.PlotHistogramHovered] = imgui.ImVec4(1,0.60000002384186,0,1)
    colors[imgui.Col.PlotLines] = imgui.ImVec4(1,1,1,1)
    colors[imgui.Col.SliderGrab] = imgui.ImVec4(1,1,1,0.30000001192093)
    colors[imgui.Col.CloseButton] = imgui.ImVec4(0.5,0.5,0.89999997615814,0.5)
    colors[imgui.Col.TextSelectedBg] = imgui.ImVec4(0,0,1,0.34999999403954)
    colors[imgui.Col.ModalWindowDarkening] = imgui.ImVec4(0.20000000298023,0.20000000298023,0.20000000298023,0.34999999403954)
    colors[imgui.Col.TitleBg] = imgui.ImVec4(0.27000001072884,0.27000001072884,0.54000002145767,0.8299999833107)
    colors[imgui.Col.SeparatorHovered] = imgui.ImVec4(0.60000002384186,0.60000002384186,0.69999998807907,1)
    colors[imgui.Col.ComboBg] = imgui.ImVec4(0.20000000298023,0.20000000298023,0.20000000298023,0.99000000953674)
    colors[imgui.Col.ResizeGrip] = imgui.ImVec4(1,1,1,0.30000001192093)
    colors[imgui.Col.SeparatorActive] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.89999997615814,1)
    colors[imgui.Col.Border] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.69999998807907,0.40000000596046)
    colors[imgui.Col.HeaderHovered] = imgui.ImVec4(0.44999998807907,0.44999998807907,0.89999997615814,0.80000001192093)
    colors[imgui.Col.Separator] = imgui.ImVec4(0.5,0.5,0.5,1)
    colors[imgui.Col.FrameBgHovered] = imgui.ImVec4(0.89999997615814,0.80000001192093,0.80000001192093,0.40000000596046)
    colors[imgui.Col.ScrollbarBg] = imgui.ImVec4(0.20000000298023,0.25,0.30000001192093,0.60000002384186)
    colors[imgui.Col.PlotLinesHovered] = imgui.ImVec4(0.89999997615814,0.69999998807907,0,1)
    colors[imgui.Col.TitleBgCollapsed] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.20000000298023)
    colors[imgui.Col.ChildWindowBg] = imgui.ImVec4(0,0,0,0)
    colors[imgui.Col.CheckMark] = imgui.ImVec4(0.89999997615814,0.89999997615814,0.89999997615814,0.5)
    colors[imgui.Col.ScrollbarGrabHovered] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.80000001192093,0.40000000596046)
    colors[imgui.Col.TitleBgActive] = imgui.ImVec4(0.31999999284744,0.31999999284744,0.62999999523163,0.87000000476837)
    colors[imgui.Col.HeaderActive] = imgui.ImVec4(0.52999997138977,0.52999997138977,0.87000000476837,0.80000001192093)
    colors[imgui.Col.CloseButtonActive] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.69999998807907,1)
    colors[imgui.Col.MenuBarBg] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.55000001192093,0.80000001192093)
    colors[imgui.Col.WindowBg] = imgui.ImVec4(0,0,0,0.69999998807907)
    colors[imgui.Col.SliderGrabActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,1)
    colors[imgui.Col.PopupBg] = imgui.ImVec4(0.050000000745058,0.050000000745058,0.10000000149012,0.89999997615814)
    colors[imgui.Col.ScrollbarGrabActive] = imgui.ImVec4(0.80000001192093,0.5,0.5,0.40000000596046)
    colors[imgui.Col.Button] = imgui.ImVec4(0.6700000166893,0.40000000596046,0.40000000596046,0.60000002384186)
    colors[imgui.Col.Header] = imgui.ImVec4(0.40000000596046,0.40000000596046,0.89999997615814,0.44999998807907)
    colors[imgui.Col.BorderShadow] = imgui.ImVec4(0,0,0,0)
    colors[imgui.Col.CloseButtonHovered] = imgui.ImVec4(0.69999998807907,0.69999998807907,0.89999997615814,0.60000002384186)
    colors[imgui.Col.Text] = imgui.ImVec4(0.89999997615814,0.89999997615814,0.89999997615814,1)
end

do local a=getmetatable("String")function a.__index:insert(b,c)if c==nil then return self..b end;return self:sub(1,c)..b..self:sub(c+1)end;function a.__index:extract(d)self=self:gsub(d,"")return self end;function a.__index:array()local e={}for f in self:gmatch(".")do e[#e+1]=f end;return e end;function a.__index:isEmpty()return self:find("%S")==nil end;function a.__index:isDigit()return self:find("%D")==nil end;function a.__index:isAlpha()return self:find("[%d%p]")==nil end;function a.__index:split(g,h)local i,c={},1;repeat local f,j=self:find(g or" ",c,h)local k=self:sub(c,f and f-1)if k~=""then i[#i+1]=k end;c=j and j+1 until c==nil;return i end;local l=string.lower;function a.__index:lower()for m=192,223 do self=self:gsub(string.char(m),string.char(m+32))end;self=self:gsub(string.char(168),string.char(184))return l(self)end;local n=string.upper;function a.__index:upper()for m=224,255 do self=self:gsub(string.char(m),string.char(m-32))end;self=self:gsub(string.char(184),string.char(168))return n(self)end;function a.__index:isSpace()return self:find("^[%s%c]*$")~=nil end;function a.__index:isUpper()return self:upper()==self end;function a.__index:isLower()return self:lower()==self end;function a.__index:isSimilar(o)return self==o end;function a.__index:isTitle()local p=self:find("[A-zА-яЁё]")local q=self:sub(p,p)return q:isSimilar(q:upper())end;function a.__index:startsWith(o)return self:sub(1,#o):isSimilar(o)end;function a.__index:endsWith(o)return self:sub(#self-#o+1,#self):isSimilar(o)end;function a.__index:capitalize()local r=self:sub(1,1):upper()self=self:gsub("^.",r)return self end;function a.__index:tabsToSpace(s)local t=(" "):rep(s or 4)self=self:gsub("\t",t)return self end;function a.__index:spaceToTabs(s)local t=(" "):rep(s or 4)self=self:gsub(t,"\t")return self end;function a.__index:center(u,v)local w=u-#self;local f=string.rep(v or" ",w)return f:insert(self,math.ceil(w/2))end;function a.__index:count(x,y,z)local A=self:sub(y or 1,z or#self)local s,c=0,y or 1;repeat local f,j=A:find(x,c,true)s=f and s+1 or s;c=j and j+1 until c==nil;return s end;function a.__index:trimEnd()self=self:gsub("%s*$","")return self end;function a.__index:trimStart()self=self:gsub("^%s*","")return self end;function a.__index:trim()self=self:match("^%s*(.-)%s*$")return self end;function a.__index:swapCase()local i={}for f in self:gmatch(".")do if f:isAlpha()then f=f:isLower()and f:upper()or f:lower()end;i[#i+1]=f end;return table.concat(i)end;function a.__index:splitEqually(u)assert(u>0,"Width less than zero")if u>=self:len()then return{self}end;local i,m={},1;repeat if#i==0 or#i[#i]>=u then i[#i+1]=""end;i[#i]=i[#i]..self:sub(m,m)m=m+1 until m>#self;return i end;function a.__index:rFind(d,c,h)local m=c or#self;repeat local i={self:find(d,m,h)}if next(i)~=nil then return table.unpack(i)end;m=m-1 until m<=0;return nil end;function a.__index:wrap(u)assert(u>0,"Width less than zero")assert(u<self:len(),"Width is greater than the string length")local c=1;self=self:gsub("(%s+)()(%S+)()",function(B,C,D,E)if E-c>(u or 72)then c=C;return"\n"..D end end)return self end;function a.__index:levDist(o)if#self==0 then return#o elseif#o==0 then return#self elseif self==o then return 0 end;local F=0;local G={}for m=0,#self do G[m]={}G[m][0]=m end;for m=0,#o do G[0][m]=m end;for m=1,#self,1 do for H=1,#o,1 do F=self:byte(m)==o:byte(H)and 0 or 1;G[m][H]=math.min(G[m-1][H]+1,G[m][H-1]+1,G[m-1][H-1]+F)end end;return G[#self][#o]end;function a.__index:getSimilarity(o)local I=self:levDist(o)return 1-I/math.max(#self,#o)end;function a.__index:empty()return""end;function a.__index:toCamel()local J=self:array()for m,q in ipairs(J)do J[m]=m%2==0 and q:lower()or q:upper()end;return table.concat(J)end;function a.__index:unplain()local J=self:array()for m,q in ipairs(J)do if q:find("().%+-*?[]^$",1,true)then J[m]="%"..q end end;return table.concat(J)end;function a.__index:shuffle(K)math.randomseed(K or os.time())local J=self:array()for m=#J,2,-1 do local H=math.random(m)J[m],J[H]=J[H],J[m]end;return table.concat(J)end;function a.__index:cutLimit(L,M)assert(L>0,"Maximum length cannot be less than or equal to 1")if#self>0 and#self>L then M=M or".."self=self:sub(1,L)..M end;return self end;function a.__index:switchLayout()local i=""local N=self:find("^[%s%p]*%a")~=nil;local k={{"а","f"},{"б",","},{"в","d"},{"г","u"},{"д","l"},{"е","t"},{"ё","`"},{"ж",";"},{"з","p"},{"и","b"},{"й","q"},{"к","r"},{"л","k"},{"м","v"},{"н","y"},{"о","j"},{"п","g"},{"р","h"},{"с","c"},{"т","n"},{"у","e"},{"ф","a"},{"х","["},{"ц","w"},{"ч","x"},{"ш","i"},{"щ","o"},{"ь","m"},{"ы","s"},{"ъ","]"},{"э","'"},{"/","."},{"я","z"},{"А","F"},{"Б","<"},{"В","D"},{"Г","U"},{"Д","L"},{"Е","T"},{"Ё","~"},{"Ж",":"},{"З","P"},{"И","B"},{"Й","Q"},{"К","R"},{"Л","K"},{"М","V"},{"Н","Y"},{"О","J"},{"П","G"},{"Р","H"},{"С","C"},{"Т","N"},{"У","E"},{"Ф","A"},{"Х","{"},{"Ц","W"},{"Ч","X"},{"Ш","I"},{"Щ","O"},{"Ь","M"},{"Ы","S"},{"Ъ","}"},{"Э","\""},{"Ю",">"},{"Я","Z"}}for O in self:gmatch(".")do local P=false;for Q,R in ipairs(k)do if O==R[N and 2 or 1]then O=R[N and 1 or 2]P=true;break end end;if not P then for Q,R in ipairs(k)do if O==R[N and 1 or 2]then O=R[N and 2 or 1]break end end end;i=i..O end;return i end end
do -- Xcfg Modified
    Xcfg = {
        _version    = 2.1,
        _author     = "Double Tap Inside",
        _modified   = "JustFedot",
        _email      = "double.tap.inside@gmail.com",
        _help = [[
            Module xcfg             = Xcfg()
            Создает и возвращает новый экземпляр модуля Xcfg.
    
            nil                     = xcfg.mkpath(Str filename)
            Создает необходимые директории для указанного пути файла.
    
            Table loaded / nil      = xcfg.load(Str filename, [Bool save = false])
            Загружает конфигурационный файл. Если 'save' установлено в true, автоматически сохраняет файл после загрузки.
    
            Bool result             = xcfg.save(Str filename, Table new)
            Сохраняет данные в конфигурационный файл.
    
            Bool result             = xcfg.insert(Str filename, (Value value or Int index), [Value value])
            Вставляет значение в конфигурационный файл. Если указан индекс, вставляет по индексу.
    
            Bool result             = xcfg.remove(Str filename, [Int index])
            Удаляет значение из конфигурационного файла. Если указан индекс, удаляет значение по индексу.
    
            Bool result             = xcfg.set(Str filename, (Int index or Str key), Value value)
            Устанавливает или обновляет значение в конфигурационном файле по ключу или индексу.
    
            Bool result             = xcfg.update(Table old, (Table new or StrFilename new), [Bool overwrite = true])
            Обновляет старую таблицу новыми значениями из другой таблицы или файла. 'overwrite' определяет, перезаписывать ли существующие значения.
    
            Bool result             = xcfg.write(Str filename, Str str)
            Пишет строку в файл, перезаписывая его содержимое.
    
            Bool result             = xcfg.append(Str filename, Str str)
            Добавляет строку в конец файла.
    
            Table                   = xcfg.setupImcfg(Table cfg)
            Создает и возвращает таблицу с элементами управления imgui на основе конфигурационной таблицы 'cfg'. Поддерживает различные типы данных, включая вложенные таблицы.
        ]]
    }
	function Xcfg.__init()
		local self = {}
		
		-- draw values
		local function draw_string(str)
			return string.format("%q", str)
		end
		
		local function is_var(key_or_index)
			if type(key_or_index) == "string" and key_or_index:match("^[_%a][_%a%d]*$") then
				return true
			
			else
				return false
			end
		end
		
		local function draw_table_key(key)
			if is_var(key) then
				return key
				
			else
				return "["..draw_key(key).."]"
			end
		end
		
		local function draw_table(tbl, tab)
			local tab = tab or ""
			local result = {}
			
			for key, value in pairs(tbl) do
				if type(value) == "string" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_string(value))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_string(value))
					end
					
				elseif type(value) == "number" or type(value) == "boolean" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, tostring(value))
						
					else
						table.insert(result, draw_table_key(key).." = "..tostring(value))
					end
				
				elseif type(value) == "table" then
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_table(value, tab.."\t"))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_table(value, tab.."\t"))
					end
					
				else
					if type(key) == "number" and key <= #tbl then
						table.insert(result, draw_string(tostring(value)))
						
					else
						table.insert(result, draw_table_key(key).." = "..draw_string(tostring(value)))
					end
				end
			end
			
			if #result == 0 and tab == "" then
				return ""
				
			elseif #result == 0 then
				return "{}"
			
			elseif tab == "" then
				return table.concat(result, ",\n")..",\n"
			
			else
				return "{\n"..tab..table.concat(result, ",\n"..tab)..",\n"..tab:sub(2).."}"
			end       
		end
		
		local function draw_value(value, tab)
			if type(value) == "string" then
				return draw_string(value)
			
			elseif type(value) == "number" or type(value) == "boolean" or type(value) == "nil" then
				return tostring(value)
			
			elseif type(value) == "table" then
				return draw_table(value, tab)
				
			else
				return draw_string(tostring(value))
			end
		end
		
		local function draw_key(key)
			if "string" == type(key) then
				return draw_string(key)
			
			elseif "number" == type(key) then
				return tostring(key)
			end
		end
		
		
		local function draw_config(tbl)
			local result = {}
		
			for key, value in pairs(tbl) do
				
				if type(key) == "number" then
					table.insert(result, "table.insert(tbl, "..draw_value(value, "\t")..")")
				
				elseif type(key) == "string" then			
					if is_var(key) then
						table.insert(result, "tbl."..draw_table_key(key).." = "..draw_value(value, "\t"))
					
					else
						table.insert(result, "tbl"..draw_table_key(key).." = "..draw_value(value, "\t"))
					end
				end
			end
			
			if #result == 0 then
				return ""
				
			else
				return table.concat(result, "\n").."\n"
			end
		end

		function self.load(filename, overwrite)
			assert(type(filename)=="string", ("bad argument #1 to 'load' (string expected, got %s)"):format(type(filename)))
			
			if overwrite == nil then
				overwrite = false
			end
			
			local file = io.open(filename, "r")
			
			if file then
				local text = file:read("*all")
				file:close()
				local lua_code = loadstring("local tbl = {}\n"..text.."\nreturn tbl")
				
				if lua_code then
					local result = lua_code()
					
					if type(result) == "table" then
						if overwrite then
							self.save(filename, result)
						end
						
						return result
					end
				end
			end
		end
		
		function self.save(filename, new)
			assert(type(filename)=="string", ("bad argument #1 to 'table_save' (string expected, got %s)"):format(type(filename)))
			assert(type(new)=="table", ("bad argument #2 to 'table_save' (table expected, got %s)"):format(type(new)))
		
			self.mkpath(filename)
			local file = io.open(filename, "w+")
			
			if file then
				local text = draw_config(new)
				file:write(text)
				file:close()
				
				return true
			else
				return false
			end
		end
		
		function self.insert(filename, value_or_index, value)
			assert(type(filename)=="string", ("bad argument #1 to 'insert' (string expected, got %s)"):format(type(filename)))
			
			if value then
				assert(type(value_or_index)=="number", ("bad argument #2 to 'insert' (number expected, got %s)"):format(type(value_or_index)))
			end
			
			local result
			
			if value then
				result = "table.insert(tbl, "..value_or_index..", "..draw_value(value, "\t")..")"
				
			else
				result = "table.insert(tbl, "..draw_value(value_or_index, "\t")..")"
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.remove(filename, index)
			assert(type(filename)=="string", ("bad argument #1 to 'remove' (string expected, got %s)"):format(type(filename)))
			assert(type(index)=="number" or index == nil, ("bad argument #2 to 'remove' (number or nil expected, got %s)"):format(type(index)))
			local result
			
			if index then
				result = "table.remove(tbl, "..index..")"
				
			else
				result = "table.remove(tbl)"
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.set(filename, key, value)
			assert(type(filename)=="string", ("bad argument #1 to 'set' (string expected, got %s)"):format(type(filename)))
			assert(type(key)=="number" or type(key)=="string", ("bad argument #2 to 'set' (number or string expected, got %s)"):format(type(key)))
			
			local result
					
			if is_var() then
				result = "tbl."..tostring(var).." = "..draw_value(value, "\t")
			
			else
				result = "tbl["..draw_key(key).."] = "..draw_value(value, "\t")
			end
			
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(result.."\n")
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.mkpath(filename)
			assert(type(filename)=="string", ("bad argument #1 to 'mkpath' (string expected, got %s)"):format(type(filename)))
		
			local sep, pStr = package.config:sub(1, 1), ""
			local path = filename:match("(.+"..sep..").+$") or filename
			
			for dir in path:gmatch("[^" .. sep .. "]+") do
				pStr = pStr .. dir .. sep
				createDirectory(pStr)
			end
		end

		function self.update(old, new, overwrite)
			assert(type(old)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(old)))
			assert(type(new)=="string" or type(new)=="table", ("bad argument #2 to 'update' (string or table expected, got %s)"):format(type(new)))
			
			if overwrite == nil then
				overwrite = true
			end
		
			if type(new) == "table" then
				if overwrite then
					for key, value in pairs(new) do
						old[key] = value
					end
					
				else
					for key, value in pairs(new) do
						if old[key] == nil then
							old[key] = value
						end
					end
				end
				
				return true
				
			elseif type(new) == "string" then
				local loaded = self.load(new)
				
				if loaded then
					if overwrite then
						for key, value in pairs(loaded) do
							old[key] = value
						end
						
					else
						for key, value in pairs(loaded) do
							if old[key] == nil then
								old[key] = value
							end
						end
					end
					
					return true
				end
			end
			
			return false
		end
		
		function self.append(filename, str)
			self.mkpath(filename)
			local file = io.open(filename, "a+")
			
			if file then
				file:write(str)
				file:close()
				return true
				
			else
				return false
			end
		end
		
		function self.write(filename, str)
			self.mkpath(filename)
			local file = io.open(filename, "w+")
			
			if file then
				file:write(str)
				file:close()
				return true
				
			else
				return false
			end
		end

        function self.setupImcfg(cfg)
            assert(type(cfg) == "table", ("bad argument #1 to 'setupImcfg' (table expected, got %s)"):format(type(cfg)))
            local function setupImcfgRecursive(cfg)
                local imcfg = {}
                for k, v in pairs(cfg) do
                    if type(v) == "table" then
                        imcfg[k] = setupImcfgRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        assert(false, ("Unsupported type for imcfg: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImcfgRecursive(cfg)
        end
		
		return self
	end
	setmetatable(Xcfg, {
	__call = function(self)
		return self.__init()
	end
})
end
local xcfg = Xcfg()

local filename = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.cfg'

local imgui_windows = {
    main = imgui.ImBool(false),
}

local cfg = {
    lastUpdateTime = "Никогда.",
    savedList = {},
    installedScripts = {},
    isNeedOpenWindowWhenUpdatesAvaible = true,
    isNeedOpenWindowWhenNewScriptsAvaibleForDownload = true,
    imguiStyle = 0,
}
xcfg.update(cfg,filename)

function saveConfig()
    xcfg.save(filename,cfg)
end

local imcfg = xcfg.setupImcfg(cfg)

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local doubleTapDebugTable = {}
local advertisements = {}

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    addChat('Загружен. Команда: {ffc0cb}/jfm{ffffff}.')
    takeRepositoryInfo()
    sampRegisterChatCommand('jfm',function()
        imgui_windows.main.v = not imgui_windows.main.v
    end)
    while true do wait(0)
        imgui.Process = imgui_windows.main.v
    end
end

function imguiStyler(num)
    local tbl = {
        [0] = jfTheme,
        [1] = violetTheme,
        [2] = defaultTheme,
    }
    if num ~= 2 then defaultTheme() end
    tbl[num]()
end
imguiStyler(cfg.imguiStyle)

function takeRepositoryInfo()
    local registry = getScriptRegistry()
    if registry then
        if #cfg.savedList < #registry then
            addChat('{ffff00}[Внимание]{ffffff} Доступны новые скрипты для скачивания. Пропишите: {ffc0cb}/jfm{ffffff}.')
            if cfg.isNeedOpenWindowWhenNewScriptsAvaibleForDownload then
                imgui_windows.main.v = true
            end
        end
        cfg.savedList = registry
        doubleTapDebugTable = simplifyTable(getListOfInstalledScripts())
        for k, v in pairs(cfg.savedList) do
            v.description = trimMultilineString(v.description)
            if doubleTapDebugTable[v.name] and doubleTapDebugTable[v.name] ~= v.version and not v.isDeleted then
                addChat('Доступно обновление {ffff00}"'..v.name..'"{ffffff}. Пропишите: {ffc0cb}/jfm{ffffff}.')
                if cfg.isNeedOpenWindowWhenUpdatesAvaible then imgui_windows.main.v = true end
            end
        end
        table.sort(cfg.savedList, function(a, b)
            local a_exists = doubleTapDebugTable[a.name] ~= nil
            local b_exists = doubleTapDebugTable[b.name] ~= nil
        
            if a_exists and b_exists then
                local a_update_available = a.version ~= doubleTapDebugTable[a.name]
                local b_update_available = b.version ~= doubleTapDebugTable[b.name]
        
                if a_update_available and not b_update_available then
                    return true
                elseif not a_update_available and b_update_available then
                    return false
                else
                    return dateToNumber(a.realeseDate) > dateToNumber(b.realeseDate)
                end
            elseif a_exists and not b_exists then
                return true
            elseif not a_exists and b_exists then
                return false
            else
                return dateToNumber(a.realeseDate) > dateToNumber(b.realeseDate)
            end
        end)
        advertisements = getAdvertisement()
        for k, v in pairs(advertisements) do
            v.description = trimMultilineString(v.description)
        end
        cfg.lastUpdateTime = os.date("%H:%M %d.%m.%Y")
        saveConfig()
        addChat('{99ff99}[Успех] {ffffff}Данные с репозитория получены.')
    else
        addChat('{FF0000}[Ошибка] {ffffff}Не удалось получить информацию из репозитория.')
    end
end

function getAdvertisement()
    local url = "https://gitlab.com/public-samp-projects/script-manager/-/raw/master/advertisement.lua"
    local response = requests.get(url)
    if response.status_code == 200 then
        local text = u8:decode(response.text)
        local codeTable = loadstring("return " .. text)
        return codeTable()
    else
        return {}
    end
end

function updateListOfInstalledScripts()
    doubleTapDebugTable = simplifyTable(getListOfInstalledScripts())
end

function simplifyTable(tbl)
    local path = getWorkingDirectory()..'\\'
    local res = {}
    for k,v in ipairs(tbl) do
        local file, err = io.open(path..v, 'r')
        if file then
            for line in file:lines() do
                local version = line:match("script_version%('(.+)'%)")
                if version then
                    res[v] = version
                    break
                end
            end
            file:close()
        else
            addChat('{ff0000}[Ошибка] {ffffff}Произошла ошибка при чтении версии файла.')
        end
    end
    return res
end

function enterpolyseFileName(str, reverse)
    if reverse then
        -- Декодирование
        return (str:gsub("%%(%x%x)", function(hex)
            return string.char(tonumber(hex, 16))
        end))
    else
        -- Кодирование
        return (str:gsub(".", function(char)
            return string.format("%%%02X", string.byte(char))
        end))
    end
end

function getListOfInstalledScripts()
    local path = getWorkingDirectory()
    local result = {}
    for filename in lfs.dir(path) do
        if filename:find('^%[JF%].+%.lua$') then
            table.insert(result,filename)
        end
    end
    return result
end


function getScriptCodeFromUrl(url)
    local response = requests.get(url)
    if response.status_code == 200 then
        return u8:decode(response.text)
    else
        return nil, 'Ошибка запроса: ' .. response.status_code
    end
end

function saveCodeAsFile(text, filename)
    local path = getWorkingDirectory()..'\\'..filename
    if not doesFileExist(path) then
        local file = io.open(path,'w')
        if file then
            file:write(text)
            file:close()
            return true
        else
            return false
        end
    end
end

function deleteScriptFile(filename, needChatPopup)
    local path = getWorkingDirectory()..'\\'..filename
    if doesFileExist(path) then
        local success, err = os.remove(path)
        if success then
            if needChatPopup then addChat('{99ff99}[Успех] {ffffff}Скрипт {ffff00}"'..filename..'" {ffffff}успешно удален.') end
            return true
        else
            addChat("{ff0000}[Ошибка] {ffffff}Ошибка при удалении файла выведена в консоль.")
            print(err)
            return false
        end
    else
        addChat('{ff0000}[Ошибка] {ffffff}Файла не существует.')
        return false
    end
end

function getScriptRegistry()
    local response = requests.get(scriptregistry_url)
    if response.status_code == 200 then
        local text = u8:decode(response.text)
        local codeTable = loadstring("return " .. text)
        return codeTable()
    else
        return nil
    end
end

function createScriptFile(filename, chatPoputText)
    local url = "https://gitlab.com/public-samp-projects/script-manager/-/raw/master/Scripts/"..enterpolyseFileName(filename, false)
    local code = getScriptCodeFromUrl(url)
    if code then
        if saveCodeAsFile(code, filename) then
            addChat(chatPoputText and chatPoputText or '{99ff99}[Успех] {ffffff}Скрипт {ffff00}"'..filename..'"{ffffff} установлен.')
            addChat('Перезагрузите скрипты.')
        else
            addChat('{ff0000}[Ошибка] {ffffff}Произошла ошибка при попытке сохранения файла.')
        end
    else
        addChat('{ff0000}[Ошибка] {ffffff}Произошла ошибка при попытке загрузки кода.')
    end
end

function deleteSettingsData(name)
    local path = getWorkingDirectory()..'\\config\\'..name:gsub('%.lua', '')
    if doesDirectoryExist(path) then
        if removeDirectory(path) then
            addChat("{99ff99}[Успех] {ffffff}Настройки были удалены успешно.")
        else
            addChat('{ff0000}[Ошибка] {ffffff}Ошибка удаления настроек. Удалите вручную.')
        end
    else
        addChat('{ffa500}[Внимание] {ffffff}Настройки не найдены.')
    end
end

function removeDirectory(path)
    local success = true
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local filePath = path .. '/' .. file
            local mode = lfs.attributes(filePath, "mode")
            if mode == "directory" then
                if not removeDirectory(filePath) then
                    success = false
                end
            elseif mode == "file" then
                if not os.remove(filePath) then
                    success = false
                end
            end
        end
    end
    if not lfs.rmdir(path) then
        success = false
    end
    return success
end


local w,h = getScreenResolution()
local window_width,window_height = 1200,600
local imTabs = 1
local isUpdateAvaible = false

local scriptSeachBuffer = imgui.ImBuffer(256)

local savedPopupData

local _confirmPopupData = {
    scriptName = 'update.lua',
    text = "Вы уверены что дрочите?",
    buttonText = "Удалить",
    buttonFunc = deleteScriptFile
}

function _openConfirmPopup(name, text, buttonText, buttonFunc)
    _confirmPopupData.scriptName = name
    _confirmPopupData.text = text
    _confirmPopupData.buttonText = buttonText
    _confirmPopupData.buttonFunc = buttonFunc
    imgui.OpenPopup(u8'Подтверждение##confirmPopup')
end

function __updateMajorScript(name)
    if deleteScriptFile(name) then
        deleteSettingsData(name)
        createScriptFile(name, '{99ff99}[Успех] {ffffff}Скрипт {ffff00}"'..name..'"{ffffff} обновлён.')
        updateListOfInstalledScripts()
        isUpdateAvaible = false
    end
end

function __deleteScript(name, isDataDeleteNeeded)
    if not isDataDeleteNeeded then deleteSettingsData(name) end
    deleteScriptFile(name, true)
    updateListOfInstalledScripts()
    isUpdateAvaible = false
end

function __installScript(name)
    createScriptFile(name)
    updateListOfInstalledScripts()
end

function imgui.OnDrawFrame()

    if imgui_windows.main.v then


        imgui.SetNextWindowSize(imgui.ImVec2(window_width,window_height), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2-window_width/2, h/2-window_height/2), imgui.Cond.FirstUseEver)
        imgui.Begin(u8(thisScript().name)..' Ver: '..thisScript().version.." (Last Upd: "..cfg.lastUpdateTime..")##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar)

        imgui.BeginChild('##buttoChild', imgui.ImVec2(200, 0), false)

            imTabs = imgui.ActivatedButtons(imTabs, 4, u8'Доступные скрипты', u8'Установленные скрипты', u8'Настройки', u8'неНавязчивая Реклама')

            if imgui.IsItemHovered() then imgui.SetTooltip(u8('Это рофло-вкладка, но если вы заинтересованы, можете купить рекламу.')) end

            imgui.SetCursorPosY(imgui.GetWindowHeight()-60)
            if imgui.Button(u8'Обновить информацию...##updateRepositoryButton', imgui.ImVec2(imgui.GetWindowWidth(),50)) then
                takeRepositoryInfo()
            end
            if imgui.IsItemHovered() then
                imgui.SetTooltip(u8('Кнопка для принудительного обновления данных из репозитория GitLab.'))
            end

        imgui.EndChild()

        imgui.SameLine()

        imgui.BeginChild('##mainChild', imgui.ImVec2(-1,-1), true)

            if isUpdateAvaible then
                imgui.BeginChild('##updateNotfChild', imgui.ImVec2(-1,120), true)
                imgui.CenterTextColoredRGB('{ff0000}Внимание!\nДоступны обновления скриптов.\nПожалуйста, перейдите во вкладку {ffff00}"Установленные скрипты" {ffffff}для обновления, либо нажмите кнопку {ffff00}"Обновить всё" {ffffff}ниже.')
                if imgui.CenterButton('Обновить всё##updateAllButton', 200) then
                    for k, v in pairs(cfg.savedList) do
                        if doubleTapDebugTable[v.name] and doubleTapDebugTable[v.name] ~= v.version and not v.isDeleted then
                            if doubleTapDebugTable[v.name]:match('(%d)%.') ~= v.version:match('(%d)%.') then
                                _openConfirmPopup(v.name, "{ffa500}Внимание! {ffffff}Скрипт получает мажорное обновление.\nФайлы настроек будут {ff0000}удалены.", "Обновить", __updateMajorScript)
                                break
                            else
                                deleteScriptFile(v.name)
                                createScriptFile(v.name, '{99ff99}[Успех] {ffffff}Скрипт {ffff00}"'..v.name..'"{ffffff} обновлён.')
                                updateListOfInstalledScripts()
                            end
                        end
                    end
                    isUpdateAvaible = false
                end
                imgui.EndChild()
            end

            -- if imgui.Button('PopupModalDebug') then
            --     _openConfirmPopup(cfg.savedList[1].name, "{ffa500}Внимание! {ffffff}Скрипт получает мажорное обновление.\nФайлы настроек будут {ff0000}удалены.", "Обновить", __updateMajorScript)
            -- end

            if imTabs==1 then

                imgui.BeginChild('##searchScriptChild', imgui.ImVec2(-1, 40), false)
                imgui.SetCursorPosX(imgui.GetWindowWidth()/2-150)
                imgui.PushItemWidth(200)
                imgui.InputText('##searchInput', scriptSeachBuffer)
                if imgui.IsItemHovered() and not imgui.IsItemActive() then
                    imgui.SetTooltip(u8('Поиск по скриптам.\nПоиск осуществляется по названию, краткому и полному описанию.'))
                end
                imgui.SameLine()
                if imgui.Button(u8'Очистить##cleanSearchBuffer', imgui.ImVec2(100,0)) then
                    scriptSeachBuffer.v = ''
                end
                imgui.Separator()
                imgui.EndChild()

                --imgui.Combo('##scriptVersionCombo', imgui.ImInt(1), {u8"Первая", "Vtoraya", "Tretya"}, 3)

                drawTabsInterface(__imTabs1)

            elseif imTabs==2 then

                drawTabsInterface(__imTabs2)

            elseif imTabs==3 then

                if imgui.Checkbox(u8'Открытие окна при появлении новых скриптов для скачивания', imcfg.isNeedOpenWindowWhenNewScriptsAvaibleForDownload) then
                    cfg.isNeedOpenWindowWhenNewScriptsAvaibleForDownload = imcfg.isNeedOpenWindowWhenNewScriptsAvaibleForDownload.v
                    saveConfig()
                end
                if imgui.IsItemHovered() then
                    imgui.SetTooltip(u8('При включении данной настройки, окно скрипта будет автоматически открываться при обнаружении новых доступных скриптов для скачивания.\nСкрипт проверяет наличие обновлений при запуске и при нажатии на кнопку проверки обновлений, уведомляя вас в чате.'))
                end                
                if imgui.Checkbox(u8'Открытие окна при доступных обновлениях', imcfg.isNeedOpenWindowWhenUpdatesAvaible) then
                    cfg.isNeedOpenWindowWhenUpdatesAvaible = imcfg.isNeedOpenWindowWhenUpdatesAvaible.v
                    saveConfig()
                end
                if imgui.IsItemHovered() then
                    imgui.SetTooltip(u8('Скрипт проверяет обновления при запуске, а так же при нажатии соответствующей кнопки, и уведомляет вас в чате.\nЭта настройка отвечает за то, будет ли окно с обновлениями открываться автоматически, если присутствуют обновления.'))
                end
                
                imgui.Separator()

                if imgui.Combo(u8'Выберите тему##selectStyleCombo', imcfg.imguiStyle, {u8"Стандартная [JF]", u8"Фиолетовая Violet", u8"Стандартная ImGui"}, 10) then
                    if imcfg.imguiStyle.v ~= cfg.imguiStyle then
                        cfg.imguiStyle = imcfg.imguiStyle.v
                        imguiStyler(cfg.imguiStyle)
                        saveConfig()
                    end
                end
                if imgui.IsItemHovered() and not imgui.IsItemActive() then
                    imgui.SetTooltip(u8('Тут вы можете поменять тему интерфейса.'))
                end

            elseif imTabs==4 then

                imgui.TextDisabled(u8'Для размещения рекламы в этом блоке, свяжитесь с автором в Telegram:')
                imgui.SameLine()
                if imgui.Link("https://t.me/justfedot", u8"Кликни") then
                    os.execute(('explorer.exe "%s"'):format("https://t.me/justfedot"))
                end
                imgui.Separator()

                if #advertisements > 0 then

                    for k,v in pairs(advertisements) do

                        imgui.TextColoredRGB(v.title)
                        imgui.TextColoredRGB(v.description)
                        imgui.NewLine()
                        imgui.Text(u8"Ссылки:")
                        imgui.SameLine()
                        for index, value in ipairs(v.links) do
                            if imgui.Link(u8(value.description), u8(value.link)) then
                                os.execute(('explorer.exe "%s"'):format(value.link))
                            end
                            if index ~= #v.links then imgui.SameLine() end
                        end
                        imgui.Separator()

                    end

                else

                    imgui.CenterTextColoredRGB('{ff0000}Упс... {ffffff}нет доступной {99ff99}рекламы{ffffff}.\nВы можете приобрести её перейдя по {99ff99}ссылке вверху{ffffff}.')

                end

            end

            _confirmPopup()
        imgui.EndChild()

        imgui.End()

    end

end

function drawTabsInterface(draw)

    local buttonPos = imgui.GetWindowWidth()-imgui.GetStyle().ItemSpacing.x-90
    local totalScriptsCount = false

    for k, v in pairs(cfg.savedList) do

        totalScriptsCount = draw(k, v, buttonPos, totalScriptsCount)

    end

    if not totalScriptsCount then
        local messages = {
            [__imTabs1] = '{ffa500}Ой... {ffffff}Похоже нет доступных для скачивания скриптов.\n{abcdef}Либо вы установили все доступные...',
            [__imTabs2] = '{ffa500}Ой... {ffffff}Похоже у вас нет установленных скриптов.\n{abcdef}Для установки перейдите во вкладку "Доступные Скрипты"...',
        }
        imgui.CenterTextColoredRGB(messages[draw])
    end

end

function __imTabs1(k, v, buttonPos, totalScriptsCount)
    if not doubleTapDebugTable[v.name] and not v.isDeleted then
        local displayScript = false

        if #scriptSeachBuffer.v == 0 then
            displayScript = true
        else
            local searchQuery = string.lower(u8:decode(scriptSeachBuffer.v))
            local name = string.lower(v.name)
            local shortDescription = string.lower(v.short_description)
            local description = string.lower(v.description)

            if name:find(searchQuery, 1, true) or shortDescription:find(searchQuery, 1, true) or description:find(searchQuery, 1, true) then
                displayScript = true
            end
        end

        if displayScript then
            totalScriptsCount = true
            imgui.TextColoredRGB('Скрипт: {FFFF00}'..v.name)
            if imgui.IsItemHovered() then
                imgui.SetTooltip(u8('Версия в репозитории: '..v.version..'\nДата релиза/обновления: '..v.realeseDate))
            end
            imgui.SameLine()

            imgui.SetCursorPosX(buttonPos)
            if imgui.Button(u8'Установить##installScriptButton'..k, imgui.ImVec2(80,0)) then
                _openConfirmPopup(v.name, "{ffa500}Внимание! {ffffff}Вы действительно хотите {ffa500}установить {ffffff}скрипт?", "Установить", __installScript)
            end
            if imgui.IsItemHovered() then
                imgui.SetTooltip(u8('Версия в репозитории: '..v.version..'\nДата релиза/обновления: '..v.realeseDate))
            end
            imgui.TextDisabled(u8(v.short_description))
            if imgui.CollapsingHeader(u8'Показать полное описание##showDescButton'..k) then
                imgui.TextColoredRGB(v.description)
            end
            imgui.Separator()
        end
    else
        if not v.isDeleted and doubleTapDebugTable[v.name] and doubleTapDebugTable[v.name] ~= v.version then
            isUpdateAvaible = true
        end
    end
    return totalScriptsCount
end

function __imTabs2(k, v, buttonPos, totalScriptsCount)
    if doubleTapDebugTable[v.name] then
        totalScriptsCount = true
        imgui.TextColoredRGB('Скрипт: {FFFF00}'..v.name..' {abcdef}Ver: '..doubleTapDebugTable[v.name])
        if imgui.IsItemHovered() then
            imgui.SetTooltip(u8('Версия в репозитории: '..v.version..'\nДата релиза/обновления: '..v.realeseDate))
        end
        imgui.SameLine()

        if v.isDeleted then
            imgui.TextDisabled(u8("(Устаревшее)"))
        elseif doubleTapDebugTable[v.name] ~= v.version then
            imgui.TextColoredRGB('{99ff99}Доступно обновление. {abcdef}('..doubleTapDebugTable[v.name]..' -> '..v.version..')')
            isUpdateAvaible = true

            imgui.SameLine()
            imgui.SetCursorPosX(buttonPos-90)
            if imgui.Button(u8'Обновить##UpdateScriptButton'..k, imgui.ImVec2(80,0)) then
                if doubleTapDebugTable[v.name]:match('(%d)%.') ~= v.version:match('(%d)%.') then
                    _openConfirmPopup(v.name, "{ffa500}Внимание! {ffffff}Скрипт получает мажорное обновление.\nФайлы настроек будут {ff0000}удалены.", "Обновить", __updateMajorScript)
                else
                    if deleteScriptFile(v.name) then
                        createScriptFile(v.name, '{99ff99}[Успех] {ffffff}Скрипт {ffff00}"'..v.name..'"{ffffff} обновлён.')
                        updateListOfInstalledScripts()
                        isUpdateAvaible = false
                    end
                end
            end
        end

        imgui.SameLine()
        imgui.SetCursorPosX(buttonPos)

        if imgui.Button(u8'Удалить##deleteScriptButton'..k, imgui.ImVec2(80,0)) then
            _openConfirmPopup(v.name, "{ffa500}Внимание! {ffffff}Вы действительно хотите {ff0000}удалить {ffffff}скрипт?\nФайлы {ff0000}настроек/логов будут удалены!", "Удалить", __deleteScript)
        end

        if not v.isDeleted then
            imgui.TextDisabled(u8(v.short_description))
            if imgui.CollapsingHeader(u8'Показать полное описание##showDescButton'..k) then
                imgui.TextColoredRGB(v.description)
            end
        end
        imgui.Separator()
    end
    return totalScriptsCount
end

local __needDeleteConfigs = false
function _confirmPopup()
    if imgui.BeginPopupModal(u8'Подтверждение##confirmPopup',nil,imgui.WindowFlags.NoResize) then
        imgui.SetWindowSize(imgui.ImVec2(imgui.CalcTextSize(u8(_confirmPopupData.text)).x, -1))

        imgui.TextColoredRGB('Скрипт: {ffa500}"'.._confirmPopupData.scriptName..'"')

        imgui.NewLine()

        imgui.TextColoredRGB(_confirmPopupData.text)

        imgui.NewLine()

        imgui.TextColoredRGB('Вы {99ff99}подтверждаете{ffffff} действие?')

        if _confirmPopupData.buttonFunc == __deleteScript then
            imgui.Separator()
            if imgui.Checkbox(u8'Не удалять данные##confirmPopupDeleteSettings', imgui.ImBool(__needDeleteConfigs)) then
                __needDeleteConfigs = not __needDeleteConfigs
            end
            if imgui.IsItemHovered() then
                imgui.SetTooltip(u8('Если галочка активна, то файлы скрипта по пути: /config/'.._confirmPopupData.scriptName:gsub('%.lua$', '')..'/\nУдалены не будут.'))
            end
            imgui.Separator()
        end

        imgui.NewLine()

        imgui.SetCursorPosX(imgui.GetWindowWidth()/2-100)
        if imgui.Button(u8(_confirmPopupData.buttonText..'##confirmPopup'), imgui.ImVec2(100, 50)) then
            _confirmPopupData.buttonFunc(_confirmPopupData.scriptName, __needDeleteConfigs)
            imgui.CloseCurrentPopup()
        end
        imgui.SameLine()
        if imgui.Button(u8'Отмена##declinePopup', imgui.ImVec2(100, 50)) then
            imgui.CloseCurrentPopup()
        end
    
        imgui.EndPopup()
    end
end

function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end
function imgui.CenterTextColoredRGB(text)
    local width = imgui.GetWindowWidth()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local textsize = w:gsub('{.-}', '')
            local text_width = imgui.CalcTextSize(u8(textsize))
            imgui.SetCursorPosX( width / 2 - text_width .x / 2 )
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else
                imgui.Text(u8(w))
            end
        end
    end
    render_text(text)
end
function imgui.ActivatedButtons(tabs, count, ...)

    local buttonLabels = {...}
    local style = imgui.GetStyle()
    local windowWidth = imgui.GetWindowWidth()

    for index = 1, count do
        if index == tabs then
            imgui.PushStyleColor(imgui.Col.Button, style.Colors[imgui.Col.CheckMark])
            imgui.PushStyleColor(imgui.Col.ButtonHovered, style.Colors[imgui.Col.CheckMark])
            imgui.PushStyleColor(imgui.Col.ButtonActive, style.Colors[imgui.Col.CheckMark])

            imgui.Button(buttonLabels[index] or 'Button ' .. index .. '##' .. index, imgui.ImVec2(windowWidth, 50))

            imgui.PopStyleColor(3)
        else
            if imgui.Button(buttonLabels[index] or 'Button ' .. index .. '##' .. index, imgui.ImVec2(windowWidth, 50)) then
                return index
            end
        end

    end

    return tabs

end
function imgui.CenterButton(label, size)
    if size then
        imgui.SetCursorPosX((imgui.GetWindowWidth() - size) / 2)
        return imgui.Button(u8(label),imgui.ImVec2(size,0))
    else
        imgui.SetCursorPosX((imgui.GetWindowWidth() - imgui.CalcTextSize(u8(label)).x) / 2)
        return imgui.Button(u8(label))
    end
end
function imgui.Link(label, description)

    local size = imgui.CalcTextSize(label)
    local p = imgui.GetCursorScreenPos()
    local p2 = imgui.GetCursorPos()
    local result = imgui.InvisibleButton(label, size)

    imgui.SetCursorPos(p2)

    if imgui.IsItemHovered() then
        if description then
            imgui.BeginTooltip()
            imgui.PushTextWrapPos(600)
            imgui.TextUnformatted(description)
            imgui.PopTextWrapPos()
            imgui.EndTooltip()

        end

        imgui.TextColored(imgui.GetStyle().Colors[imgui.Col.CheckMark], label)
        imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y + size.y), imgui.ImVec2(p.x + size.x, p.y + size.y), imgui.GetColorU32(imgui.GetStyle().Colors[imgui.Col.CheckMark]))

    else
        imgui.TextColored(imgui.GetStyle().Colors[imgui.Col.CheckMark], label)
    end

    return result
end

function trimMultilineString(s)
    local lines = {}
    for line in s:gmatch("[^\n]+") do
        table.insert(lines, line:match("^%s+(.+)") or ' ')
    end
    return table.concat(lines, "\n")
end

function dateToNumber(dateStr)
    local day, month, year = dateStr:match("(%d+)%.(%d+)%.(%d+)")
    return tonumber(year .. month .. day)
end