script_author('JustFedot')
script_name('[JF] Open Cases')
script_version('3.0.2')

require("moonloader")
local sampev = require("samp.events")
local imgui = require("imgui")
local encoding = require("encoding")
encoding.default = "CP1251"
local u8 = encoding.UTF8

do
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    local ImVec2 = imgui.ImVec2

    -- Цвета
    colors[clr.Text]                 = ImVec4(0.90, 0.90, 0.90, 1.00)
    colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
    colors[clr.WindowBg]             = ImVec4(0.12, 0.14, 0.17, 1.00)
    colors[clr.ChildWindowBg]        = ImVec4(0.10, 0.14, 0.17, 1.00)
    colors[clr.PopupBg]              = ImVec4(0.08, 0.08, 0.08, 0.94)
    colors[clr.Border]               = ImVec4(0.43, 0.43, 0.50, 0.50)
    colors[clr.BorderShadow]         = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.FrameBg]              = ImVec4(0.20, 0.25, 0.29, 1.00)
    colors[clr.FrameBgHovered]       = ImVec4(0.18, 0.35, 0.58, 0.40)
    colors[clr.FrameBgActive]        = ImVec4(0.18, 0.35, 0.58, 0.67)
    colors[clr.TitleBg]              = ImVec4(0.09, 0.12, 0.14, 0.65)
    colors[clr.TitleBgActive]        = ImVec4(0.18, 0.35, 0.58, 1.00)
    colors[clr.TitleBgCollapsed]     = ImVec4(0.00, 0.00, 0.00, 0.51)
    colors[clr.MenuBarBg]            = ImVec4(0.15, 0.18, 0.22, 1.00)
    colors[clr.ScrollbarBg]          = ImVec4(0.02, 0.02, 0.02, 0.39)
    colors[clr.ScrollbarGrab]        = ImVec4(0.20, 0.25, 0.29, 1.00)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.18, 0.22, 0.25, 1.00)
    colors[clr.ScrollbarGrabActive]  = ImVec4(0.09, 0.21, 0.31, 1.00)
    colors[clr.ComboBg]              = ImVec4(0.20, 0.25, 0.29, 1.00)
    colors[clr.CheckMark]            = ImVec4(0.18, 0.35, 0.58, 1.00)
    colors[clr.SliderGrab]           = ImVec4(0.18, 0.35, 0.58, 1.00)
    colors[clr.SliderGrabActive]     = ImVec4(0.22, 0.39, 0.63, 1.00)
    colors[clr.Button]               = ImVec4(0.20, 0.25, 0.29, 1.00)
    colors[clr.ButtonHovered]        = ImVec4(0.18, 0.35, 0.58, 1.00)
    colors[clr.ButtonActive]         = ImVec4(0.22, 0.39, 0.63, 1.00)
    colors[clr.Header]               = ImVec4(0.18, 0.35, 0.58, 0.55)
    colors[clr.HeaderHovered]        = ImVec4(0.22, 0.39, 0.63, 0.80)
    colors[clr.HeaderActive]         = ImVec4(0.22, 0.39, 0.63, 1.00)
    colors[clr.Separator]            = ImVec4(0.43, 0.43, 0.50, 0.50)
    colors[clr.SeparatorHovered]     = ImVec4(0.60, 0.60, 0.70, 1.00)
    colors[clr.SeparatorActive]      = ImVec4(0.70, 0.70, 0.90, 1.00)
    colors[clr.ResizeGrip]           = ImVec4(0.18, 0.35, 0.58, 0.25)
    colors[clr.ResizeGripHovered]    = ImVec4(0.18, 0.35, 0.58, 0.67)
    colors[clr.ResizeGripActive]     = ImVec4(0.18, 0.35, 0.58, 0.95)
    colors[clr.CloseButton]          = ImVec4(0.20, 0.25, 0.29, 0.60)
    colors[clr.CloseButtonHovered]   = ImVec4(0.25, 0.30, 0.35, 0.80)
    colors[clr.CloseButtonActive]    = ImVec4(0.30, 0.35, 0.40, 1.00)       
    colors[clr.PlotLines]            = ImVec4(0.61, 0.61, 0.61, 1.00)
    colors[clr.PlotLinesHovered]     = ImVec4(1.00, 0.43, 0.35, 1.00)
    colors[clr.PlotHistogram]        = ImVec4(0.90, 0.70, 0.00, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(1.00, 0.60, 0.00, 1.00)
    colors[clr.TextSelectedBg]       = ImVec4(0.18, 0.35, 0.58, 0.35)
    --colors[clr.ModalWindowDarkening] = ImVec4(0.80, 0.80, 0.80, 0.35)

    -- Скругления и отступы
    style.WindowPadding = ImVec2(15, 15)
    style.WindowRounding = 3.0
    style.FramePadding = ImVec2(5, 5)
    style.ItemSpacing = ImVec2(12, 8)
    style.ItemInnerSpacing = ImVec2(8, 6)
    style.IndentSpacing = 25.0
    style.ScrollbarSize = 15.0
    style.ScrollbarRounding = 15.0
    style.GrabMinSize = 15.0
    style.GrabRounding = 7.0
    style.ChildWindowRounding = 8.0
    style.FrameRounding = 6.0
end

do
    Jcfg = {
        _version = 0.1,
        _author = "JustFedot",
        _telegram = "@justfedot",
        _help = [[
            Jcfg - модуль для сохранения и загрузки конфигурационных файлов в Lua, используя формат JSON, с поддержкой конфигурации для ImGui.
            Важно: модуль должен быть подключен после всех необходимых `require`.
        
            Использование:
                - Инициализация модуля:
                    jcfg = Jcfg()
        
                - Сохранение массива в файл:
                    jcfg.save(table, path)
                    - table: массив, который нужно сохранить.
                    - path: путь для сохранения. Если не указан, сохранение будет в moonloader/config/Имя_скрипта/config.json
        
                - Загрузка массива из файла:
                    table = jcfg.load(path)
                    - table: переменная, в которую будет загружен массив.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Обновление массива данными из файла:
                    jcfg.update(table, path)
                    - table: массив, который нужно обновить данными из файла.
                    - path: путь к файлу для загрузки. Если не указан, будет искать в moonloader/config/Имя_скрипта/config.json
        
                - Настройка массива для использования с ImGui:
                    imtable = jcfg.setupImgui(table)
                    - table: массив, который будет преобразован для использования с ImGui.
                    - imtable: возвращает массив, готовый к использованию с ImGui.
        
            Пример использования:
        
                -- Инициализация модуля
                local jcfg = Jcfg()
        
                -- Создание конфигурации
                local cfg = {
                    params = {'123'},
                    param = 12
                }
        
                -- Обновление конфигурации данными из файла (если файл существует)
                jcfg.update(cfg)
        
                -- Настройка конфигурации для использования с ImGui
                local imcfg = jcfg.setupImgui(cfg)
        
                -- Сохранение конфигурации в файл
                jcfg.save(cfg)
        ]]                      
    }

    function Jcfg.__init()
        local self = {}

        local json = require('dkjson')

        local function makeDirectory(path)
            assert(type(path) == "string" and path:find('moonloader'), "Path must be a string and include 'moonloader' folder")
            
            path = path:gsub("[\\/][^\\/]+%.json$", "")

            if not doesDirectoryExist(path) then
                if not createDirectory(path) then
                    return error("Failed to create directory: " .. path)
                end
            end
        end        

        local function setupImguiConfig(table)
            assert(type(table) == "table", ("bad argument #1 to 'setupImgui' (table expected, got %s)"):format(type(table)))
            local function setupImguiConfigRecursive(table)
                local imcfg = {}
                for k, v in pairs(table) do
                    if type(v) == "table" then
                        imcfg[k] = setupImguiConfigRecursive(v)
                    elseif type(v) == "number" then
                        if v % 1 == 0 then
                            imcfg[k] = imgui.ImInt(v)
                        else
                            imcfg[k] = imgui.ImFloat(v)
                        end
                    elseif type(v) == "string" then
                        imcfg[k] = imgui.ImBuffer(256)
                        imcfg[k].v = u8(v)
                    elseif type(v) == "boolean" then
                        imcfg[k] = imgui.ImBool(v)
                    else
                        error(("Unsupported type for imguiConfig: %s"):format(type(v)))
                    end
                end
                return imcfg
            end
            return setupImguiConfigRecursive(table)
        end
        ----------------------------------------------------------------

        function self.save(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'save' (table expected, got %s)"):format(type(table)))
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
            else
                assert(thisScript().name, "Script name is not defined")
                path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
            end
            makeDirectory(path)
            local file = io.open(path,"w")
            if file then
                file:write(json.encode(table, {indent = true}))
                file:close()
            else
                error("Could not open file for writing: " .. path)
            end
        end

        function self.load(path)
            assert(path == nil or type(path) == "string", "Path must be nil or a valid file path.")
            if path then
                path = path:find('%.json$') and path or path..'.json'
			else
				path = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\config.json'
			end
            if doesFileExist(path) then
                local file = io.open(path, "r")
                if file then
                    local content = file:read("*all")
                    file:close()
                    return json.decode(content)
                else
                    return error("Could not load configuration")
                end
            else
                return {}
            end
        end

        function self.update(table, path)
            assert(type(table)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(table)))
            assert(path == nil or (type(path) == "string" and path:match("^.+%.json$")), "Path must be nil or a valid file path ending with '.json'")
            local loadedCfg = self.load(path)
			
			if loadedCfg then
				for k, v in pairs(table) do
					if loadedCfg[k] ~= nil then
						table[k] = loadedCfg[k]
					end
				end
			end

            return true
        end

        function self.setupImgui(table)
            assert(imgui ~= nil, "The imgui library is not loaded. Please ensure it is required before using 'setupImgui' function.")
            return setupImguiConfig(table)
        end

        return self
    end

    setmetatable(Jcfg, {
        __call = function(self)
            return self.__init()
        end
    })
end
local jcfg = Jcfg()


local cfg = {
    isReloaded = false,
    silentMode = false,
    noCloseInventory = false,
    delay = {
        min = 60,
        random = 15
    },
    acessory = {
        item = {false, false, false, false, false, false}
    },
    firstLaunch = true
}
jcfg.update(cfg)
local imcfg = jcfg.setupImgui(cfg)
function save()
    jcfg.save(cfg)
end

function resetDefaultCfg()
    cfg = {
        isReloaded = false,
        silentMode = false,
        noCloseInventory = false,
        delay = {
            min = 60,
            random = 15
        },
        acessory = {
            item = {false, false, false, false, false, false}
        },
        firstLaunch = true
    }
    cfg.isReloaded = true
    save()
    thisScript():reload()
end


local utils = (function()
    local self = {}

    local function cyrillic(text)
        local convtbl = {[230]=155,[231]=159,[247]=164,[234]=107,[250]=144,[251]=168,[254]=171,[253]=170,[255]=172,[224]=97,[240]=112,[241]=99,[226]=162,[228]=154,[225]=151,[227]=153,[248]=165,[243]=121,[184]=101,[235]=158,[238]=111,[245]=120,[233]=157,[242]=166,[239]=163,[244]=63,[237]=174,[229]=101,[246]=36,[236]=175,[232]=156,[249]=161,[252]=169,[215]=141,[202]=75,[204]=77,[220]=146,[221]=147,[222]=148,[192]=65,[193]=128,[209]=67,[194]=139,[195]=130,[197]=69,[206]=79,[213]=88,[168]=69,[223]=149,[207]=140,[203]=135,[201]=133,[199]=136,[196]=131,[208]=80,[200]=133,[198]=132,[210]=143,[211]=89,[216]=142,[212]=129,[214]=137,[205]=72,[217]=138,[218]=167,[219]=145}
        local result = {}
        for i = 1, #text do
            local c = text:byte(i)
            result[i] = string.char(convtbl[c] or c)
        end
        return table.concat(result)
    end

    local function roundUpToThreeDecimalPlaces(num)
        local mult = 10^3
        return math.ceil(num * mult) / mult
    end

    --------------------------------------------------------------------------------------------------------------------------------

    function self.addChat(a)
        if cfg.silentMode then return end
        if a then local a_type = type(a) if a_type == 'number' then a = tostring(a) elseif a_type ~= 'string' then return end else return end
        sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
    end

    function self.printStringNow(text, time)
        if not text then return end
        time = time or 100
        text = type(text) == "number" and tostring(text) or text
        if type(text) ~= 'string' then return end
        printStringNow(cyrillic(text), time)
    end

    function self.formatTime(seconds)
        local hours = math.floor(seconds / 3600)
        local minutes = math.floor((seconds % 3600) / 60)
        local seconds = seconds % 60
        return string.format("%02d:%02d:%02d", hours, minutes, seconds)
    end

    function self.random(x, y)
        math.randomseed(os.time())

        for i = 1, 5 do math.random() end

        return math.random(x, y)
    end

    return self
end)()


local imgui_windows = {
    main = imgui.ImBool(false)
}



local state = {
    active = false,
    items = {},
    scanning = false,
    click = false,
    nextItem = false,
    state = "{ff0000}Отключен",
    keys = false
}

local txdIds = {
    page = {},
    cell = {},
    acessory = {}
}

local workThread = lua_thread.create_suspended(function()

    local function checkInventoryWindow()
        for id = 2000, 2200 do
            if sampTextdrawIsExists(id) then
                local text = sampTextdrawGetString(id)
                if text:find('INVENTORY') or text:find('…H‹EHЏAP’', 1, false) then return true end
            end
        end
        return false
    end

    local function changePage(num)
        -- if sampTextdrawIsExists(num) then
        --     txdIds.cell = {}
        --     sampSendClickTextdraw(num)
        -- end
        txdIds.cell = {}
        while #txdIds.cell < 36 do
            sampSendClickTextdraw(num)
            wait(500)
        end
        --wait(500)
    end


    local timeAnchor = 0
    local remeaningTime = 10


    local wait_delay = 500

    while state.active do wait(0)




        if timeAnchor ~= 0 then

            local cClock = os.clock()

            local deadLine = (remeaningTime - (cClock - timeAnchor))

            if deadLine <= 0 then

                timeAnchor = 0
                state.click = true
                
            end

            state.state = "{ffa500}Ожидаю: " .. utils.formatTime(deadLine)

        end





        if state.click then

            while not checkInventoryWindow() do
                state.state = "{ffa500}Открываю инвентарь..."
                sampSendChat("/invent")
                wait(300)
            end
            

            state.state = "{ffa500}Запускаю открытие..."



            for index, needPress in ipairs(cfg.acessory.item) do

                if needPress then


                    state.state = "{ffa500}Прожимаю слот аксессуара {99ff99}"..index

                    state.nextItem = false
    
                    while #txdIds.acessory < 6 do wait(0) end
    
                    while not state.nextItem do
                        sampSendClickTextdraw(txdIds.acessory[index])
                        wait(wait_delay)
                    end


                end

            end



            for index, item in pairs(state.items) do

                state.nextItem = false

                while not checkInventoryWindow() or #txdIds.cell < 36 do wait(0) end

                state.state = "{ffa500}Переключаю страницу на: {99ff99}"..item.page

                changePage(txdIds.page[item.page])

                state.state = "Открываю {99ff99}'"..item.name.."'"
                

                while not state.nextItem do
                    sampSendClickTextdraw(txdIds.cell[item.slot])
                    if index == #state.items then
                        wait(wait_delay)
                    else
                        wait(utils.random(wait_delay, 1500))
                    end
                end

            end

            state.click = false

            if not cfg.noCloseInventory then
                state.state = "{ffa500}Закрываю инвентарь..."
                sampSendClickTextdraw(65535)
                sampSendClickTextdraw(65535)
            end

            timeAnchor = os.clock()

            remeaningTime = (cfg.delay.min + (cfg.delay.random > 0 and utils.random(0, cfg.delay.random) or 0))*60

        end




    end
end)

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end

    utils.addChat('{99ff99}Загружен{ffffff}. Команды: {ffa500}/css /cs')

    sampRegisterChatCommand('css', function()
        imgui_windows.main.v = not imgui_windows.main.v
    end)

    sampRegisterChatCommand('cs', function()
        toggleScript()
    end)
    
    if cfg.isReloaded then
        cfg.isReloaded = false
        imgui_windows.main.v = true
        save()
    end

    while true do wait(0)
        imgui.Process = imgui_windows.main.v
    end
end







function toggleScript()

    if cfg.firstLaunch then

        imgui_windows.main.v = false
        sampShowDialog(
            65535,
            script.this.name .. ": "..script.this.version .. " Помощь",
            "{FF0000}Важно:{FFFFFF} настройки инвентаря должны быть {FF8000}сброшены до стандартных{FFFFFF}, иначе скрипт может работать некорректно.\n"..
            '{FFFFFF} Чтобы сбросить настройки инвентаря, пропишите {FF8000} /settings {FFFFFF} -> {FF8000} Настройки инвентаря {FFFFFF} -> {FF8000} Сбросить настройки инвентаря {FFFFFF}.\n\n'..
            "{00FF00}Как работает авто-открытие сундуков и тайников:\n"..
            "-------------------------------------------------------\n"..
            ("{FFFFFF}Этот скрипт автоматически открывает {FF8000}сундуки{FFFFFF} и {FF8000}тайники{FFFFFF}.\n" ..
            "Когда вы его включаете, скрипт сканирует инвентарь и ищет предметы по ключевым словам: ' {FF8000}Тайник{FFFFFF} ' и ' {FF8000}Сундук{FFFFFF} '.\n" ..
            "Он запоминает, где находятся эти предметы, и автоматически нажимает на них.\n" ..
            "{FF0000}Важно:{FFFFFF} после активации авто-открытия, {FF0000}не перемещайте сундуки в инвентаре{FFFFFF}!\n" ..
            "Если вам нужно что-то переместить, сначала {FF0000}отключите авто-открытие{FFFFFF}.\n\n")..
            "{00FF00}Как работает открытие слотов аксессуаров:\n"..
            "-------------------------------------------------------\n"..
            ("{FFFFFF}Скрипт также может автоматически нажимать на слоты аксессуаров (например, {FF8000}аксессуар обрез{FFFFFF}).\n" ..
            "В меню есть кнопка для настройки, где вы можете выбрать, какие из 6 слотов нужно открывать.\n" ..
            "{FF0000}Важно:{FFFFFF} на выбранном предмете должна быть кнопка ' {FF8000}Использовать{FFFFFF} '.\n" ..
            "Если скрипт зависает при попытке открыть слоты аксессуаров, попробуйте {FF0000}отключить проблемный слот{FFFFFF}.\n\n") ..
            "{ABCDEF}Это окошко показывается только при первом включении скрипта. Более вы его не увидите.\n"..
            "Ознакопиться повторно можно нажав кнопку 'Помощь' в меню скрипта.",
            "Понял",
            nil,
            0
        )
        cfg.firstLaunch = false
        save()
        return

    end


    state.active = not state.active

    utils.addChat(
        state.active and "Активирован." or "Деактивирован."
    )

    if state.active then

        state.state = "{ffa500}Сканирую инвентарь..."

        state.items = {}
        state.click = false
        state.scanning = false
        state.nextItem = false

        if #state.items == 0 then
            state.scanning = true
            sampSendChat('/stats')
        else
            state.click = true
        end

    else

        state.state = "{ff0000}Отключен"
        state.keys = false

    end

    __threadManager(workThread, state.active)

    if not state.active then
        state.state = "{ff0000}Отключен"
    end
end










function __threadManager(thread, var)
    if var then
        if thread:status() ~= "yielded" then
            thread:run()
        end
    else
        if thread:status() == "yielded" then
            thread:terminate()
        end
    end
end


function sampev.onShowDialog(id, style, title, button1, button2, text)

    if state.active then
        if title:find('{BFBBBA}{ff0000}Ошибка') and text:find('{ffffff}Для открытия тайника необходимо иметь ключ для открытия тайника') then
            sampSendDialogResponse(id, 1)
            return false
        elseif text:find('При использовании') and text:find('вы дополнительно получили предмет') and text:find('4 ДОНАТ') then
            sampSendDialogResponse(id, 1)
            return false
        end
    end

    if not state.scanning then return end


    if title == "{BFBBBA}Основная статистика" then
        state.items = {}
        state.keys = false
        sampSendDialogResponse(id, 1)
        return
    end

    if title:find("^{BFBBBA}{BEA555}.+ %[ID:%d+%]$") and text:find("{ffcc66}%[слот%].+Название.+{ffcc66}Количество") then
        local n = -1
        for line in text:gmatch("[^\r\n]+") do
            __inventoryLineProsessor(line)

            if line:find(">> Следующая страница") then
                sampSendDialogResponse(id, 1, n, line)
                return
            end

            n=n+1
        end

        lua_thread.create(function()
            repeat wait(0) until sampIsDialogActive()
            sampCloseCurrentDialogWithButton(0)
            utils.addChat(
                "Найдено: {ffa500}" .. (#state.items) .. " {ffffff}предметов. {99ff99}Запускаю {ffffff}открытие."
            )

            if not state.keys then
                utils.addChat("{FF0000}Внимание! {FFFFFF}У вас нет {ff0000}ключей от тайника{ffffff}. Скрипт продолжит работу, но не сможет открыть тайники.")
            end

            state.click = true
            state.scanning = false
        end)

        -- sampSendDialogResponse(id, 0)

        -- state.openInventory = true

        return
    end
end

function __inventoryLineProsessor(line)

    if line:find("{FFFFFF}%[%d+%][%s\t]*.+[%s\t]*{8F8FDC}%[%d+ шт%]") then
        local slot, name, count = line:match("{FFFFFF}%[(%d+)%][%s\t]*(.-)[%s\t]*{8F8FDC}%[(%d+) шт%]")
        if name:find("^[Сс]ундук") or name:find("^[Тт]айник") then
            slot, count = tonumber(slot) + 1, tonumber(count)

            local page = math.ceil(slot / 36)  -- Номер страницы

            local slot_on_page = slot % 36
            slot_on_page = (slot_on_page == 0) and 36 or slot_on_page -- Номер слота на текущей странице

            table.insert(state.items, {
                name = name,            -- Имя предмета
                page = page,            -- Номер страницы в инвентаре
                slot = slot_on_page,    -- Номер слота на текущей странице
                count = count           -- Количество предметов
            })

        elseif name == "Торговый ключ" or name == "Ключ для тайника" then

            state.keys = true

        end
    end

end



function sampev.onShowTextDraw(id, data)

    local cell = __getInventoryCell(data.position.x, data.position.y)
    if cell then
        txdIds.cell[cell] = id
    end


    -- Запись id переключателей страниц
    local page = __getPageButton(data.position.x, data.position.y)
    if page then
        txdIds.page[page] = id
    end

    local acessoryCell = __getAcessoryCell(data.position.x, data.position.y)
    if acessoryCell then
        txdIds.acessory[acessoryCell] = id
    end

    if not state.active then return end

    if state.click and not state.nextItem and (data.text == "…CЊO‡’€O‹AЏ’" or data.text == "USE") then
        -- sampSendClickTextdraw(id+1)
        -- state.nextItem = true
        
        lua_thread.create(function()

            local tid = id+1

            while not sampTextdrawIsExists(tid) do
                if not state.active then return end
                wait(0)
            end

            while sampTextdrawIsExists(tid) do
                if not state.active then return end
                sampSendClickTextdraw(tid)
                wait(200)
            end

            if not state.active then return end

            state.nextItem = true
            --wait(500)
            -- sampSendClickTextdraw(id+1)
            -- wait(utils.random(500, 1500))
            -- state.nextItem = true
        end)
    end
end

function __getInventoryCell(x, y)
    x, y = math.floor(x), math.floor(y)
    local coords = {
        x = {[467] = 1, [493] = 2, [520] = 3, [546] = 4, [573] = 5, [599] = 6},
        y = {[127] = 0, [158] = 6, [188] = 12, [219] = 18, [249] = 24, [280] = 30}
    }

    if coords.x[x] and coords.y[y] then
        return coords.x[x] + coords.y[y]
    else
        return false
    end
end


function __getPageButton(x, y)
    x, y = math.floor(x), math.floor(y)
    local coords = {
        x = {[522] = 1, [531] = 2, [541] = 3, [550] = 4, [560] = 5},
        y = {[314] = 0}
    }

    if coords.x[x] and coords.y[y] then
        return coords.x[x] + coords.y[y]
    else
        return false
    end
end

function __getAcessoryCell(x, y) --130 156 182
    x, y = math.floor(x), math.floor(y)
    local coords = {
        x = {[383] = 1, [405] = 2, [428] = 3},
        y = {[130] = 0, [156] = 3}
    }

    if coords.x[x] and coords.y[y] then
        return coords.x[x] + coords.y[y]
    else
        return false
    end
end



function sampev.onSendClickTextDraw(id)
    if state.active and state.click then return false end
end


































----------------------------------------------------------------INTERFACE----------------------------------------------------------------

local fa = require 'fAwesome5'

local fa_font = nil
local fa_glyph_ranges = imgui.ImGlyphRanges({ fa.min_range, fa.max_range })
function imgui.BeforeDrawFrame()
    if fa_font == nil then
        local font_config = imgui.ImFontConfig()
        font_config.MergeMode = true

        fa_font = imgui.GetIO().Fonts:AddFontFromFileTTF('moonloader/resource/fonts/fa-solid-900.ttf', 13.0, font_config, fa_glyph_ranges)
    end
end


local notifications = {}
local w,h = getScreenResolution()
local window_width,window_height = 540, 280
local imStyle = imgui.GetStyle()
local imTabs = 1
function imgui.OnDrawFrame()
    
    if imgui_windows.main.v then

        imgui.SetNextWindowSize(imgui.ImVec2(window_width, window_height), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2 - window_width/2, h/2 - window_height/2), imgui.Cond.FirstUseEver)
        imgui.Begin("##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.NoResize)

        imgui.customTitleBar(imgui_windows.main, resetDefaultCfg, imgui.GetWindowWidth())

        -- imTabs = imgui.Tabs(2, imTabs, 40,
        --     fa.ICON_FA_WRENCH .. u8(" Основные настройки"),
        --     fa.ICON_FA_SITEMAP .. u8(" Предметы")
        -- )

        __tabInterface(imTabs)

        imgui.showNotifications(2)

        imgui.End()

    end

end
function imgui.customTitleBar(param, resetFunc, wWidth)
    if imgui.Link("t.me/justfedotScript", u8("Telegram канал автора.\nНажми чтобы перейти/скопировать")) then
        imgui.addNotification(u8"Ссылка скопирована!")
        imgui.SetClipboardText("https://t.me/justfedotScript")
        os.execute(('explorer.exe "%s"'):format("https://t.me/justfedotScript"))
    end  

    imgui.SameLine()
    imgui.SetCursorPosX(wWidth/2 - imgui.CalcTextSize(script.this.name .. ": " .. script.this.version).x/2)
    imgui.TextColoredRGB(script.this.name .. ": " .. script.this.version)

    imgui.SameLine()

    imgui.SetCursorPosX(wWidth - 170 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa.ICON_FA_DOLLAR_SIGN.."##popup_donation_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("donationPopupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(wWidth - 110 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.Button(fa.ICON_FA_ARROW_DOWN.."##popup_menu_button", imgui.ImVec2(50, 25)) then
        imgui.OpenPopup("upWindowPupupMenu")
    end

    imgui.SameLine()

    imgui.SetCursorPosX(wWidth - 50 - imStyle.ItemSpacing.x)
    imgui.SetCursorPosY(imStyle.ItemSpacing.y)
    if imgui.customCloseButton(fa.ICON_FA_TIMES.."##close_button", imgui.ImVec2(50, 25)) then
        param.v = false
    end

    if imgui.BeginPopup("upWindowPupupMenu") then
        
        imgui.CenterTextColoredRGB("Доп. Функции:")
        imgui.Separator()

        if imgui.Selectable(u8("Перезагрузить скрипт").."##reloadScriptButton", false) then
            cfg.isReloaded = true
            save()
            thisScript():reload()
        end
        if imgui.Selectable(u8("Сбросить все настройки").."##resetSettingsButton", false) then
            resetFunc()
        end
    
        imgui.EndPopup()
    end

    if imgui.BeginPopup("donationPopupMenu") then
        imgui.CenterTextColoredRGB("{ffa500}Donation")
        imgui.Separator()
        imgui.Text(u8("Если вы желаете выразить поддержку, можете поддержать автора.\nНа данный момент доступен перевод на Укр. Карту 'Monobank'."))
        if imgui.Link("5375 4112 2231 4945", u8"Нажмите что-бы скопировать.") then
            imgui.SetClipboardText("5375411222314945")
            utils.addChat("Номер карты {99ff99}скопирован.")
            imgui.addNotification(u8"Номер карты скопирован.")
        end
        imgui.TextColoredRGB(("Имя на карте: {99ff99}Михайло Б.\nВ комментарии к платежу укажите {99ff99}'"..script.this.name.."' {ffffff}что-бы я понимал актуальность скрипта.\n{ffa500}Большое спасибо за поддержку."))
    
        imgui.EndPopup()
    end

    imgui.Separator()
end


function __tabInterface(tab)
    local tabs = {
        [1] = __mainTab,
        [2] = __itemsTab
    }
    imgui.BeginChild('##main_child', imgui.ImVec2(0, 0), true)
    local tabWidth = imgui.GetWindowWidth()
    tabs[tab](tabWidth)
    imgui.EndChild()
end

function __mainTab(tabWidth)



    if imgui.Checkbox(u8'Активация', imgui.ImBool(state.active)) then toggleScript() end

    imgui.SameLine()
    imgui.SetCursorPosX(tabWidth-imStyle.ItemInnerSpacing.x*2-100)
    if imgui.Button(fa.ICON_FA_HANDS_HELPING..u8(' Помощь'), imgui.ImVec2(100, 0)) then
        imgui.OpenPopup('helpWindowPopup')
    end


    imgui.Separator()

    imgui.TextColoredRGB(
        "Статус скрипта: "..state.state
    )

    imgui.Separator()

    if imgui.Checkbox(u8'Не закрывать инвентарь', imcfg.noCloseInventory) then
        cfg.noCloseInventory = imcfg.noCloseInventory.v
        save()
    end
    imgui.Tooltip("Если включено, окошко инвентаря закрываться не будет.\n(( Использовать во время фулл афк открытия ))")

    imgui.SameLine()

    if imgui.Checkbox(u8'Сайлент-Мод', imcfg.silentMode) then
        cfg.silentMode = imcfg.silentMode.v
        save()
    end
    imgui.Tooltip("Если включено, скрипт не будет присылать никаких сообщений в чат.")

    if imgui.Button(fa.ICON_FA_USER_SECRET..u8(' Слоты аксессуаров ')..fa.ICON_FA_USER_SECRET, imgui.ImVec2(-1, 0)) then
        imgui.OpenPopup('acessoriesSettings')
    end
    imgui.Tooltip("Настройки прожатия по слотам аксессуаров.")


    
    if imgui.SliderInt(u8'Минимальное ожидание', imcfg.delay.min, 1, 120) then
        cfg.delay.min = imcfg.delay.min.v
        save()
    end
    imgui.Tooltip("Минимальное время ожидания перед повторным открытием.\n(( В минутах. ))")

    if imgui.SliderInt(u8'Дополнительное время', imcfg.delay.random, 0, 120) then
        cfg.delay.random = imcfg.delay.random.v
        save()
    end
    imgui.Tooltip("Рандомно добавленное время.\nУкажите 0 если не хотите рандомизировать открытия.\n(( В минутах. ))")




    if imgui.BeginPopup('acessoriesSettings') then

        imgui.CenterTextColoredRGB("Настройка прожатия по слотам 'Аксессуаров'")
        imgui.Separator()

        for k, v in ipairs(cfg.acessory.item) do

            if imgui.Checkbox(u8'Слот №'..k.."##checkbozAcs"..k, imcfg.acessory.item[k]) then
                cfg.acessory.item[k] = imcfg.acessory.item[k].v
                save()
            end

            if (k<3 or k >=4) and k ~= #cfg.acessory.item then imgui.SameLine() end

        end


        imgui.EndPopup()
    end



    if imgui.BeginPopup('helpWindowPopup') then

        imgui.CenterTextColoredRGB("{FF0000}Важно:{FFFFFF} настройки инвентаря должны быть {FF8000}сброшены до стандартных{FFFFFF}, иначе скрипт может работать некорректно.")
        imgui.TextColoredRGB(
            '{FFFFFF} Чтобы сбросить настройки инвентаря, пропишите {FF8000} /settings {FFFFFF} -> {FF8000} Настройки инвентаря {FFFFFF} -> {FF8000} Сбросить настройки инвентаря {FFFFFF}.'
        )
        imgui.NewLine()
    
        imgui.CenterTextColoredRGB("{00FF00}Как работает авто-открытие сундуков и тайников:")
        imgui.Separator()
    
        imgui.TextColoredRGB(
            "{FFFFFF}Этот скрипт автоматически открывает {FF8000}сундуки{FFFFFF} и {FF8000}тайники{FFFFFF}.\n" ..
            "Когда вы его включаете, скрипт сканирует инвентарь и ищет предметы по ключевым словам: ' {FF8000}Тайник{FFFFFF} ' и ' {FF8000}Сундук{FFFFFF} '.\n" ..
            "Он запоминает, где находятся эти предметы, и автоматически нажимает на них.\n" ..
            "{FF0000}Важно:{FFFFFF} после активации авто-открытия, {FF0000}не перемещайте сундуки в инвентаре{FFFFFF}!\n" ..
            "Если вам нужно что-то переместить, сначала {FF0000}отключите авто-открытие{FFFFFF}."
        )
    
        imgui.NewLine()
    
        imgui.CenterTextColoredRGB("{00FF00}Как работает открытие слотов аксессуаров:")
        imgui.Separator()
    
        imgui.TextColoredRGB(
            "{FFFFFF}Скрипт также может автоматически нажимать на слоты аксессуаров (например, {FF8000}аксессуар обрез{FFFFFF}).\n" ..
            "В меню есть кнопка для настройки, где вы можете выбрать, какие из 6 слотов нужно открывать.\n" ..
            "{FF0000}Важно:{FFFFFF} на выбранном предмете должна быть кнопка ' {FF8000}Использовать{FFFFFF} '.\n" ..
            "Если скрипт зависает при попытке открыть слоты аксессуаров, попробуйте {FF0000}отключить проблемный слот{FFFFFF}."
        )
    
        imgui.EndPopup()
    end    



end


----------------------------------------------------------------
function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end
function imgui.CenterTextColoredRGB(text)
    local width = imgui.GetWindowWidth()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local textsize = w:gsub('{.-}', '')
            local text_width = imgui.CalcTextSize(u8(textsize))
            imgui.SetCursorPosX( width / 2 - text_width .x / 2 )
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else
                imgui.Text(u8(w))
            end
        end
    end
    render_text(text)
end
function imgui.customCloseButton(label, size)
    local style = imgui.GetStyle()
    local colors = style.Colors

    local buttonColor = imgui.ImVec4(0.8, 0.0, 0.0, 1.0) -- Red
    local buttonHoverColor = imgui.ImVec4(1.0, 0.2, 0.2, 1.0) -- Lighter Red
    local buttonActiveColor = imgui.ImVec4(0.6, 0.0, 0.0, 1.0) -- Darker Red

    local oldButtonColor = colors[imgui.Col.Button]
    local oldButtonHoveredColor = colors[imgui.Col.ButtonHovered]
    local oldButtonActiveColor = colors[imgui.Col.ButtonActive]
    local oldTextColor = colors[imgui.Col.Text]

    imgui.PushStyleColor(imgui.Col.Button, buttonColor)
    imgui.PushStyleColor(imgui.Col.ButtonHovered, buttonHoverColor)
    imgui.PushStyleColor(imgui.Col.ButtonActive, buttonActiveColor)
    imgui.PushStyleColor(imgui.Col.Text, oldTextColor)

    local clicked = imgui.Button(label, size)
    
    imgui.PopStyleColor(4)

    return clicked
end
function imgui.Link(label, description)

    local size = imgui.CalcTextSize(label)
    local p = imgui.GetCursorScreenPos()
    local p2 = imgui.GetCursorPos()
    local result = imgui.InvisibleButton(label, size)

    imgui.SetCursorPos(p2)

    if imgui.IsItemHovered() then
        if description then
            imgui.BeginTooltip()
            imgui.PushTextWrapPos(600)
            imgui.TextUnformatted(description)
            imgui.PopTextWrapPos()
            imgui.EndTooltip()

        end

        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
        imgui.GetWindowDrawList():AddLine(imgui.ImVec2(p.x, p.y + size.y), imgui.ImVec2(p.x + size.x, p.y + size.y), imgui.GetColorU32(imgui.GetStyle().Colors[imgui.Col.CheckMark]))

    else
        imgui.TextColored(imgui.ImVec4(0.27, 0.53, 0.87, 1.00), label)
    end

    return result
end
function imgui.addNotification(text)
    table.insert(notifications, {
        text = text,
        startTime = os.clock()
    })
end
function imgui.showNotifications(duration)
    local currentTime = os.clock()
    local activeNotifications = #notifications

    -- Начинаем отображение подсказок, если есть активные уведомления
    if activeNotifications ~= 0 then
        imgui.BeginTooltip()
    end
    for i = #notifications, 1, -1 do
        local notification = notifications[i]
        -- Проверяем, прошло ли время показа
        if currentTime - notification.startTime < duration then
            imgui.Text(notification.text)
            activeNotifications = activeNotifications + 1
            -- Если это не последнее уведомление, добавляем разделитель
            if i > 1 then
                imgui.Separator()
            end
        else
            table.remove(notifications, i)
        end
    end

    if activeNotifications ~= 0 then
        imgui.EndTooltip()
    end
end
function imgui.Tabs(count, tabs, width, ...)
    local buttonLabels = {...}
    local style = imgui.GetStyle()
    local windowWidth = imgui.GetWindowWidth() - style.WindowPadding.x * 2
    local minButtonWidth = 0

    -- Определение самой длинной надписи
    for i = 1, count do
        local labelWidth = imgui.CalcTextSize(buttonLabels[i] or "Button "..i).x + style.FramePadding.x * 2
        if labelWidth > minButtonWidth then
            minButtonWidth = labelWidth
        end
    end

    -- Расчет количества кнопок в строке и общего количества строк
    local buttonsPerRow = math.floor(windowWidth / minButtonWidth)
    if buttonsPerRow < 1 then buttonsPerRow = 1 end  -- Убедиться, что хотя бы одна кнопка помещается в строку
    local rows = math.ceil(count / buttonsPerRow)

    for row = 1, rows do
        local buttonsInThisRow = math.min(buttonsPerRow, count - (row - 1) * buttonsPerRow)
        local buttonWidth = (windowWidth - (buttonsInThisRow - 1) * style.ItemSpacing.x) / buttonsInThisRow

        -- Отрисовка кнопок в строке
        for i = 1, buttonsInThisRow do
            local buttonIndex = (row - 1) * buttonsPerRow + i
            local buttonStyle = imgui.GetStyle()
            local needStylePop = false

            if tabs == buttonIndex then
                imgui.PushStyleColor(imgui.Col.Button, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonHovered, buttonStyle.Colors[imgui.Col.CheckMark])
                imgui.PushStyleColor(imgui.Col.ButtonActive, buttonStyle.Colors[imgui.Col.CheckMark])
                needStylePop = true
            end

            local buttonLabel = buttonLabels[buttonIndex] or "Button "..buttonIndex
            local buttonPressed = imgui.Button(buttonLabel, imgui.ImVec2(buttonWidth, width))

            if needStylePop then
                imgui.PopStyleColor(3)
            end

            if buttonPressed and tabs ~= buttonIndex then
                tabs = buttonIndex
            end

            if i < buttonsInThisRow then
                imgui.SameLine()
            end
        end

        -- Переход на новую строку, если это не последняя
        -- if row < rows then
        --     imgui.NewLine()
        -- end
    end

    return tabs
end
function imgui.Tooltip(text)
    if imgui.IsItemHovered() and not imgui.IsItemActive() then
        imgui.SetTooltip(u8(text))
    end
end
function imgui.redButtonDisabled(var, ...)
    if var then
        -- Устанавливаем красный цвет для активной кнопки
        imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(1, 0, 0, 1)) -- Красный цвет
        imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(1, 0.2, 0.2, 1)) -- Светло-красный при наведении
        imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(1, 0, 0, 1)) -- Красный активный цвет
        local clicked = imgui.Button(...)
        imgui.PopStyleColor(3) -- Убираем все три установленных цвета
        return clicked
    else
        -- Делаем кнопку неактивной, полупрозрачной
        local r, g, b, a = imgui.ImColor(imgui.GetStyle().Colors[imgui.Col.Button]):GetFloat4()
        imgui.PushStyleColor(imgui.Col.Button, imgui.ImVec4(r, g, b, a / 2))
        imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.ImVec4(r, g, b, a / 2))
        imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.ImVec4(r, g, b, a / 2))
        imgui.PushStyleColor(imgui.Col.Text, imgui.GetStyle().Colors[imgui.Col.TextDisabled])
        imgui.Button(...)
        imgui.PopStyleColor(4) -- Убираем все четыре установленных цвета
        return false
    end
end